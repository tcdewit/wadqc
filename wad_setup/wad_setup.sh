#!/usr/bin/env bash
# prefer python as this will resolve to the correct python binary in a virtualenv
for shell in python python3 python2; do
    if command -v "${shell}" &>/dev/null; then
        /usr/bin/env "${shell}" "scripts/wad_setup.py" "$@"; 
        exit $?; 
    fi
done
# We didn't find any of them.
echo "ERROR! Need to have python, python3, or python2 installed and in your path!"
exit 1
