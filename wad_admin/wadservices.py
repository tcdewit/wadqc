#!/usr/bin/env python
from __future__ import print_function
import os
import sys
import time
import argparse

from . import wadservices_communicate as wad_com
    
__version__ = 20190313
"""
Changelog:
  20190313: added support for nginx
  20180927: added support for CentOS7
  20180910: added wadapi (start before orthanc)
  20180828: fix order for wadadmin
  20180809: wrapper for apache2 name
  20180316: fixed process status for macOs
  20180214: integrated apache2 commands
  20170922: Added option to display used path, WADROOT, pg_ctl etc.
"""

def applycmd(services, cmd, inifile, pgdata):
    """
    apply cmd to all services
    """
    for s in services:
        # postgres is a special case
        print('* %s %s:...'%(cmd, s), end='')
        sys.stdout.flush()
        if s == 'postgresql':
            print('%s: %s'%(s, wad_com.postgresql(cmd, pgdata)))

        # wadprocessor is a special case
        elif s == 'wadprocessor':
            print('%s: %s'%(s, wad_com.wadcontrol('quit' if cmd=='stop' else cmd, inifile)))
            
        else:
            if cmd == 'status':
                print('%s: %s'%(s, wad_com.status(s)))

            elif cmd == 'stop':
                print('%s: %s'%(s, wad_com.stop(s)))
            
            elif cmd == 'start':
                print('%s: %s'%(s, wad_com.start(s, inifile=inifile)))

            elif cmd == 'info':
                print('%s: %s'%(s, wad_com.info(s)))

def main():
    # do not run as root! the script will ask for permission if it needs root
    if os.name == 'nt':
        pass
    elif os.geteuid() == 0:
        print("Do not run wadservices as root! The script will ask you for root permission if it needs it! Exit.")    
        exit(False)

    print("wadservices v.{}\n".format(__version__))
    print("NOTICE: Changing the state of nginx/httpd/apache2 or systemd controlled services needs root permissions.\n"
          "If root permissions are needed, you will be prompted for your password.\n\n")

    epilog = ""
    parser = argparse.ArgumentParser(description='wadservices.py version %s'%__version__, epilog=epilog)
    validcmds = ['info', 'start', 'stop', 'status', 'restart']
    validservices = ['postgresql', 'orthanc', 'wadadmin', 'waddashboard', 'wadprocessor', 'wadapi', 'all']
    services = 'all'

    if 'WADROOT' in os.environ:
        inifile = os.path.join(os.environ['WADROOT'], 'WAD_QC', 'wadconfig.ini')
        pgdata = os.path.join(os.environ['WADROOT'], 'pgsql','data')
    else: # development
        print("WADROOT cannot be found in the environment. Add it to your .bashrc and try again from a new shell.")
        sys.exit()

    parser.add_argument('-i','--inifile',
                        type=str,
                        default=inifile,
                        help='path to wadconfig.ini file [%s]'%(inifile),dest='inifile')

    parser.add_argument('-d','--data',
                        type=str,
                        default=pgdata,
                        help='path to postgresql data folder [%s]'%(pgdata),dest='pgdata')

    parser.add_argument('-c','--command',
                        type=str,
                        default='',
                        help='command must be one of %s'%('/'.join(validcmds)),dest='cmd')

    parser.add_argument('-s','--services',
                        default=services, type=str,
                        help='apply command to these services [%s]; choose any combination of: %s'%(services,', '.join(validservices)),dest='services')

    args = parser.parse_args()
    cmd = args.cmd
    inifile = args.inifile
    pgdata = args.pgdata
    
    if not cmd in validcmds:
        print('Invalid command "%s"'%cmd)
        parser.print_help()
        sys.exit()

    # make sure the list of services is valid.
    services = args.services.split(',')
    services = [s.strip() for s in services]
    for s in services:
        if not s in validservices:
            print('Invalid service "%s"'%s)
            parser.print_help()
            sys.exit()

    if 'all' in services:
        services = validservices[:-1]

    # remove double services and make sure postgresql is the first one, 
    #  wadadmin must be the last one; this is needed for the order is reversed when stopping
    #  and wadamin might have started some services, so wadadmin must stop before killing the others
    services = list(set(services))
    first = ['postgresql', 'wadapi']
    last = ['wadadmin']
    for s in reversed(first):
        if s in services:
            del services[services.index(s)]
            services.insert(0, s)    
    for s in last:
        if s in services:
            del services[services.index(s)]
            services.append(s)    
    if 'orthanc' in services:
        services[services.index('orthanc')] = 'Orthanc'

    if cmd == 'restart':
        applycmd(list(reversed(services)), 'stop', inifile, pgdata) # stop postgresql last!
        time.sleep(5) # wait 5 seconds for all systems to shutdown
        applycmd(services, 'start', inifile, pgdata)
    elif cmd == 'stop': # stop postgresql last!
        applycmd(reversed(services), cmd, inifile, pgdata)
    else: # info, start, status
        if cmd == 'info':
            print("Using:")
            print("  WADROOT: {}".format(os.environ['WADROOT']))
            print("  inifile: {}".format(args.inifile))
            print("  pgdata: {}\n".format(args.pgdata))
            if 'WADROOT' in os.environ:
                inifile = os.path.join(os.environ['WADROOT'], 'WAD_QC', 'wadconfig.ini')
                pgdata = os.path.join(os.environ['WADROOT'], 'pgsql','data')

        applycmd(services, cmd, inifile, pgdata)


if __name__ == "__main__":
    main()
