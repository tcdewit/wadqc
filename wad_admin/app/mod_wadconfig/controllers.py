# Import flask dependencies
from flask import Blueprint, request, render_template, \
     flash, session, redirect, url_for, Markup

try:
    from app.mod_auth.controllers import login_required
    from app.libs.shared import INIFILE, dbio_connect
    from app.libs import html_elements
    from app.libs.modulerepos import check_releases
    import wadservices_communicate as wad_com

except ImportError:
    from wad_admin.app.mod_auth.controllers import login_required
    from wad_admin.app.libs.shared import INIFILE, dbio_connect
    from wad_admin.app.libs import html_elements
    from wad_admin.app.libs.modulerepos import check_releases
    import wad_admin.wadservices_communicate as wad_com

dbio = dbio_connect() 

import time

# logging
from werkzeug.local import LocalProxy
from flask import current_app
logger = LocalProxy(lambda: current_app.logger)

# Define the blueprint: 'auth', set its url prefix: app.url/auth
mod_blueprint = Blueprint('wadconfig', __name__, url_prefix='/wadadmin')

# Set the route and accepted methods
@mod_blueprint.route('/home/', methods=['GET', 'POST'])
@login_required
def home():
    """
    Just present a menu with choices; should all be require authorization
    """
    menus = [
    ]
    resourcesmenu=[
        { 'label':'server status', 'href':url_for('wadconfig.whatsup') }, #'whatsup'
        { 'label':'data sources', 'href':url_for('wadconfig_sources.default') },
        { 'label':'tags for selector rules', 'href':url_for('wadconfig_ruletags.default') },
        { 'label':'WAD Processor', 'href':url_for('wadconfig_wadprocessor.default', gid=1) }, 
        { 'label':'WAD-QC database', 'href':url_for('wadconfig_dbtool.default') }, 
        { 'label':'User manager', 'href':url_for('auth_users.default') }, 
        { 'label':'Licenses', 'href':url_for('wadconfig.licenses') },
    ]

    configmenu=[
        { 'label':'factory modules', 'href':url_for('wadconfig_factorymodules.default') }, 
        { 'label':'import modules', 'href':url_for('wadconfig_importexport.importing', gid=1) }, 
        { 'label':'export modules', 'href':url_for('wadconfig_importexport.exporting', gid=1) }, 
        { 'label':'backup selectors', 'href':url_for('wadconfig_backuprestore.backingup', gid=1) }, 
        { 'label':'restore selectors', 'href':url_for('wadconfig_backuprestore.restoring', gid=1) }, 
        { 'label':'selectors', 'href':url_for('wadconfig_selectors.default') }, 
    ]


    processmenu=[
        { 'label':'processes', 'href':url_for('wadconfig_processes.default') },
        { 'label':'results', 'href':url_for('wadconfig_results.default') }, 
    ]

    advancedmenu=[
        { 'label':'analysis modules', 'href':url_for('wadconfig_modules.default') },
        { 'label':'module configs',  'href':url_for('wadconfig_configs.default') }, 
        { 'label':'meta configs',  'href':url_for('wadconfig_metas.default') }, 
    ]

    menus.append({'title':'Resources', 'items':resourcesmenu})
    menus.append({'title':'Configuration', 'items':configmenu})
    menus.append({'title':'Processes/Results', 'items':processmenu})
    menus.append({'title':'Advanced', 'items':advancedmenu})
    return render_template("wadconfig/home.html", menus=menus, title='WAD-QC Server Administration')


@mod_blueprint.route('/licenses/')
@login_required
def licenses():
    # show the licenses of the various pieces of software included

    link = html_elements.Link(label="Flask", href="http://flask.pocoo.org/", target="_blank")
    subtitle = "WAD-Admin is a {} app, and makes use of these additional pieces of open source software.".format(link)

    table_rows = []
    table_rows.append(["Bootstrap", 
                       html_elements.Link(label="https://getbootstrap.com/", href="https://getbootstrap.com/", target="_blank"), 
                       html_elements.Link(label="MIT", href=url_for('static', filename='licenses/bootstrap/LICENSE.txt'))])

    table_rows.append(["tablesorter",
                       html_elements.Link(label="https://mottie.github.io/tablesorter/", href="https://mottie.github.io/tablesorter/", target="_blank"), 
                       html_elements.Link(label="MIT", href=url_for('static', filename='licenses/tablesorter/LICENSE.txt'))])

    table_rows.append(["JSON Editor Online", 
                       html_elements.Link(label="http://jsoneditoronline.org/", href="http://jsoneditoronline.org/", target="_blank"), 
                       html_elements.Link(label="Apache License 2.0", href=url_for('static', filename='licenses/jsoneditoronline/LICENSE.txt'))])


    table = html_elements.Table(headers=['Component', 'URL', 'License'], rows=table_rows)
    return render_template("wadconfig/generic.html", title='Licenses', subtitle=Markup(subtitle), html=Markup(table))

@mod_blueprint.route('/whatsup/')
@login_required
def whatsup():
    # show the status of several services
    result = []

    wp_start = html_elements.Button(label='start', href=url_for('.wadcontrol', command='start'))
    wp_pause = html_elements.Button(label='pause', href=url_for('.wadcontrol', command='pause'))
    wp_quit  = html_elements.Button(label='quit', href=url_for('.wadcontrol', command='quit'))

    ort_start = html_elements.Button(label='start', href=url_for('.orthanc', command='start'))
    ort_restart = html_elements.Button(label='restart', href=url_for('.orthanc', command='restart'))
    ort_quit = html_elements.Button(label='quit', href=url_for('.orthanc', command='quit'))

    # psql_start = html_elements.Button(label='start', href=url_for('.pgcontrol', command='start')) # should be running else wadadmin would be down
    psql_reload = html_elements.Button(label='reload', href=url_for('.pgcontrol', command='reload'))
    # psql_quit = html_elements.Button(label='quit', href=url_for('.pgcontrol', command='stop')) # quit will bring wadamin down

    wap_restart = html_elements.Button(label='restart', href=url_for('.sitecontrol', command='restart', service='wadapi'))
    wap_quit = html_elements.Button(label='quit', href=url_for('.sitecontrol', command='stop', service='wadapi'))
    wap_start = html_elements.Button(label='start', href=url_for('.sitecontrol', command='start', service='wadapi'))

    wab_restart = html_elements.Button(label='restart', href=url_for('.sitecontrol', command='restart', service='waddashboard'))
    wab_quit = html_elements.Button(label='quit', href=url_for('.sitecontrol', command='stop', service='waddashboard'))
    wab_start = html_elements.Button(label='start', href=url_for('.sitecontrol', command='start', service='waddashboard'))

    services = [ # process name, identifier on cmdline
                 {'name': 'postgresql', 'process':'postgresql', 'buttons':['', psql_reload]}, # at least for the Orthanc database
                 {'name': 'orthanc', 'process':'Orthanc', 'buttons':[ort_start, ort_restart, ort_quit]}, # preferred PACS solution 
                 {'name': 'wadprocessor', 'process':'wadprocessor', 'buttons':[wp_start, wp_pause, wp_quit]}, # wad_qc processor
                 {'name': 'wadadmin', 'process':'wadadmin', 'buttons':[]}, # wadadmin has no controls
                 {'name': 'wadapi', 'process':'wadapi', 'buttons':[wap_start, wap_restart, wap_quit]}, # wadapi
                 {'name': 'waddashboard', 'process':'waddashboard', 'buttons':[wab_start, wab_restart, wab_quit]}, # waddashboard
                 ]    

    systemd_services = {'orthanc': 'wadorthanc', 'postgresql': 'wadpostgresql', 'wadprocessor': 'wadprocessor'}
    apache2_services = {'wadadmin': 'wadadmin', 'waddashboard': 'waddashboard', 'wadapi': 'wadapi'}

    # set service status to NOT RUNNING
    for service in services:
        status = is_service_running(service['process'])
        if service['name'] in systemd_services.keys():
            systemd = wad_com.controlled_by_systemd(systemd_services[service['name']])
            if systemd:
                result.append({
                    "label": "{} (controlled by systemd)".format(service['name']),
                    "cell": {'value':status[0], 'style': status[1]}, 
                    "buttons": []
                })
            else:
                result.append({
                    "label": service['name'],
                    "cell": {'value':status[0], 'style':status[1]},
                    "buttons":service['buttons']
                })

        if service['name'] in apache2_services.keys():
            if wad_com.controlled_by_apache2(wad_com.get_apache2_name(apache2_services[service['name']])):
                result.append({
                    "label": "{} (controlled by apache2)".format(service['name']),
                    "cell": {'value':status[0], 'style':status[1]}, 
                    "buttons": []
                })
            elif wad_com.controlled_by_nginx(wad_com.get_nginx_name(apache2_services[service['name']])):
                result.append({
                    "label": "{} (controlled by nginx/systemd)".format(service['name']),
                    "cell": {'value':status[0], 'style':status[1]}, 
                    "buttons": []
                })
            else:
                result.append({
                    "label": service['name'],
                    "cell": {'value':status[0], 'style':status[1]},
                    "buttons": service['buttons']
                })

    table_rows = []
    for res in result:
        row = [res['label'], res['cell']]
        row.extend(res['buttons'])
        table_rows.append(row)
    table = html_elements.Table(headers=['Service',   'Status'], rows=table_rows)

    # versions
    hdr2 = html_elements.Heading(label="Versions", type="h2")
    col_ok   = {'bgcolor':'yellowgreen'}
    col_bad =  {'bgcolor':'Red'}

    version_db   = dbio.DBVariables.get(dbio.DBVariables.name == 'iqc_db_version').val
    version_dbio = dbio.__version__
    import pkg_resources
    version_whl = pkg_resources.get_distribution("wad_qc").version
    
    results2 = []
    results2.append({
        "label": 'wad_qc whl',
        "val": version_whl,
        "col": {}
    })
    results2.append({
        "label": 'WAD-QC DataBase',
        "val": version_db, 
        "col": col_ok if version_db>=version_dbio else col_bad
    })
    results2.append({
        "label": 'wad_qc.dbio',
        "val": version_dbio, 
        "col": col_ok if version_dbio>=version_db else col_bad
    })

    whl_updates = request.args.get('whl_update', None)
    if whl_updates is None:
        whl_updates = html_elements.Button(label='check for update', href=url_for('.whl_updates', current=version_whl))
    table_rows2 = []
    for res in results2:
        row = [res["label"], {'value':res['val'], 'style':res['col']}]
        if row[0] == 'wad_qc whl':
            row.append(whl_updates)
        table_rows2.append(row)
    table2 = hdr2+html_elements.Table(headers=['Item',   'Version'], rows=table_rows2)

    table = str(table)+'<BR>'+str(table2)

    return render_template("wadconfig/generic.html", title='Whatsup', html=Markup(table))

def is_service_running(service):
    # show the status of several services
    # service = cli identifier
    col =  {'bgcolor':'Red'}
    if service == 'wadprocessor':
        stat = wad_com.wadcontrol('status', INIFILE)
        if stat == 'ERROR':
            stat = 'stopped'
    elif service == 'postgresql':
        stat = wad_com.postgresql('status')
    else:
        stat = wad_com.status(service)

    if stat == 'paused': 
        col = {'bgcolor':'yellow'}
    elif stat == 'running': 
        col = {'bgcolor':'yellowgreen'}

    if stat in ['enabled', 'enabled/running']:
        col = {'bgcolor':'yellowgreen'}

    status = (stat, col)

    return status

@mod_blueprint.route('/whl_updates/')
@login_required
def whl_updates():
    """
    check for updates
    """
    _current = request.args.get('current', None)
    if _current is None:
        return redirect(url_for('.whatsup'))
    results, msg = check_releases(dbio, _current, logger=logger)
    if not 'update_available' in results:
        return redirect(url_for('.whatsup', whl_update=msg))
    if results['update_available']:
        return redirect(url_for('.whatsup', whl_update="Version {} available on WAD-QC website.".format(results['latest'])))
    elif results.get('unreleased', False):
        return redirect(url_for('.whatsup', whl_update="This is an unreleased version. Latest release is version {}".format(results['latest'])))
    return redirect(url_for('.whatsup', whl_update="This is the latest version."))

@mod_blueprint.route('/wadcontrol/')
@login_required
def wadcontrol():
    _command = request.args['command'] if 'command' in request.args else None
    if not _command in ['start', 'quit', 'status', 'pause']:
        return redirect(url_for('.whatsup'))

    wad_com.wadcontrol(_command, INIFILE)
    return redirect(url_for('.whatsup'))

@mod_blueprint.route('/sitecontrol/')
@login_required
def sitecontrol():
    _command = request.args.get('command', None)
    _site = request.args.get('service', None)

    if not _site in ['waddashboard', 'wadapi']: # no controls for wadadmin!
        return redirect(url_for('.whatsup'))
    
    if not _command in ['start', 'stop', 'restart']:
        return redirect(url_for('.whatsup'))

    if _command == 'restart':
        wad_com.stop(_site)
        time.sleep(5) # wait 5 seconds for all systems to shutdown
        wad_com.start(_site)
    else:
        if _command == 'start':
            wad_com.start(_site)
        elif _command == 'stop':
            wad_com.stop(_site)

    return redirect(url_for('.whatsup'))

@mod_blueprint.route('/pgcontrol/')
@login_required
def pgcontrol():
    _command = request.args['command'] if 'command' in request.args else None
    if not _command in ['start', 'status', 'reload', 'stop']:
        return redirect(url_for('.whatsup'))

    wad_com.postgresql(_command)
    return redirect(url_for('.whatsup'))

@mod_blueprint.route('/orthanc/')
@login_required
def orthanc():
    _command = request.args['command'] if 'command' in request.args else None
    if not _command in ['start', 'quit', 'restart']:
        return redirect(url_for('.whatsup'))

    if _command == 'start':
        wad_com.start('Orthanc', INIFILE)
    elif _command == 'quit':
        wad_com.stop('Orthanc')
    elif _command == 'restart':
        wad_com.stop('Orthanc')
        time.sleep(5) # wait 5 seconds for all systems to shutdown
        wad_com.start('Orthanc', INIFILE)

    return redirect(url_for('.whatsup'))
