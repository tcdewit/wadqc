from flask import Blueprint, render_template, Markup, url_for, redirect, request, flash, send_file
try:
    from app.mod_auth.controllers import login_required
    from app.libs.shared import dbio_connect, INIFILE, bytes_as_string, string_as_bytes
    from app.libs.configmaintenance import validate_json
except ImportError:
    from wad_admin.app.mod_auth.controllers import login_required
    from wad_admin.app.libs.shared import dbio_connect, INIFILE, bytes_as_string, string_as_bytes
    from wad_admin.app.libs.configmaintenance import validate_json
dbio = dbio_connect()

import os
import tempfile
from io import BytesIO
from peewee import IntegrityError
import json, jsmin

from .forms_editor import EditorForm

# logging
from werkzeug.local import LocalProxy
from flask import current_app
logger = LocalProxy(lambda: current_app.logger)

mod_blueprint = Blueprint('editor', __name__, url_prefix='/wadadmin')

class WDException(Exception):
    def __init__(self,*args,**kwargs):
        Exception.__init__(self,*args,**kwargs)

@mod_blueprint.route('/editor/', methods=['GET', 'POST'])
@login_required
def default():
    # display and allow editing of json
    _cid = int(request.args['cid']) if 'cid' in request.args else None # id of config
    _mid = int(request.args['mid']) if 'mid' in request.args else None # id of meta

    # invalid request are to be ignored
    if (_cid is None and _mid is None) or (not _cid is None and not _mid is None):
        logger.error("Must supply a config id OR a meta id")
        # go back to overview page
        return redirect(url_for('wadconfig.home')) # catch wild call

    title = None
    dajson = None
    fname = None
    opt = {}
    if _mid is None:
        try:
            config = dbio.DBModuleConfigs.get_by_id(_cid)
        except dbio.DBModuleConfigs.DoesNotExist:
            logger.error("Must supply a valid config id")
            # go back to overview page
            return redirect(url_for('wadconfig.home')) # catch wild call
            
        if config:
            dajson = config.val
            fname = config.name
            title = "Edit config: \"{}\"".format(fname)
            opt = {'cid': _cid}
        else:
            logger.error("Must supply a valid config id")
            # go back to overview page
            return redirect(url_for('wadconfig.home')) # catch wild call

    else:
        try:
            meta = dbio.DBMetaConfigs.get_by_id(_mid)
        except dbio.DBMetaConfigs.DoesNotExist:
            logger.error("Must supply a valid meta id")
            # go back to overview page
            return redirect(url_for('wadconfig.home')) # catch wild call
        if meta:
            dajson = meta.val
            fname = 'meta'
            title = 'Edit meta'
            opt = {'mid': _mid}
            if len(meta.module_configs) > 0:
                fname = meta.module_configs[0].name
                title = "Edit meta: \"{}\"".format(fname)
        else:
            logger.error("Must supply a valid meta id")
            # go back to overview page
            return redirect(url_for('wadconfig.home')) # catch wild call
    
    dajson = json.dumps(json.loads(jsmin.jsmin(bytes_as_string(dajson))))

    subtitle = ''#dajson
    page = ''

    form = EditorForm(None if request.method=="GET" else request.form)

    return render_template("wadconfig/jsonedit.html", title=title, subtitle='', msg='',
                           metaformat=dbio.METAFORMAT,cfgformat=dbio.CFGFORMAT, html=Markup(page),
                           inpanel={'type': "panel-info", 'title': "info", 'content':subtitle},
                           form=form,
                           json=Markup(dajson), fname=fname, witheditor=True, **opt)
    
@login_required
@mod_blueprint.route('/editor/download/', methods=['GET', 'POST'])
def download_from_editor():
    # download the json as shown in editor
    title = "ERROR"
    subtitle = ""
    page = ""

    try:
        data = json.loads(request.form.get('downloadform-posttable', None))
        if data is None:
            msg = "No data supplied"
            subtitle = msg
            raise WDException(msg)

        _dajson = data.get('json', None)
        _fname = data.get('fname', None)

        if not _dajson:
            msg = "Must supply a json"
            subtitle = msg
            raise WDException(msg)

        if not _fname:
            msg = "Must supply a fname"
            subtitle = msg
            raise WDException(msg)

        # make a bit more pretty
        blob = json.loads(_dajson)
        blob = string_as_bytes(json.dumps(blob, indent=4))
    
        return send_file(BytesIO(blob), as_attachment=True, attachment_filename="{}.json".format(_fname))#mimetype=None

    except WDException as e:
        logger.error(str(e))

    if title =="ERROR":
        inpanel = {'type': "panel-danger", 'title': "ERROR", 'content':subtitle}
    else:
        inpanel = {'type': "panel-success", 'title': "Succes", 'content':subtitle}
        
    return render_template("wadconfig/generic.html", title=title, subtitle='', msg='', html=Markup(page),
                           inpanel=inpanel)

@login_required
@mod_blueprint.route('/editor/store/', methods=['GET', 'POST'])
def store_from_editor():
    # store the json as shown in editor as the indicated config
    title = "ERROR"
    subtitle = ""
    page = ""

    try:
        data = json.loads(request.form.get('sendform-posttable', None))
        if data is None:
            msg = "No data supplied"
            subtitle = msg
            raise WDException(msg)
            
        _dajson = data.get('json', None)
        _cid = int(data['cid']) if 'cid' in data else None # id of config
        _mid = int(data['mid']) if 'mid' in data else None # id of meta

        # invalid request are to be ignored
        if (_cid is None and _mid is None) or (not _cid is None and not _mid is None):
            msg = "Must supply a config id or a meta id"
            subtitle = msg
            raise WDException(msg)
    
        if not _dajson:
            msg = "Must supply a json"
            subtitle = msg
            raise WDException(msg)
    
        # store config
        if _mid is None:
            try:
                config = dbio.DBModuleConfigs.get_by_id(_cid)
            except dbio.DBModuleConfigs.DoesNotExist:
                msg = '"{}" is not a valid config id!'.format(_cid)
                subtitle = msg
                raise WDException(msg)
                
            if config:
                # validate config
                valid, msg = validate_json("configfile", _dajson)
                if valid:
                    blob = json.loads(jsmin.jsmin(_dajson))
                else:
                    msg = 'Refusing to store invalid json as config! {}'.format(msg)
                    subtitle = msg
                    raise WDException(msg)
                    
                # save config; check if config is in use first
                # inuse = (len(config.selectors)+len(config.processes)+len(config.results))>0
                #  only inuse if coupled to result/process; for selector it does not matter,
                #   clones are to reproduce results
                inuse = (len(config.processes)+len(config.results))>0
                clonedmsg = ''
                if inuse or config.origin == 'factory': 
                    # changed config: clone config (already clones meta)
                    nw_config = dbio.DBModuleConfigs.get_by_id(config.id).clone()
                    if inuse:
                        clonedmsg = 'Config was in use, so stored a new config'
                    elif config.origin == 'factory':
                        clonedmsg = 'Config was a factory one, so stored a new config'
                    if len(config.selectors)>0:
                        clonedmsg += ' and coupled selectors point to the new config'
                    clonedmsg += '.'

                    nw_config.origin = 'result' if inuse else 'user' # if it was a factory config is is now 'user'
                    nw_config.val = json.dumps(blob)
                    nw_config.save()

                    # change pointers of selectors
                    for sel in config.selectors:
                        sel.module_config = nw_config.id
                        sel.save()
                else: 
                    config.val = json.dumps(blob)
                    config.save()
    
                title = 'Success'
                subtitle = 'Stored modified config "{}" in database. {}'.format(config.name, clonedmsg)
            else:
                msg = '"{}" is not a valid config id!'.format(_cid)
                subtitle = msg
                raise WDException(msg)
        
        else:
            try:
                meta = dbio.DBMetaConfigs.get_by_id(_mid)
            except dbio.DBMetaConfigs.DoesNotExist:
                msg = '"{}" is not a valid meta id!'.format(_mid)
                subtitle = msg
                raise WDException(msg)

            if meta:
                # validate meta
                valid, msg = validate_json("metafile", _dajson)
                if valid:
                    blob = json.loads(jsmin.jsmin(_dajson))
                else:
                    msg = 'Refusing to store invalid json as meta! {}'.format(msg)
                    subtitle = msg
                    raise WDException(msg)

                # save config
                meta.val = json.dumps(blob)
                meta.save()
    
                title = 'Success'
                subtitle = 'Stored modified meta '
                if len(meta.module_configs) > 0:
                    fname = meta.module_configs[0].name
                    subtitle += '"{} "'.format(fname)
                subtitle += 'in database.'
            else:
                msg = '"{}" is not a valid meta id!'.format(_mid)
                subtitle = msg
                raise WDException(msg)
    
    except WDException as e:
        logger.error(str(e))

    if title =="ERROR":
        inpanel = {'type': "panel-danger", 'title': "ERROR", 'content':subtitle}
    else:
        inpanel = {'type': "panel-success", 'title': "Succes", 'content':subtitle}
        
    return render_template("wadconfig/generic.html", title=title, subtitle='', msg='', html=Markup(page),
                           inpanel=inpanel)
