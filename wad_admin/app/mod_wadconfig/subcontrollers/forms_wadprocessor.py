# Import Form
from flask_wtf import FlaskForm 

# Import Form elements such as TextField and BooleanField (optional)
from wtforms import IntegerField, FloatField, StringField, FileField, HiddenField # BooleanField

# Import Form validators
from wtforms.validators import NumberRange, Required, NoneOf

class ModifyForm(FlaskForm):
    interval = IntegerField('interval [s]', [NumberRange(min=1, message='Interval must be at least 1 s.')])
    timeout  = FloatField('timeout [s]', [NumberRange(min=1., message='Timeout must be at least 1.0 s.')])
    workers  = IntegerField('workers', [NumberRange(min=1, message='Number of workers must be larger than 0.')])
    gid = HiddenField('gid', [])
    
