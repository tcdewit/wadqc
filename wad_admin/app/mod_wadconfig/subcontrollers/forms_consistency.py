# Import Form 
from flask_wtf import FlaskForm
from wtforms import HiddenField, FormField

class ResendForm(FlaskForm):
    posttable = HiddenField()

class DeleteForm(FlaskForm):
    posttable = HiddenField()

class ConsistencyForm(FlaskForm):
    resendform = FormField(ResendForm)
    deleteform = FormField(DeleteForm)
