# Import Form 
from flask_wtf import FlaskForm 

# Import Form elements such as TextField 
from wtforms import StringField, HiddenField, SelectField, BooleanField, IntegerField, FieldList, FormField, FloatField

# Import Form validators
from wtforms.validators import Required, NoneOf, NumberRange, Optional

try:
    from ..models import WARuleTags
    from app.libs.shared import dbio_connect
except ImportError:
    from wad_admin.app.mod_wadconfig.models import WARuleTags
    from wad_admin.app.libs.shared import dbio_connect
dbio = dbio_connect()

logic_choices = [(m.id, m.name) for m in dbio.DBSelectorLogics.select().order_by(dbio.DBSelectorLogics.name)]
tag_choices = [(m.val, m.name) for m in WARuleTags.select().order_by(WARuleTags.name)]

class EqualsEntryForm(FlaskForm):
    lim_name = HiddenField()
    lim_val = StringField()
    
class PeriodEntryForm(FlaskForm):
    lim_name = HiddenField()
    lim_val = IntegerField()

class RefMinLowHighMaxEntryForm(FlaskForm):
    lim_name = HiddenField()
    ref_val = FloatField('', [Optional()]) # allow empty field
    min_val = FloatField('', [Optional()]) # allow empty field
    low_val = FloatField('', [Optional()]) # allow empty field
    high_val = FloatField('', [Optional()]) # allow empty field
    max_val = FloatField('', [Optional()]) # allow empty field

class MinLowHighMaxEntryForm(FlaskForm):
    lim_name = HiddenField()
    min_val = FloatField('', [Optional()]) # allow empty field
    low_val = FloatField('', [Optional()]) # allow empty field
    high_val = FloatField('', [Optional()]) # allow empty field
    max_val = FloatField('', [Optional()]) # allow empty field

class ParamEntryForm(FlaskForm):
    pindent = HiddenField()
    pname = HiddenField()
    pvalue = StringField()
    pinfo = HiddenField()
    ptrail = HiddenField() # list of indexes of param in config
    
class ActionEntryForm(FlaskForm):
    action = HiddenField()
    params = FieldList(FormField(ParamEntryForm))

class RuleEntryForm(FlaskForm):
    tag    = SelectField(choices = tag_choices)
    logic  = SelectField(choices = logic_choices, coerce=int)
    value0 = StringField()
    value1 = StringField()
    value2 = StringField()
    value3 = StringField()
    value4 = StringField()
    delrule = BooleanField('delete rule')
    ruleid = HiddenField()
 
    def __init__(self, *args, **kwargs):
        super(RuleEntryForm, self).__init__(*args, **kwargs)
        self.delrule.label = 'delete'

class ModifyForm(FlaskForm):
    name = StringField('name', [Required(message='Name cannot be empty!'), NoneOf(['None'],message='Name cannot be None!')])
    isactive = BooleanField('isactive')
    description = StringField('description', [])
    module_config = SelectField('config', coerce=int )
    currentname = HiddenField('currentname', [])
    gid = HiddenField('gid', [])
    cid = HiddenField('cid', [])
    actions = FieldList(FormField(ActionEntryForm))
    rules = FieldList(FormField(RuleEntryForm))
    addnum = IntegerField('extra rules to add:', [NumberRange(message='Number of extra rules cannot be empty!', min=0)])
    equals = FieldList(FormField(EqualsEntryForm))
    minlowhighmaxs = FieldList(FormField(MinLowHighMaxEntryForm))
    refminlowhighmaxs = FieldList(FormField(RefMinLowHighMaxEntryForm))
    periods = FieldList(FormField(PeriodEntryForm))
