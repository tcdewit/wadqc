from flask import Blueprint, render_template, url_for, redirect, request, send_file, send_from_directory, flash
import os
import tempfile
from io import BytesIO
try:
    from app.mod_auth.controllers import login_required
    from app.libs.shared import dbio_connect, upload_file
    from app.libs.exchange import export_configs, import_configs
except ImportError:
    from wad_admin.app.mod_auth.controllers import login_required
    from wad_admin.app.libs.shared import dbio_connect, upload_file
    from wad_admin.app.libs.exchange import export_configs, import_configs
dbio = dbio_connect()

# logging
from werkzeug.local import LocalProxy
from flask import current_app
logger = LocalProxy(lambda: current_app.logger)

# Import modify forms
from .forms_importexport import ExportForm, ImportForm

mod_blueprint = Blueprint('wadconfig_importexport', __name__, url_prefix='/wadadmin')

@mod_blueprint.route('/export/', methods=['GET', 'POST'])
@login_required
def exporting():
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    # invalid table request are to be ignored
    formtitle = 'Export modules and configurations'
    configs = getConfigs()
    form = ExportForm(None if request.method=="GET" else request.form, configs=configs)
    if form.gid.data is None:
        if _gid is None:
            return redirect(url_for('wadconfig.home')) # catch wild call
        form.gid.data = 1

    # Verify the sign in form
    valid = True
    if form.validate_on_submit():
        if valid:
            # the elements of the form
            field_dict = {k:v for k,v in request.form.items()}
            result = doExport(field_dict)
            if result is None:
                valid = False
            else:
                return result
            
    return render_template("wadconfig/export.html", form=form, action='.', #action=url_for('.upload_file'),
                           title=formtitle, msg='Select configs to export (modules will be added automatically) and click Export')

@mod_blueprint.route('/import/', methods=['GET', 'POST'])
@login_required
def importing():
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    # invalid table request are to be ignored
    formtitle = 'Import modules and configurations'
    form = ImportForm(None if request.method=="GET" else request.form)
    if form.gid.data is None:
        if _gid is None:
            return redirect(url_for('wadconfig.home')) # catch wild call
        form.gid.data = 1

    # Verify the sign in form
    valid = True
    if form.validate_on_submit():
        if not 'file' in request.files or len(request.files['file'].filename) == 0:
            flash('No valid zip file chosen.', 'error')
            valid = False
        if valid:
            inname = upload_file(request.files['file'])
            msg = doImport(inname)
            logger.info(msg)
            if  msg == 'Success':
                return render_template("wadconfig/generic.html", title=formtitle, subtitle='', msg='', html="",
                                       inpanel={'type': "panel-success", 'title': "Success", 
                                                'content':"Successfully imported {}".format(os.path.basename(request.files['file'].filename))})
            else:
                flash(msg, 'error')
            
    return render_template("wadconfig/import.html", form=form, action='.', #action=url_for('.upload_file'),
                           title=formtitle, msg='Select package with configs and modules to import click Import')

def doImport(inname):
    """
    return success or not.
    check if zip
    unpack zip
      check if manifest.ini
      check consistency of manifest
      check consistency of manifest with folders modules and configs; check if exe file present in folder
      check uniqueness of new module names (if not, suggest user renames current modules, refuse import)
        create each module (zip for upload?!)
        create each config coupled to correct module
    """
    valid, msg = import_configs(dbio, inname, logger)

    # remove zip
    os.remove(inname)
    
    return msg

def getConfigs(selectors_only=False):
    """
    Return a list of configs suitable for export
    """
    stuff = dbio.DBModuleConfigs.select().order_by(dbio.DBModuleConfigs.id)
    
    results = []
    for cfg in stuff:
        if selectors_only:
            if len(cfg.selectors) == 0:
                continue
        res = {'cfg_name':cfg.name, 
                'cfg_description':cfg.description,
                'cfg_origin':cfg.origin,
                'cfg_selector': cfg.selectors[0].name if len(cfg.selectors)>0 else '',
                'cfg_hasmeta': not cfg.meta is None,
                'cid':cfg.id}

        # exclude configs coupled to results, but not to a current selector (meaning replaced)
        if res['cfg_origin'] == 'result' and res['cfg_selector'] == '':
            continue
        else:
            results.append(res)
    return results
    
def doExport(field_dict):
    """
    Pack all selected configs and connected modules with a manifest.ini file and send
    Export:
      manifest.json
      modules/module_1/
              module_2/
      configs/config_1.json
             /config_2.json
    """
    cidsuffix = '-cid'
    selsuffix = '-cfg_selected'
    cfg_ids = []
    for key, entry in field_dict.items():
        if key.endswith(selsuffix): # it's either marked and present or not present
            cidkey = key[:(len(key)-len(selsuffix))]+cidsuffix
            cfg_ids.append(int(field_dict[cidkey]))
    
    # make the Backup location
    modules_dir = dbio.DBVariables.get_by_name('modules_dir').val
    temp_dir = dbio.DBVariables.get_by_name('temp_dir').val
    if not os.path.exists(temp_dir):
        os.makedirs(temp_dir)

    # make a temporary file; either stream it of write it to disk
    with tempfile.SpooledTemporaryFile(dir=temp_dir) as tmpf:
        error, msg = export_configs(dbio, tmpf, cfg_ids, logger)

        if not error:
            # Reset file pointer
            tmpf.seek(0)
    
            return send_file(BytesIO(tmpf.read()), as_attachment=True, attachment_filename='export.zip')#mimetype=None

    logger.error(msg)
    return None
