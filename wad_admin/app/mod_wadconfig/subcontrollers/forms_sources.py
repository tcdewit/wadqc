# Import Form
from flask_wtf import FlaskForm 

# Import Form elements such as TextField and BooleanField (optional)
from wtforms import StringField, SelectField, HiddenField, IntegerField, PasswordField, validators, BooleanField

# Import Form validators
from wtforms.validators import Required, NoneOf

try:
    from app.libs.shared import dbio_connect
except ImportError:
    from wad_admin.app.libs.shared import dbio_connect
dbio = dbio_connect()

src_choices = [(m.id, m.name) for m in dbio.DBSourceTypes.select()]
protocol_choices = [(m, m) for m in ['http', 'https'] ]

class ModifyForm(FlaskForm):
    name = StringField('name:', [Required(message='Name cannot be empty!'), NoneOf(['None'],message='Name cannot be None!')])
    aetitle = StringField('aetitle:', [])
    source_type = SelectField("source_type:", choices = src_choices, coerce=int )
    protocol = SelectField("protocol:", choices = protocol_choices )
    host = StringField('host:', [])
    port = IntegerField('port:', [])
    user = StringField('user:', [])
    pswd = PasswordField('password:', [
        #validators.DataRequired('Password is required'),
        validators.EqualTo('confirm', message='Passwords must match')
    ])
    confirm = PasswordField('repeat password:')
    modifylocal = BooleanField('modify local orthanc.json and restart Orthanc:')
    
    currentname = HiddenField('gid', [])
    gid = HiddenField('gid', [])
    
