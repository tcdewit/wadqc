# Import Form 
from flask_wtf import FlaskForm 
from flask import Markup
# Import Form elements such as TextField 
from wtforms import HiddenField, BooleanField, FieldList, FormField, FileField

# Import Form validators
from wtforms.validators import Required, NoneOf, NumberRange

class SelectorEntryForm(FlaskForm):
    # prefix with cfg_ to prevent no display due to clashes.
    sel_name = HiddenField()
    sel_description = HiddenField()
    sel_isactive = HiddenField()
    cid = HiddenField()
    sel_selected = BooleanField('include in backup')

class BackupForm(FlaskForm):
    gid = HiddenField('gid', [])
    selectors = FieldList(FormField(SelectorEntryForm))

class RestoreForm(FlaskForm):
    gid = HiddenField('gid', [])
    file = FileField('File', [])  
    skip_modules = BooleanField('do not restore modules (link to existing modules with same name)')
