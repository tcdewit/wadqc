from flask import Blueprint, render_template, Markup, url_for, redirect, request, send_file, flash
import os
try:
    from app.mod_auth.controllers import login_required
    from app.libs import html_elements
    from app.libs.shared import dbio_connect, upload_file, bytes_as_string, string_as_bytes
    from app.libs import configmaintenance
except ImportError:
    from wad_admin.app.mod_auth.controllers import login_required
    from wad_admin.app.libs import html_elements
    from wad_admin.app.libs.shared import dbio_connect, upload_file, bytes_as_string, string_as_bytes
    from wad_admin.app.libs import configmaintenance
dbio = dbio_connect()

# logging
from werkzeug.local import LocalProxy
from flask import current_app
logger = LocalProxy(lambda: current_app.logger)

# Import modify forms
from .forms_configs import ModifyForm
from .forms_confirm import ConfirmForm
from .forms_consistency import ConsistencyForm

import json
from io import BytesIO

mod_blueprint = Blueprint('wadconfig_configs', __name__, url_prefix='/wadadmin')

@mod_blueprint.route('/configs/', methods=['GET', 'POST'])
@login_required
def default():
    # display and allow editing of modules table
    subtitle=[
        'Deleting a module_config sets off a cascade: It also deletes all entries '\
        'that directly reference that version of the module_config, and all other entries that reference those entries. '\
        'The number of references is shown in coupled selector, #processes and #results. ',
        'A module_config coupled to a Selector cannot be deleted.',
        "Use the filterboxes in the table and the button below the table to delete a selection of Configs."
    ]
           
    stuff = dbio.DBModuleConfigs.select().order_by(dbio.DBModuleConfigs.id)

    table_rows = []
    for data in stuff:
        selname = ''
        if len(data.selectors) >0:
            selname = data.selectors[0].name

        meta = None
        if not data.meta is None:      
            meta = html_elements.Link(label='meta', href=url_for('editor.default', mid=data.meta.id))
        table_rows.append([data.id, html_elements.Link(label=data.name, href=url_for('editor.default', cid=data.id)), data.description, 
                           data.data_type.name,
                           data.origin, data.module.name, meta, 
                           selname, len(data.processes), len(data.results),
                           html_elements.Button(label='clone', href=url_for('.duplicate', gid=data.id)),
                           '' if len(data.selectors) >0 else html_elements.Button(label='delete', href=url_for('.delete', gid=data.id)),
                           html_elements.Button(label='configure', href=url_for('.modify', gid=data.id)),
                           ])

    table = html_elements.Table(headers=['id', 'name: view/edit', 'description', 'data_type', 'origin', 
                                         'module_name', 'meta: view/edit',
                                         'coupled_selector', '#processes', '#results'], rows=table_rows,
                             _class='tablesorter-wadred', _id='sortTable')

    newbutton = html_elements.Button(label='New', href=url_for('.modify'))
    page = table+newbutton
    
    form = ConsistencyForm(None if request.method=="GET" else request.form)

    return render_template("wadconfig/consistency.html", title="Configs", subtitle='', msg='', html=Markup(page),
                           inpanel={'type': "panel-warning", 'title': "WARNING", 'content':subtitle},
                           form=form, action_delete=url_for('.multi_delete'), label_delete="Delete shown Configs")

@mod_blueprint.route('/configs/duplicate/')
@login_required
def duplicate():
    """
    clone given id to new config, changing name and origin
    """
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    # invalid table request are to be ignored
    if not _gid is None:
        nw_cfg = dbio.DBModuleConfigs.get_by_id(_gid).clone()
        nw_cfg.name = 'clone of '+nw_cfg.name
        nw_cfg.origin = 'user'
        nw_cfg.save()

    # go back to overview page
    return redirect(url_for('.default'))


@mod_blueprint.route('/configs/delete/', methods=['GET', 'POST'])
@login_required
def delete():
    _gid = int(request.args['gid']) if 'gid' in request.args else None
    
    # invalid table request are to be ignored
    if _gid is None:
        logger.error("Delete config called with no valid config id")
        return redirect(url_for('.default'))
    
    try:
        cfg = dbio.DBModuleConfigs.get_by_id(_gid)
        if not cfg:
            logger.error("Delete config called with no valid config id")
            return redirect(url_for('.default'))
    except dbio.DBModuleConfigs.DoesNotExist:
        logger.error("Delete config called with no valid config id")
        return redirect(url_for('.default'))
        
    # refuse to delete config coupled to Selector
    if len(cfg.selectors) > 0:
        page = [
            'Config {} is coupled to Selector "{}". Deleting this Config would also delete that Selector '
            'and all its Results.'.format(cfg.id, cfg.selectors[0].name),
            'If you do want to do that, delete Selector "{}". Else, couple the Selector "{}" '
            'to a different Config, and then delete this Config.'
            ''.format(cfg.selectors[0].name, cfg.selectors[0].name)
        ]
        return render_template("wadconfig/generic.html", title='Delete Config',  subtitle='', msg='', html='',
                           inpanel={'type': "panel-danger", 'title': "ERROR", 'content':page})

    # ask for confirmation if coupled to results or processes
    if (len(cfg.processes)+len(cfg.results))>0:
        # invalid table request are to be ignored
        formtitle = 'Confirm action: delete config with {} coupled results and {} coupled processes'.format(len(cfg.results), len(cfg.processes))
        msg = [
            'This will also delete those results and processes.',
            'Tick confirm and click Submit to proceed.'
        ]
        form = ConfirmForm(None if request.method=="GET" else request.form)
    
        # Verify the sign in form
        valid = True
        if form.validate_on_submit():
            # check if this is a new module
            if form.confirm.data is False:
                flash('Must tick confirm!', 'error')
                valid = False
    
            if valid:
                # do stuff
                cfg.delete_instance(recursive=True)
                # go back to overview page
                return redirect(url_for('.default'))
    
        return render_template("wadconfig/confirm.html", form=form, 
                               action=url_for('.delete', gid=_gid),
                               title=formtitle, msg=msg)

    else:
        cfg.delete_instance(recursive=True)
        # go back to overview page
        return redirect(url_for('.default'))
            
@mod_blueprint.route('/configs/download/')
@login_required
def download():
    """
    download given id of given table from iqc db
    """
    _gid = int(request.args['cfgid']) if 'cfgid' in request.args else None
    
    # invalid table request are to be ignored
    if _gid is None:
        return redirect(url_for('.default'))

    row = dbio.DBModuleConfigs.get_by_id(_gid)
    # find the elements of the config.json to download
    filename = row.name+'.json'
    blob = row.val #str(base64.b64decode(row['val']))

    # make a bit more pretty
    blob = json.loads(bytes_as_string(blob))
    blob = string_as_bytes(json.dumps(blob, sort_keys=True, indent=4))

    return send_file(BytesIO(blob), as_attachment=True, attachment_filename=filename)#mimetype=None

@mod_blueprint.route('/configs/modify/', methods=['GET', 'POST'])
@login_required
def modify():
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    # invalid table request are to be ignored
    formtitle = 'Configure module config'
    form = ModifyForm(None if request.method=="GET" else request.form)

    modules = [(m.id, m.name) for m in dbio.DBModules.select()]
    form.module.choices = [(m.id, m.name) for m in dbio.DBModules.select()]
    form.datatype.choices = [(m.id, m.name) for m in dbio.DBDataTypes.select()]
    if not _gid is None:
        config = dbio.DBModuleConfigs.get_by_id(_gid)
        form.name.data = config.name
        form.currentname.data = config.name
        form.description.data = config.description
        form.gid.data = _gid
        form.module.data = config.module.id
        form.datatype.data = config.data_type.id
        formtitle += " \"{}\"".format(config.name)

    new_entry = False
    if form.gid is None or form.gid.data == ''  or form.gid.data is None: #define here, to avoid wrong label on redisplaying a form with errors
        new_entry = True
        formtitle = 'New module config'
 
    existingnames = [v.name for v in dbio.DBModuleConfigs.select()]

    # Verify the form
    valid = True
    if form.validate_on_submit():
        # check if this is a new module
        if new_entry:
            if form.name.data in existingnames:
                flash('A config with this name already exist.', 'error')
                valid = False
            if not 'configfile' in request.files or len(request.files['configfile'].filename) == 0:
                flash('No configfile chosen.', 'error')
                valid = False
        else: # not a new form
            pass

        field_dict = {k:v for k,v in request.form.items()}
        blob = None
        meta = None
        if valid:
            if 'configfile' in request.files and len(request.files['configfile'].filename) > 0:
                outname = upload_file(request.files['configfile'])  
                if not outname is None:
                    try:
                        with open(outname,'r') as fcfg: blob = fcfg.read()
                    except Exception as e:
                        flash('{} is not a valid config.json'.format(request.files['configfile'].filename), 'error')
                        valid = False
                        msg = ""
                    os.remove(outname)
                if valid:
                    valid, msg = configmaintenance.validate_json('configfile', blob)
                    if not valid:
                        flash('{} {}'.format(request.files['configfile'].filename, msg), 'error')
                
        if valid:
            if 'metafile' in request.files and len(request.files['metafile'].filename) > 0:
                outname = upload_file(request.files['metafile'])
                try:
                    with open(outname,'r') as fcfg: meta = fcfg.read()
                except Exception as e:
                    flash('{} is not a valid meta.json'.format(request.files['metafile'].filename), 'error')
                    valid = False
                    msg = ""
                os.remove(outname)
            if valid:
                valid, msg = configmaintenance.validate_json('metafile', meta)
                if not valid:
                    flash('{} {}'.format(request.files['metafile'].filename, msg), 'error')
            
        if valid:
            igid = 0
            if 'gid' in field_dict:
                try:
                    igid = int(field_dict['gid'])
                except:
                    pass
                
            if igid>0: # update, not create
                # check if a new config should be created or not:
                #  if the config is coupled and the module or the blob has changed, 
                #  then a new config should be created. Or should a coupled config remain unalterable
                #  for ever? A blob or module change can only occur if uncoupled?
                config = dbio.DBModuleConfigs.get_by_id(igid)
                inuse = (len(config.selectors)+len(config.processes)+len(config.results))>0
                if inuse:
                    if not blob is None or not config.module.id == int(field_dict['module']) or not config.data_type.id == int(field_dict['datatype']):
                        flash('Cannot change module or datatype or upload new file for a module config that is coupled.', 'error')  
                        return render_template("wadconfig/configs_modify.html", form=form, action='.', #action=url_for('.upload_file'),
                                           title=formtitle, msg='Fill out the fields and click Submit')
                    
                config.name = field_dict['name']
                config.description = field_dict['description']
                config.module = int(field_dict['module'])
                config.data_type = int(field_dict['datatype'])
                config.origin = 'user'
                if not blob is None:
                    config.val = blob
                if not meta is None:
                    nw_meta = dbio.DBMetaConfigs.create(val=meta)
                    config.meta = nw_meta.id
                config.save()
            else:
                field_dict['val'] = blob
                if not meta is None:
                    nw_meta = dbio.DBMetaConfigs.create(val=meta)
                else:
                    nw_meta = dbio.DBMetaConfigs.create()
                field_dict['meta'] = nw_meta.id
                field_dict['data_type'] = int(field_dict['datatype'])
                field_dict['origin'] = 'user'
                dbio.DBModuleConfigs.create(**field_dict)
            return redirect(url_for('.default'))
            
    return render_template("wadconfig/configs_modify.html", form=form, action='.', #action=url_for('.upload_file'),
                           title=formtitle, msg='Fill out the fields and click Submit')

@mod_blueprint.route('/configs/multi_delete', methods=['GET', 'POST'])
@login_required
def multi_delete():
    """
    delete all configs as shown in filtered configs table 
    """
    skipvalidate = False
    if 'deleteform-posttable'in request.form:
        datasets = json.loads(request.form.get('deleteform-posttable'))
        skipvalidate = True # skip validate trigger on arrival
    else:
        extra = request.form.get('extradata')
        if extra is None or len(extra) == 0:
            return redirect(url_for('.default'))
        else:
            datasets = json.loads(extra)
            
    # invalid table request are to be ignored
    formtitle = 'Confirm action: delete {} Configs'.format(len(datasets))
    msg = [
        'This will delete the selected Configs and all coupled Processes and Results. '
        'Configs coupled to a Selector will NOT be deleted.',
        'Tick confirm and click Submit to proceed.'
    ]

    form = ConfirmForm(None if request.method=="GET" else request.form)
    form.extradata.data = json.dumps(datasets)

    # Verify the sign in form
    valid = True
    if not skipvalidate and form.validate_on_submit():
        # check if this is a new module
        if form.confirm.data is False:
            flash('Must tick confirm!', 'error')
            valid = False

        if valid:
            # do stuff
            num = 0
            num_error = 0
            num_skip = 0
            for dat in datasets:
                error = False
                skip = False
                try:
                    cfg = dbio.DBModuleConfigs.get_by_id(dat[0])
                    if not cfg:
                        error = True
                except dbio.DBModuleConfigs.DoesNotExist:
                    error = True
                    
                if error:
                    logger.error("Config {} does not exist".format(dat[0]))
                    num_error += 1       
                    continue
        
                # refuse to delete config coupled to Selector
                if len(cfg.selectors) > 0:
                    num_skip += 1
                    skip = True
                    continue

                if not error and not skip:
                    try:
                        cfg.delete_instance(recursive=True)
                        num += 1
                    except Exception as e:
                        logger.error("Problem deleting Config {}: {}".format(dat[0], str(e)))
                        num_error += 1
                    
            msg = ['Did delete {}/{} Configs.'.format(num, num+num_error+num_skip)]
            if num_skip>0:
                msg.append('Skipped {} Configs that were coupled to Selectors.'.format(num_skip))

            if num_error == 0:
                inpanel={'type': "panel-success", 'title': "Success", 'content':msg}
            elif num >0:
                inpanel={'type': "panel-warning", 'title': "WARNING", 'content':msg}
            else:
                inpanel={'type': "panel-danger", 'title': "ERROR", 'content':msg}
                
            return render_template("wadconfig/generic.html", title='Delete Configs', subtitle='', msg='', html="",
                                   inpanel=inpanel)


    return render_template("wadconfig/confirm.html", form=form, 
                           action=url_for('.multi_delete'),
                           title=formtitle, msg=msg)
