from flask import Blueprint, render_template, Markup, url_for, redirect, request, flash, session, escape
from werkzeug.security import generate_password_hash, check_password_hash

try:
    from app.mod_auth.controllers import login_required
    from app.mod_wadconfig.models import WAUsers, role_names
    from app.libs.shared import dbio_connect
    from app.libs import html_elements
    from app import AUTO_REFRESH
except ImportError:
    from wad_admin.app.mod_auth.controllers import login_required
    from wad_admin.app.mod_wadconfig.models import WAUsers, role_names
    from wad_admin.app.libs.shared import dbio_connect
    from wad_admin.app.libs import html_elements
    from wad_admin.app import AUTO_REFRESH

dbio = dbio_connect()

# Import modify forms
from .forms_usermanager import ModifyForm

# logging
from werkzeug.local import LocalProxy
from flask import current_app
logger = LocalProxy(lambda: current_app.logger)

mod_blueprint = Blueprint('auth_users', __name__, url_prefix='/auth')

@mod_blueprint.route('/users', methods=['GET', 'POST'])
@login_required
def default():
    # display and allow editing of modules table
    stuff = WAUsers.select().order_by(WAUsers.username)
    subtitle = "Only inactive users can be deleted."

    if session.get('logged_in'):
        currentid = session.get('user_id')

    table_rows = []
    for data in stuff:
        table_rows.append([escape(data.username), escape(data.email), 
                           role_names[data.role], data.refresh,
                           data.status == 1,
                           html_elements.Button(label='edit', href=url_for('.modify', gid=data.id)),
                           html_elements.Button(label='delete', href=url_for('.delete', gid=data.id)) if data.status == 0 and not (data.id == currentid) else '',
                           ])

    table = html_elements.Table(headers=['username', 'email', 'role', 'refresh', 'enabled'], rows=table_rows,
                             _class='tablesorter-wadred', _id='sortTable')

    newbutton = html_elements.Button(label='New', href=url_for('.modify'))
    page = table +newbutton
    
    return render_template("wadconfig/generic.html", title='Users', subtitle='', msg='', html=Markup(page),
                           inpanel={'type': "panel-info", 'title': "info", 'content':subtitle})

@mod_blueprint.route('/users/modify', methods=['GET', 'POST'])
@login_required
def modify():
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    if session.get('logged_in'):
        currentid = session.get('user_id')

    # invalid table request are to be ignored
    formtitle = 'Modify User'
    form = ModifyForm(None if request.method=="GET" else request.form)

    if form.gid.data is None or len(form.gid.data)==0 or form.gid.data== "":
        if _gid is None:
            formtitle = 'New User'
            if form.refresh.data is None or form.refresh.data== "":
                form.refresh.data = AUTO_REFRESH
            if form.role.data is None or form.role.data== "":
                form.role.data = 100 # default to admin user
        else:   
            user = WAUsers.get(WAUsers.id==_gid)
            form.username.data = str(user.username)
            form.role.data = int(user.role)
            form.isenabled.data = (user.status == 1)
            form.email.data = str(user.email)
            form.refresh.data = user.refresh
            
            form.gid.data = _gid

    # Verify the sign in form
    valid = True
    if form.validate_on_submit():
        if valid:
            if form.gid.data is None or len(form.gid.data)==0: # new user
                if len(form.password.data) == 0:
                    flash('Cannot create user without password!', 'error') # 
                    valid = False
                else:
                    WAUsers.create(username = str(form.username.data), 
                                password = generate_password_hash(str(form.password.data)), 
                                email = str(form.email.data),
                                role = int(form.role.data),
                                refresh = int(form.refresh.data),
                                status = int(form.isenabled.data)
                            )
                    return render_template("wadconfig/generic.html", title='New User', msg='',
                                inpanel={'type': "panel-success", 'title': "Success", 
                                        'content':"Successfully created new user {} as {}.".format(str(form.username.data), role_names[int(form.role.data)])})

            else:
                user = WAUsers.get(WAUsers.id==form.gid.data)

                # check if we do not change the role of the current admin
                # actually we should check if we not by accident try to remove the last admin
                if int(form.gid.data) == currentid and not int(form.role.data) == 100:
                    flash('Cannot change role of currently logged in admin', 'error') # 
                    valid = False
                if valid:
                    user.username = str(form.username.data)
                    user.email = str(form.email.data)
                    user.refresh = int(form.refresh.data)
                    user.role = int(form.role.data)
                    user.status = int(form.isenabled.data)
        
                    # if this is user is the logged in user, update refresh
                    if session.get('logged_in') == True and session.get('user_id') == user.id:
                        session['refresh'] = user.refresh
                        
                    if len(form.password.data)>0:
                        user.password = generate_password_hash(str(form.password.data))
                    user.save()
                    flash('Changes applied successfully', 'error') # it is not an error, just used as a trigger for display
            
    return render_template("auth/usermanager_modify.html", form=form, action='.',
                           title=formtitle, msg='',
                           inpanel={'type': "panel-info", 'title': "info",
                                    'content':'Fill out the fields and click Submit'})

@mod_blueprint.route('/users/delete', methods=['GET', 'POST'])
@login_required
def delete():
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    if session.get('logged_in'):
        currentid = session.get('user_id')
    if currentid == _gid:
        logger.info("Cannot delete logged in user.")
        return redirect(url_for('wadconfig.home'))
        
    user = WAUsers.get(WAUsers.id==_gid)
    user.delete_instance()

    return redirect(url_for('auth_users.default'))

@mod_blueprint.route('/users/change_default_passwords', methods=['GET', 'POST'])
@login_required
def change_default_passwords():
    """
    Everytime when logging in (as admin), check if one of the services uses the default password.
    If so, present a screen to change that password.
    
    WAD-Admin: check passwords of all users, refer to usermanager_modify to solve.
    WAD-Dashboard (in WAD-Dashboard): check passwords of all users, refer to usermanager_modify to solve.
    
    Orthanc: check database and modify in json and database
    
    PostgreSQL: check database and ...
    """
    title = "Security Warning"
    subtitle = [
        "Some of the WAD Services make use of default passwords. This is a security risk. Use the links to change the passwords!",
        "After changing the passwords, logout of WAD-Admin and restart all WAD Services for the changes to take effect."
    ]
    has_problems = False

    # WAD-Admin
    wa_problem_users = []
    for user in WAUsers.select():
        if check_password_hash(user.password, "waddemo"):
            wa_problem_users.append((user.id, user.username))
            has_problems = True


    # Sources
    source_problem_users = []
    for user in dbio.DBDataSources.select():
        if user.pswd == "waddemo":
            source_problem_users.append((user.id, user.name))
            has_problems = True

    
    # WAD-Dashboard
    wd_problem_users = []
    try:
        from wad_dashboard.app.mod_waddashboard.models import WDUsers
        for user in WDUsers.select():
            if check_password_hash(user.password, "waddemo"):
                wd_problem_users.append((user.id, user.username))
                has_problems = True
    except ImportError as e: # table does not exist (yet)
        pass
    except Exception as e:
        logger.info("Could not check WAD Dashboard users. Skipping. ({})".format(str(e)))

    if not has_problems:
        return redirect(url_for("wadconfig.home"))

    page = Markup()
    # WAD-Admin
    if len(wa_problem_users)>0:
        page += Markup('<BR>')+html_elements.Heading("WAD-Admin", "h2")
        table_rows = []
        for userid, username in wa_problem_users:
            row = [html_elements.Link(label=username, href=url_for('.modify', gid=userid), target="_blank")]
            table_rows.append(row)
        table = html_elements.Table(headers=['Username'], rows=table_rows)
        page += table
    
    # Sources
    if len(source_problem_users)>0:
        page += Markup('<BR>')+html_elements.Heading("Sources", "h2")
        table_rows = []
        for userid, username in source_problem_users:
            row = [html_elements.Link(label=username, href=url_for('wadconfig_sources.modify', gid=userid), target="_blank")]
            table_rows.append(row)
        table = html_elements.Table(headers=['Source'], rows=table_rows)
        page += table
        

    # WAD-Dashboard
    if len(wd_problem_users)>0:
        page += Markup('<BR>')+html_elements.Heading("WAD-Dashboard", "h2")
        table_rows = []
        for userid, username in wd_problem_users:
            row = [username, "Use WAD-Dashboard to update password."]
            table_rows.append(row)
        table = html_elements.Table(headers=['Username'], rows=table_rows)
        page += table

    return render_template("wadconfig/generic.html", title=title, subtitle='', msg='', html=Markup(page),
                           inpanel={'type': "panel-warning", 'title': "WARNING", 'content':subtitle})

