"""
Routines for import and export of modules, configs, selectors.

This should be a collection of functional stuff for interfacing with WAD-QC through the web interface.

Changelog:
  20180315: functionality for make_factory_module
  20171006: Initial version
"""
from __future__ import print_function
import os
import shutil
import zipfile
import json
import tempfile

import codecs
def string_as_bytes(x):
    return codecs.latin_1_encode(x)[0]
def bytes_as_string(x):
    return codecs.latin_1_decode(x)[0]

class DummyLogger:
    def _output(self, prefix, msg):
        print("{}: {}".format(prefix, msg))

    def warning(self, msg):
        self._output("[warning] ", msg)

    def info(self, msg):
        self._output("[info] ", msg)

    def error(self, msg):
        self._output("[error] ", msg)

    def debug(self, msg):
        self._output("[debug] ", msg)

def add_folder_to_zip(zf, folder, outroot, mode):
    """
    Add the contents of a folder to a open zip file.
    
    modes:
      zip_module:  folder is a module source, request to add it to zip
      list_module: folder is a module source, request a list of matches
      zip_dcm:  folder is a dcm collection, request to add it to zip
      list_dcm: folder is a dcm collection, request a list of matches
    """
    
    if not mode in ['zip_module', 'list_module', 'zip_dcm', 'list_dcm']:
        raise ValueError("Unknown mode '{}'".format(mode))

    results = []
    runmode, src = mode.split('_')

    # exclude these directories completely
    exclude_dirs = None 
    
    # define either exclude_extensions or include_extensions, not both
    #  exclude filenames that match these extensions (in any folder)
    exclude_extensions = None
    #  only include filenames that match these extensions (in any folder)
    include_extensions = None

    # explicitely exclude these files
    exclude_files = None
    
    if src == "module":
        # exclude these directories completely
        exclude_dirs = [
            '__pycache__',
            'Config',
            'TestSet',
            '.git',
        ]
            
        # only include filenames that match these extensions (in any folder)
        include_extensions = [
            '.py','.exe', '.sh'
        ]

        # explicitely exclude these files
        exclude_files = [
            'runtests.sh'
        ]

    elif src == "dcm":
        exclude_dirs = [
            '__pycache__',
        ]
        exclude_extensions = [
            '.pyc'
        ]
        exclude_files = [
            'runtests.sh'
        ]
        

    # find all files that match the given criteria
    for subdir, dirs, files in os.walk(folder, topdown=True):
        dirs[:] = [d for d in dirs if (exclude_dirs is None or d not in exclude_dirs) ]
        files[:] = [f for f in files if (include_extensions is None or os.path.splitext(f)[1] in include_extensions) and 
                    (exclude_files is None or f not in exclude_files)]
        for fname in files:
            filename = os.path.join(subdir, fname)
            daf = os.path.join(outroot, os.path.relpath(filename, start=folder))
            results.append(filename)
            if runmode == 'zip':
                zf.write(filename, daf)

    return results


    
# modules
def validate_import_manifest(dbio, folder, check_existing_names, logger=None):
    """
    validate if the manifest in folder is self consistent. optionally ignores checking of uniqueness of modulenames.
    returns valid, message
    """
    if logger is None:
        logger = DummyLogger()
        
    valid = True
    message = ''

    valid_origins = ['factory', 'user']
    valid_datatypes = [m.name for m in dbio.DBDataTypes.select()]
    existing_module_names = [m.name for m in dbio.DBModules.select()]

    # read manifest.ini and check consistency
    try:
        with open(os.path.join(folder, 'manifest.json'),'r') as f:
            manifest = json.load(f)
        #check magic:
        if manifest['info']['magic'] != 'wad-qc-export':
            raise ValueError('manifest.json is not valid for importing configs and modules')
        import_dbversion = manifest['info']['dbversion']
        modules = manifest['modules']
        configs = manifest['configs']

        # check consistency of manifest: do all files exist, do configs refer to existing modules
        for mod in modules.values():
            # check if file is included
            modfolder = os.path.join(folder, 'modules', mod['folder'])
            if not os.path.exists(modfolder) or not os.path.isdir(modfolder):
                logger.warning('---- missing folder %s'%mod['folder'])
                valid = False
            else:
                if not os.path.exists(os.path.join(modfolder, mod['executable'])):
                    logger.warning('---- missing file %s'%mod['executable'])
                    valid = False
            if not mod['origin'] in valid_origins:
                logger.warning('---- invalid origin %s'%mod['origin'])
                valid = False
                
        for cfg in configs.values():
            # check if module is included
            if not cfg['module'] in modules:
                logger.warning('---- missing module %s'%cfg['module'])
                valid = False
            # check if file is included
            if not os.path.exists(os.path.join(folder, 'configs', cfg['filename'])):
                logger.warning('---- missing file %s'%cfg['filename'])
                valid = False
            # check if file is included
            if 'metafilename' in cfg:
                if not os.path.exists(os.path.join(folder, 'metas', cfg['metafilename'])):
                    logger.warning('---- missing file %s'%cfg['metafilename'])
                    valid = False
            if not cfg['origin'] in valid_origins:
                logger.warning('---- invalid origin %s'%cfg['origin'])
                valid = False
            if not cfg['datatype'] in valid_datatypes:
                logger.warning('---- invalid datatype %s'%cfg['datatype'])
                valid = False
        
        if not valid:
            message = 'Cannot import this package: Inconsistent "manifest.json".'

        if check_existing_names:
            if valid:
                # check uniqueness of modulenames
                for mod in modules.values():
                    if mod['name'] in existing_module_names:
                        message = 'Cannot import this package: module "%s" already exists. Rename the existing module and try again.'%mod['name']
                        logger.warning('---- exisiting module name %s'%mod['name'])
                        valid = False
        
    except Exception as e:
        message = 'Cannot import this package: Does not contain a valid "manifest.json".'+str(e)
        valid = False

    return valid, message


def import_configs(dbio, inname, logger=None):
    """
    return success or not.
    check if zip
    unpack zip
      check if manifest.ini
      check consistency of manifest
      check consistency of manifest with folders modules and configs; check if exe file present in folder
      check uniqueness of new module names (if not, suggest user renames current modules, refuse import)
        create each module (zip for upload?!)
        create each config coupled to correct module
    """
    if logger is None:
        logger = DummyLogger()
        
    # make tmpdir
    temp_root = dbio.DBVariables.get_by_name('temp_dir').val
    if not os.path.exists(temp_root):
        os.makedirs(temp_root)
    tempdir = tempfile.mkdtemp(dir=temp_root)
    
    # unpack zipfile
    try:
        with zipfile.ZipFile(inname, 'r') as z:
            z.extractall(tempdir)
    except Exception as e:
        valid = False
        message = str(e)
        return valid, message

    valid = True
    message = 'Success'

    current_dbversion = dbio.DBVariables.get_by_name('iqc_db_version').val

    # read manifest.ini and check consistency
    if not os.path.exists(os.path.join(tempdir, 'manifest.json')):
        message = 'Cannot import this file: Does not contain a "manifest.json".'
        valid = False

    if valid:
        try:
            with open(os.path.join(tempdir, 'manifest.json'),'r') as f:
                manifest = json.load(f)
        except Exception as e:
            message = 'Cannot import this file: Does not contain a valid "manifest.json" ({}).'.format(str(e))
            valid = False
            
    if valid:
        valid, message = validate_import_manifest(dbio, tempdir, check_existing_names=True, logger=logger)
        if valid:
            import_dbversion = manifest['info']['dbversion']
            modules = manifest['modules']
            configs = manifest['configs']

    # all checks were valid. zip each provided module and upload it.
    if valid:
        try:
            for mod in modules.values():
                modfolder = os.path.join(tempdir, 'modules', mod['folder'])
                zipname = modfolder+'.zip'
                if os.path.exists(zipname):
                    os.remove(zipname)
                with zipfile.ZipFile(zipname, 'w', zipfile.ZIP_DEFLATED) as zf:
                    add_folder_to_zip(zf, modfolder, '', mode='zip_module')
        
                field_dict = {
                    'name': mod['name'],
                    'description': mod['description'],
                    'origin': mod['origin'],
                    'filename': mod['executable'],
                    'uploadfilepath': zipname,
                    'repo_url': mod.get('repo_url', ""),
                    'repo_version': mod.get('repo_version', ""),
                }
                dbio.DBModules.create(**field_dict)
                logger.info('Imported module %s'%mod['name'])

        except Exception as e:
            message = 'Error during import of module "%s". '\
                'DBVersion of imported package is %s (current: %s)'%(mod['name'], import_dbversion, current_dbversion)+str(e)
            valid = False
        
    if valid:
        try:
            for cfg in configs.values():
                fname = os.path.join(tempdir, 'configs', cfg['filename'])
                with open(fname,'r') as fcfg: blob = fcfg.read()
                field_dict = {
                    'name': cfg['name'],
                    'description': cfg['description'],
                    'origin': cfg['origin'],
                    'modulename': modules[cfg['module']]['name'],
                    'datatypename': cfg['datatype'],
                    'val': blob
                }
                meta = None
                if 'metafilename' in cfg:
                    fname = os.path.join(tempdir, 'metas', cfg['metafilename'])
                    with open(fname,'r') as fcfg: meta = fcfg.read()
                if not meta is None:
                    nw_meta = dbio.DBMetaConfigs.create(val=meta)
                else:
                    nw_meta = dbio.DBMetaConfigs.create()
                field_dict['meta'] = nw_meta.id

                dbio.DBModuleConfigs.create(**field_dict)
                logger.info('Imported config %s'%cfg['name'])
    
        except Exception as e:
            message = 'Error during import of module_config "%s". '\
                'DBVersion of imported package is %s (current: %s)'%(cfg['name'], import_dbversion, current_dbversion)+str(e)
            valid = False

    # clean up
    shutil.rmtree(tempdir, ignore_errors=True)
    logger.info("valid: {}; msg: {}".format(valid, message))
    if valid and message == "":
        message = "Success"
    return valid, message


def export_configs(dbio, tmpf, cfg_ids, logger=None):
    """
    Pack all selected configs and connected modules with a manifest.ini file and send
     input: 
       cfg_ids: list of ids of configs
     output: 
       error, message

    Export:
      manifest.json
      modules/module_1/
              module_2/
      configs/config_1.json
             /config_2.json
    """
    if logger is None:
        logger = DummyLogger()
        

    error =  False
    msg = ""

    # find connected modules
    modules_dir = dbio.DBVariables.get_by_name('modules_dir').val
    modules = {}
    for cfg_id in cfg_ids:
        cfg = dbio.DBModuleConfigs.get_by_id(cfg_id)
        mod_id = cfg.module.id
        if not mod_id in modules:
            modules[mod_id] = [cfg_id]
        else:
            modules[mod_id].append(cfg_id)
    if len(modules) == 0:
        error = True
        msg = "no modules connected to given configs"
        return error, msg

    # create zip as a spooled file (will only be save to disk if file becomes too large)
    with zipfile.ZipFile(tmpf, 'w', zipfile.ZIP_DEFLATED) as zf:
        # create manifest, and add files to zip
        manifest = {'info': {'magic': 'wad-qc-export',
                             'dbversion':dbio.DBVariables.get_by_name('iqc_db_version').val},
                    'modules': {},
                    'configs': {}
                    }
        cfg_nr = 0
        for i, (m_id, cfg_ids) in enumerate(modules.items()):
            modname = 'MODULE-%d'%i
            manifest['modules'][modname] = {}
            mod = dbio.DBModules.get_by_id(m_id)
            manifest['modules'][modname]['folder'] = 'module_%d'%i
            manifest['modules'][modname]['executable'] = mod.filename
            manifest['modules'][modname]['name'] = mod.name
            manifest['modules'][modname]['description'] = mod.description
            manifest['modules'][modname]['origin'] = mod.origin
            manifest['modules'][modname]['repo_url'] = mod.repo_url
            manifest['modules'][modname]['repo_version'] = mod.repo_version
           
            add_folder_to_zip(zf, os.path.join(modules_dir, mod.foldername), 
                              os.path.join('modules', 'module_%d'%i), mode='zip_module')
            logger.info('Added %s'%(os.path.join('modules', 'module_%d'%i)))

            for cfg_id in cfg_ids:
                cfgname = 'CONFIG-%d'%cfg_nr
                cfg_filename = 'config_%d_%d.json'%(i, cfg_nr)
                manifest['configs'][cfgname] = {}
                cfg = dbio.DBModuleConfigs.get_by_id(cfg_id)
                manifest['configs'][cfgname]['filename'] = cfg_filename
                if not cfg.meta is None:
                    manifest['configs'][cfgname]['metafilename'] = cfg_filename
                manifest['configs'][cfgname]['module'] = modname
                manifest['configs'][cfgname]['datatype'] = cfg.data_type.name
                manifest['configs'][cfgname]['name'] = cfg.name
                manifest['configs'][cfgname]['description'] = cfg.description
                # a config coupled to a selector will be decoupled, but needs to be pickable
                manifest['configs'][cfgname]['origin'] = 'factory' if cfg.origin == 'factory' else 'user'

                daf = os.path.join('configs', cfg_filename)
                zf.writestr(daf, bytes_as_string(cfg.val))
                logger.info('Added %s'%daf)
                if not cfg.meta is None:
                    daf = os.path.join('metas', cfg_filename)
                    zf.writestr(daf, bytes_as_string(cfg.meta.val))
                    logger.info('Added %s'%daf)
                cfg_nr += 1
    
        #add manifest
        zf.writestr('manifest.json', json.dumps(manifest, sort_keys=True, indent=4)) # little bit more pretty output
            
    return error, msg
    

def make_factory_zip(infilename, mode, repo_info={}, outdir=None, logger=None):
    """
    wrapper make a factory zip of an module folder
    """
    if logger is None:
        logger = DummyLogger()
        
    error = True
    msg = ''
    outfile = None

    # try to read info from the module's manifest.json
    module_info = {}

    try:
        with open(infilename, 'r') as f:
            manifest_in = json.load(f)
        dbversion = manifest_in['info']['dbversion']
        if manifest_in['info'].get('magic', None) != 'wad-qc-factory':
            msg = "input manifest is not valid for creating a factory module."
            logger.error(msg)
            return error, msg, outfile

        module_info = manifest_in['module']
        for req in ['name', 'description', 'executable', 'code', 'dependencies']:
            if not req in module_info.keys():
                msg = "Missing required field {} in module manifest.".format(req)
                logger.error(msg)
                return error, msg, outfile
        
    except Exception as e:
        msg = "ERROR! Input manifest is not a valid module manifest.json: {}".format(str(e))
        logger.error(msg)
        return error, msg, outfile
        
    # make sure the working dir is the folder containing the indicated executable
    rootdir = os.path.abspath(os.path.dirname(os.path.abspath(infilename)))

    if outdir is None:
        outdir = os.path.dirname(os.path.abspath(os.path.dirname(os.path.abspath(__file__))))
    
    # locations in zip file
    module_root = "modules"
    module_name = "MODULE-{}".format(0)
    module_folder = "module_{}".format(0)

    zf = None
    results = {'modules':[], 'configs': [], 'metas': []}
    # listing only
    if mode == 'list_module':
        # find all files that match the given criteria
        results['modules'] = add_folder_to_zip(zf, rootdir, os.path.join(module_root, module_folder), mode)
        for res in results['modules']:
            logger.info("Adding {}".format(res))
        logger.info("{} files meet the criteria. Just listing, no zip file created.".format(len(results['modules'])))
        
    elif mode == 'zip_module':
        # create zipfile
        outfile = os.path.join(outdir, module_info['name'].replace(' ', '_')+'.zip')
        zf = zipfile.ZipFile(outfile, 'w', zipfile.ZIP_DEFLATED)

        # create manifest
        manifest = {'info': {'magic': 'wad-qc-export',
                             'dbversion': dbversion },
                    'modules': {},
                    'configs': {}
                    }

        # and module to manifest
        manifest['modules'][module_name] = {}
        manifest['modules'][module_name]['folder'] = module_folder
        manifest['modules'][module_name]['origin'] = "factory"
        manifest['modules'][module_name]['name']         = module_info['name']
        manifest['modules'][module_name]['description']  = module_info['description']
        manifest['modules'][module_name]['executable']   = module_info['executable']
        manifest['modules'][module_name]['code']         = module_info['code']
        manifest['modules'][module_name]['dependencies'] = module_info['dependencies']
        manifest['modules'][module_name]['repo_url']     = repo_info.get('repo_url', "")
        manifest['modules'][module_name]['repo_version'] = repo_info.get('repo_version', "")
        
 
        # add module files to zip
        results['modules'] = add_folder_to_zip(zf, rootdir, os.path.join(module_root, module_folder), mode)
        for res in results['modules']:
            logger.info("Adding {}".format(res))

        # add configs and metas to zip and manifest
        cfg_nr = 0
        for datatype in ['dcm_series', 'dcm_study', 'dcm_instance']:
            cfg_root = os.path.join(rootdir, 'Config', datatype)
            if not os.path.exists(cfg_root):
                continue
            for cfg_in in os.listdir(cfg_root):
                if cfg_in.endswith('.json'):
                    # check for meta file
                    has_meta = False
                    if os.path.exists(os.path.join(cfg_root, 'meta', cfg_in)):
                        has_meta = True

                    cfg_filename = 'config_{}_{}.json'.format(0, cfg_nr)
                    cfg_name = 'CONFIG-{}'.format(cfg_nr)
    
                    manifest['configs'][cfg_name] = {}
                    with open(os.path.join(cfg_root, cfg_in), 'r') as f:
                        cfg = json.load(f)
    
                    manifest['configs'][cfg_name]['filename'] = cfg_filename
                    if has_meta:
                        manifest['configs'][cfg_name]['metafilename'] = cfg_filename 

                    manifest['configs'][cfg_name]['module']   = module_name
                    manifest['configs'][cfg_name]['datatype'] = datatype
                    manifest['configs'][cfg_name]['name']     = cfg_in.split(".json")[0]
                    manifest['configs'][cfg_name]['description'] = cfg["comments"]["description"]
                    manifest['configs'][cfg_name]['origin'] = 'factory'
        
                    daf = os.path.join('configs', cfg_filename)
                    zf.write(os.path.join(cfg_root, cfg_in), daf)
                    logger.info('Added config {}'.format(os.path.join(datatype, cfg_in)))
                    results['configs'].append(os.path.join(datatype, cfg_in))
                    if has_meta:
                        daf = os.path.join('metas', cfg_filename)
                        zf.write(os.path.join(cfg_root, 'meta', cfg_in), daf)
                        logger.info('Added meta {}'.format(os.path.join(datatype, 'meta', cfg_in)))
                        results['metas'].append(os.path.join(datatype, 'meta', cfg_in))
                    cfg_nr += 1
                

        #add manifest
        zf.writestr('manifest.json', json.dumps(manifest, sort_keys=True, indent=4)) # little bit more pretty output

        # close zipfile
        logger.info("Packed {} module files in {}".format(len(results['modules']), os.path.join(outdir, outfile)))
        logger.info("Packed {} config files in {}".format(len(results['configs']), os.path.join(outdir, outfile)))
        logger.info("Packed {} meta files in {}".format(len(results['metas']), os.path.join(outdir, outfile)))
        logger.info("Done.")
        zf.close()

        error = False
        return error, msg, outfile

    else:
        msg = "ERROR! Unknown mode '{}'".format(mode)
        logger.error(msg)
        return error, msg, outfile


# selectors
def import_selectors(dbio, inname, skip_modules, logger=None):
    """
    return success or not.
    check if zip
    unpack zip
      check if manifest.ini
      check consistency of manifest
      check consistency of manifest with folders modules and configs; check if exe file present in folder
      check uniqueness of new module names (if not, suggest user renames current modules, refuse restore)
      check uniqueness of new selector names (if not, suggest user renames current selector, refuse restore)
        create each module (zip for upload?!)
        create each config coupled to correct module
        create each selector, coupled to correct config, with rules
    """
    if logger is None:
        logger = DummyLogger()

    # make tmpdir
    temp_root = dbio.DBVariables.get_by_name('temp_dir').val
    if not os.path.exists(temp_root):
        os.makedirs(temp_root)
    tempdir = tempfile.mkdtemp(dir=temp_root)
    
    # unpack zipfile
    try:
        with zipfile.ZipFile(inname, 'r') as z:
            z.extractall(tempdir)
    except Exception as e:
        valid = False
        message = str(e)
        return valid, message

    valid = True
    message = 'Success'
    error_msg = ""
    
    valid_cfgorigins = ['result'] # selectors must have a config of type result
    valid_modorigins = ['factory', 'user']
    valid_datatypes = [m.name for m in dbio.DBDataTypes.select()]
    valid_logic = [m.name for m in dbio.DBSelectorLogics.select()]
    existing_module_names = [m.name for m in dbio.DBModules.select()]
    existing_config_names = [m.name for m in dbio.DBModuleConfigs.select()]
    existing_selector_names = [m.name for m in dbio.DBSelectors.select()]
    current_dbversion = dbio.DBVariables.get_by_name('iqc_db_version').val
    # read manifest.ini and check consistency
    try:
        with open(os.path.join(tempdir, 'manifest.json'),'r') as f:
            manifest = json.load(f)
        #check magic:
        if manifest['info']['magic'] != 'wad-qc-backup':
            raise ValueError('manifest.json not valid for importing selectors')
        import_dbversion = manifest['info']['dbversion']
        modules = manifest['modules']
        configs = manifest['configs']
        selectors = manifest['selectors']

        # check consistency of manifest: do all files exist, do configs refer to existing modules, do selectors refer to existing configs
        for mod in modules.values():
            if skip_modules:
                # check if module with given name exists
                if not mod['name'] in existing_module_names:
                    error_msg = 'module "%s" does not exists. Try restore with modules.'%mod['name']
                    logger.warning('---- not exisiting module name %s'%mod['name'])
                    valid = False
            else:
                # check if file is included
                modfolder = os.path.join(tempdir, 'modules', mod['folder'])
                if not os.path.exists(modfolder) or not os.path.isdir(modfolder):
                    logger.warning('---- missing folder %s'%mod['folder'])
                    valid = False
                else:
                    if not os.path.exists(os.path.join(modfolder, mod['executable'])):
                        logger.warning('---- missing file %s'%mod['executable'])
                        valid = False
                if not mod['origin'] in valid_modorigins:
                    logger.warning('---- invalid modorigin %s'%mod['origin'])
                    valid = False
                
        for cfg in configs.values():
            # check if module is included
            if not cfg['module'] in modules:
                logger.warning('---- missing module %s'%cfg['module'])
                valid = False
            # check if file is included
            if not os.path.exists(os.path.join(tempdir, 'configs', cfg['filename'])):
                logger.warning('---- missing file %s'%cfg['filename'])
                valid = False
            # check if file is included
            if 'metafilename' in cfg:
                if not os.path.exists(os.path.join(tempdir, 'metas', cfg['metafilename'])):
                    logger.warning('---- missing file %s'%cfg['metafilename'])
                    valid = False
            if not cfg['origin'] in valid_cfgorigins:
                logger.warning('---- invalid cfgorigin %s'%cfg['origin'])
                valid = False
            if not cfg['datatype'] in valid_datatypes:
                logger.warning('---- invalid datatype %s'%cfg['datatype'])
                valid = False
        
        for sel in selectors.values():
            # check if module is included
            if not sel['config'] in configs:
                logger.warning('---- missing config %s'%sel['config'])
                valid = False
            for rule in sel["rules"]:
                if not rule['logic'] in valid_logic:
                    logger.warning('---- invalid logic %s'%rule['logic'])
                    valid = False

        if not valid:
            message = 'Cannot restore this package: Inconsistent "manifest.json". '+error_msg

        if valid:
            if not skip_modules:
                # check uniqueness of names
                for mod in modules.values():
                    if mod['name'] in existing_module_names:
                        message = 'Cannot restore this package: module "%s" already exists. Rename the existing module and try again.'%mod['name']
                        logger.warning('---- exisiting module name %s'%mod['name'])
                        valid = False
                    
            for sel in selectors.values():
                if sel['name'] in existing_selector_names:
                    message = 'Cannot restore this package: selector "%s" already exists. Rename the existing selector and try again.'%sel['name']
                    logger.warning('---- exisiting selector name %s'%sel['name'])
                    valid = False
        
    except Exception as e:
        message = 'Cannot restore this package: Does not contain a valid "manifest.json".'+str(e)
        valid = False

    # all checks were valid. zip each provided module and upload it.
    nw_mods = {}
    if valid:
        if skip_modules:
            try:
                for key, mod in modules.items():
                    nw_mods[key] = dbio.DBModules.get_by_name(mod['name'])
            except Exception as e:
                message = 'Error. Cannot find existing module "%s". '\
                    'DBVersion of imported package is %s (current: %s) '%(mod['name'], import_dbversion, current_dbversion)+str(e)
                valid = False
        else:
            try:
                for key, mod in modules.items():
                    # create a module
                    modfolder = os.path.join(tempdir, 'modules', mod['folder'])
                    zipname = modfolder+'.zip'
                    if os.path.exists(zipname):
                        os.remove(zipname)
                    with zipfile.ZipFile(zipname, 'w', zipfile.ZIP_DEFLATED) as zf:
                        add_folder_to_zip(zf, modfolder, '', mode='zip_module')
            
                    field_dict = {
                        'name': mod['name'],
                        'description': mod['description'],
                        'origin': mod['origin'],
                        'filename': mod['executable'],
                        'uploadfilepath': zipname,
                        'repo_url': mod.get('repo_url', ""),
                        'repo_version': mod.get('repo_version', ""),
                    }
                    nw_mods[key] = dbio.DBModules.create(**field_dict)
                    logger.info('Restored module %s'%mod['name'])
            except Exception as e:
                message = 'Error during restore of module "%s". '\
                    'DBVersion of imported package is %s (current: %s) '%(mod['name'], import_dbversion, current_dbversion)+str(e)
                valid = False
            
    if valid:
        try:
            for sel in selectors.values():
                # create config and meta
                cfg = configs[sel['config']]
                fname = os.path.join(tempdir, 'configs', cfg['filename'])
                with open(fname,'r') as fcfg: blob = fcfg.read()
                field_dict = {
                    'name': cfg['name'],
                    'description': cfg['description'],
                    'origin': cfg['origin'],
                    'module': nw_mods[cfg['module']],
                    'datatypename': cfg['datatype'],
                    'val': blob
                }
                meta = None
                if 'metafilename' in cfg:
                    fname = os.path.join(tempdir, 'metas', cfg['metafilename'])
                    with open(fname,'r') as fcfg: meta = fcfg.read()
                if not meta is None:
                    nw_meta = dbio.DBMetaConfigs.create(val=meta)
                else:
                    nw_meta = dbio.DBMetaConfigs.create()
                field_dict['meta'] = nw_meta.id

                nw_cfg = dbio.DBModuleConfigs.create(**field_dict)
                logger.info('Restored config %s'%cfg['name'])

                # create a selector
                field_dict = {
                    'name': sel['name'],
                    'description': sel['description'],
                    'module_config': nw_cfg,
                    'isactive': False
                }
                nw_sel = dbio.DBSelectors.create(**field_dict)
                logger.info('Restored selector %s'%sel['name'])
                
                # now delete just created meta and config, as it was just for the selector, and the selecor creates a copy!
                nw_cfg.delete_instance(recursive=False)
                nw_meta.delete_instance(recursive=False)
                
                # create selectorrules and link to new selector
                for rule in sel['rules']:
                    field_dict = {
                        'selector': nw_sel,
                        'dicomtag': rule['dicomtag'],
                        'logic': dbio.DBSelectorLogics.get_by_name(rule['logic'])
                    }
                    
                    # create rulevalues and link to new selectorrule
                    nw_rule = dbio.DBSelectorRules.create(**field_dict)
                    for rval in rule['vals']:
                        field_dict = {
                            'rule': nw_rule,
                            'val': rval
                        }
                        dbio.DBRuleValues.create(**field_dict)
                        
        except Exception as e:
            message = 'Error during restore of selector "%s". '\
                'DBVersion of restored package is %s (current: %s) '%(sel['name'], import_dbversion, current_dbversion)+str(e)
            valid = False
                
    # clean up
    shutil.rmtree(tempdir, ignore_errors=True)
    return valid, message

def export_selectors(dbio, tmpf, sel_ids, logger=None):
    """
    Pack all selected selectors and connected configs and modules with a manifest.ini file and send
     input: 
       sel_ids: list of ids of selectors
     output: 
       error, message

    Export:
      manifest.json
      selectors/selector_1.json
               /selector_2.json
      modules/module_1/
              module_2/
      configs/config_1.json
             /config_2.json
    """
    if logger is None:
        logger = DummyLogger()
        
    error =  False
    msg = ""

    # find connected modules
    modules_dir = dbio.DBVariables.get_by_name('modules_dir').val
    modules = {}
    for sel_id in sel_ids:
        sel = dbio.DBSelectors.get_by_id(sel_id)
        mod_id = sel.module_config.module.id
        if not mod_id in modules:
            modules[mod_id] = [sel_id]
        else:
            modules[mod_id].append(sel_id)
    if len(modules) == 0:
        error = True
        msg = "no modules connected to given selectors"
        return error, msg
    
    # create zip as a spooled file (will only be save to disk if file becomes too large)
    with zipfile.ZipFile(tmpf, 'w', zipfile.ZIP_DEFLATED) as zf:
        # create manifest, and add files to zip
        manifest = {'info': {'magic': 'wad-qc-backup',
                             'dbversion':dbio.DBVariables.get_by_name('iqc_db_version').val},
                    'modules': {},
                    'configs': {},
                    'selectors': {}
                    }
        sel_nr = 0
        for i, (m_id, sel_ids) in enumerate(modules.items()):
            modname = 'MODULE-%d'%i
            manifest['modules'][modname] = {}
            mod = dbio.DBModules.get_by_id(m_id)
            manifest['modules'][modname]['folder'] = 'module_%d'%i
            manifest['modules'][modname]['executable'] = mod.filename
            manifest['modules'][modname]['name'] = mod.name
            manifest['modules'][modname]['description'] = mod.description
            manifest['modules'][modname]['origin'] = mod.origin
            manifest['modules'][modname]['repo_url'] = mod.repo_url
            manifest['modules'][modname]['repo_version'] = mod.repo_version

            add_folder_to_zip(zf, os.path.join(modules_dir, mod.foldername), 
                              os.path.join('modules', 'module_%d'%i), 'zip_module')
            logger.info('Added %s'%(os.path.join('modules', 'module_%d'%i)))

            for sel_id in sel_ids:
                # first the configs
                cfgname = 'CONFIG-%d'%sel_nr
                cfg_filename = 'config_%d_%d.json'%(i, sel_nr)
                manifest['configs'][cfgname] = {}
                sel = dbio.DBSelectors.get_by_id(sel_id)
                cfg = sel.module_config
                manifest['configs'][cfgname]['filename'] = cfg_filename
                if not cfg.meta is None:
                    manifest['configs'][cfgname]['metafilename'] = cfg_filename
                manifest['configs'][cfgname]['module'] = modname
                manifest['configs'][cfgname]['datatype'] = cfg.data_type.name
                manifest['configs'][cfgname]['name'] = cfg.name
                manifest['configs'][cfgname]['description'] = cfg.description
                manifest['configs'][cfgname]['origin'] = cfg.origin

                daf = os.path.join('configs', cfg_filename)
                zf.writestr(daf, bytes_as_string(cfg.val))
                logger.info('Added %s'%daf)
                if not cfg.meta is None:
                    daf = os.path.join('metas', cfg_filename)
                    zf.writestr(daf, bytes_as_string(cfg.meta.val))
                    logger.info('Added %s'%daf)

                # then the selector
                selname = 'SELECTOR-%d'%sel_nr
                manifest['selectors'][selname] = {}
                manifest['selectors'][selname]['name'] = sel.name
                manifest['selectors'][selname]['description'] = sel.description
                manifest['selectors'][selname]['config'] = cfgname
                manifest['selectors'][selname]['rules'] = []
                for rule in sel.rules:
                    vals = [ rv.val for rv in rule.values ]
                    manifest['selectors'][selname]['rules'].append(
                        {'dicomtag':rule.dicomtag, 'logic':rule.logic.name, 'vals': vals})
                    
                sel_nr += 1
    
        #add manifest
        zf.writestr('manifest.json', json.dumps(manifest, sort_keys=True, indent=4)) # little bit more pretty output
            
    return error, msg
    
