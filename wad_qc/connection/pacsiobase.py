import os

__version__ = '20170301'

"""
Changelog:
  20170301: added getSeriesId
  20160526: abstractclass only, to be subclassed by others

"""

class PACSIOBASE:
    """
    Abstract class to provide all lower level functions to access the local PACS.
    All functions mentioned here (except uploadDicomFolder) must be overwritten by the subclass
    """
    def __init__(self, config_dict):
        # do whatever is needed for a PACS connection
        self.pacstype = config_dict['TYPE'] # just for the error messages

    def getSharedStudyHeaders(self, studyid):
        """ 
        input: 'studyid' = id of study to be retrieved from PACS
        output: a dictionary of the headers shared by all instances in a study.
        
        each dicom tag 1234,abcd is an item, containing a dictionary {'Name','Type','Value'}
           "0008,0005" : {
             "Name" : "SpecificCharacterSet",
             "Type" : "String",
             "Value" : "ISO_IR 100"
             },
        where 'Value' can be a list containing a new dictionary of tags for nested tags
        """
        # Retrieve the DICOM tags of this study
        raise NotImplementedError('PACS type %s not yet implemented'%self.pacstype)
    
    def getSharedSeriesHeaders(self, seriesid):
        """ 
        input: 'seriesid' = id of series to be retrieved from PACS
        output: a dictionary of the headers shared by all instances in a series.
        """
        # Retrieve the DICOM tags of this series
        raise NotImplementedError('PACS type %s not yet implemented'%self.pacstype)

    def getInstanceHeaders(self, instanceid):
        """ 
        input: 'instanceid' = id of instance to be retrieved from PACS
        output: a dictionary of all headers of this instance.
        """
        # Retrieve the DICOM tags of this instance
        # make sure that the SrcAET is part of the InstanceHeader, if not just add it here
        raise NotImplementedError('PACS type %s not yet implemented'%self.pacstype)

    def getSeriesIds(self, studyid):
        """
        Return a list of all seriesids connected to that studyid
        """
        raise NotImplementedError('PACS type %s not yet implemented'%self.pacstype)

    def getStudyIds(self, patientid=None):
        """
        Return a list of all studyids connected to that patientid. If patientid is None, than return all studyids in the pacs
        """
        raise NotImplementedError('PACS type %s not yet implemented'%self.pacstype)

    def getPatientIds(self):
        """
        Return a list of all patientids
        """
        raise NotImplementedError('PACS type %s not yet implemented'%self.pacstype)

    def getInstancesIds(self, seriesid):
        """
        Return a list of all instanceids connected to that seriesid
        """
        raise NotImplementedError('PACS type %s not yet implemented'%self.pacstype)
    
    def getStudyId(self, seriesid=None, instanceid=None):
        # return the studyid of a series or instance
        raise NotImplementedError('PACS type %s not yet implemented'%self.pacstype)

    def getSeriesId(self, instanceid):
        # return the seriesid of an instance
        raise NotImplementedError('PACS type %s not yet implemented'%self.pacstype)

    def getData(self, data_id, data_type, dcmfolder):
        """
        Retrieve data of data_type from Orthanc, and put in the given, previously created folder dcmfolder
        hierarchy: patient/study/series/instances
        """
        raise NotImplementedError('PACS type %s not yet implemented'%self.pacstype)
    
    def uploadDicomFile(self, filename):
        """
        send a single dicom to the PACS
        Returns a log dict, containing at least {'Filename': filename}, 'Status': <invalid, error,...>}
        """
        raise NotImplementedError('PACS type %s not yet implemented'%self.pacstype)

    def uploadDicomFolder(self, folder):
        """
        Recursively upload the contents of given folder to the PACS
        Returns a list of log dicts
        """
        logs = []
        # Recursively upload a directory
        for root, dirs, files in os.walk(folder, followlinks=True):
            for fn in files:
                filename = os.path.join(root, fn)
                log = self.uploadDicomFile(filename)
                logs.append(log)
        return logs 


    def deleteData(self, data_id, data_type):
        """
        delete given data_type from PACS
        """
        raise NotImplementedError('PACS type %s not yet implemented'%self.pacstype)
