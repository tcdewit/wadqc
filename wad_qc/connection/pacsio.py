from .orthanc import orthancio

__version__ = '20160526'

"""
Changelog:
 20160526: just a wrapper for the real PACSIO classes for IO of specific types
"""

def PACSIO(config_dict):
    pacstype = config_dict['typename']
    if pacstype == 'orthanc':
        return orthancio.PACSIO(config_dict)
    raise NotImplementedError('PACS type %s not yet implemented'%pacstype)
