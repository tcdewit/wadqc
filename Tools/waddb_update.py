#!/usr/bin/env python
from __future__ import print_function
"""
"""
__version__ = '20180209'

import logging
logging.basicConfig(format='[%(levelname)s:%(module)s:%(funcName)s]: %(message)s',level=logging.INFO)
#logging.basicConfig(format='%(asctime)s [%(levelname)s:%(module)s:%(funcName)s]: %(message)s',level=logging.DEBUG)

import os
import sys

try: 
    # this will fail unless wad_qc is already installed
    from wad_qc.connection import dbio
    from wad_admin.app.libs import dbupgrade as libdbupgrade
    DEVMODE=False
    logging.info("** using installed package wad_qc **")
except ImportError: 
    # add parent folder to search path for modules
    sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    from wad_qc.connection import dbio
    from wad_admin.app.libs import dbupgrade as libdbupgrade
    DEVMODE=True
    logging.info("** development mode **")

import stat
import tempfile
import shutil
import subprocess
import argparse

LOGFILE = os.path.join(os.path.dirname(os.path.abspath(__file__)),'waddb_update.log')

def logger(f,hdr):
    logging.info(hdr)
    if f is None:
        with open(LOGFILE, 'a') as f:
            f.write(hdr+'\n')
            f.flush()
        return

    f.write(hdr+'\n')
    f.flush()

def cleanstart(inifile, setupfile, droponly=False):
    # delete all existing stuff before start
    msg = ''
    import shutil
    db_config    = dbio.get_dict_from_inifile(inifile)['iqc-db']
    setup_config = dbio.get_dict_from_inifile(setupfile)
    dbtype = db_config['TYPE']

    wadqcroot = os.path.abspath(os.path.expanduser(setup_config['iqc-storage']['WADQCROOT']))

    # stop wadprocessor if pyro exists
    if os.path.exists(os.path.join(wadqcroot, 'pyro_uri')):
        try:
            result = (_external_call(['wadcontrol', 'quit'], cwd=wadqcroot, silent=True) == 0)
        except:
            pass
        msg += '...quiting wadprocessor'#: '+str(result)
        
    # delete database if sqlite
    if dbtype == 'sqlite':
        dbase = db_config['DBASE']
        logging.info("[cleanstart] deleting %s"%dbase)
        if os.path.exists(dbase): os.remove(dbase)
    elif dbtype == 'postgresql':
        try:
            dbio.db_connect(inifile)
        except Exception as e:
            logging.info(' '.join(["Could not connect to database; is postgresql running?",str(e)]))
            raise ValueError('[cleanstart] Cannot connect!')
        try:
            dbio.db.drop_tables(dbio.DBTables, safe=True, cascade=True)
        except Exception as e:
            logging.info(' '.join(["Could not drop database, assuming it does not exist",str(e)]))
           
    else:
        raise ValueError('[cleanstart] unknown database type %s'%dbtype)

    if not droponly:
        # delete WADQCROOT: all uploaded modules and other stuff
        logging.info("[cleanstart] removing WAD QC root folder")
        shutil.rmtree(wadqcroot,ignore_errors=True)
        os.makedirs(wadqcroot)
        os.mkdir(os.path.join(wadqcroot, 'Logs'))
        
        # inifiles
        logging.info("[cleanstart] copying ini files")
        src = inifile
        dst = os.path.join(wadqcroot,'wadconfig.ini')
        shutil.copy(src, dst)
        src = setupfile
        dst = os.path.join(wadqcroot,'wadsetup.ini')
        shutil.copy(src, dst)
    else:
        logging.info("[cleanstart] removing WAD QC/Modules folder only")
        shutil.rmtree(os.path.join(wadqcroot,'Modules'),ignore_errors=True)
        

    # make a new database
    dbio.db_create_only(inifile, setupfile)
    dbio.db_connect(inifile)

    # copy taskmanager to proper location
    if DEVMODE:
        logging.info("[cleanstart] copying core files")
        dstroot = os.path.join(wadqcroot,'wad_core')
        os.makedirs(dstroot)
        srcroot = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),'wad_core')
        corefiles = ['client.py', 'processor.py', 'selector.py']
        for fn in corefiles:
            src = os.path.join(srcroot,fn)
            dst = os.path.join(dstroot,fn)
            shutil.copy(src, dst)
            os.chmod(dst, os.stat(dst).st_mode | stat.S_IEXEC) # make f executable

        # copy wad_qc stuff to proper location
        logging.info("[cleanstart] copying wad_qc")
        src = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),'wad_qc')
        dst = os.path.join(wadqcroot,'wad_qc')
        shutil.rmtree(dst,ignore_errors=True)
        shutil.copytree(src, dst, ignore=shutil.ignore_patterns('*.pyc'))
        
    return msg

def update_db(inifile, setupfile):
    """
    Check version of current db, and version of dbio_db
    """
    try:
        dbio.db_connect(inifile)
    except Exception as e:
        logging.info(' '.join(["Could not connect to database; is postgresql running?",str(e)]))
        raise ValueError('[update_db] Cannot connect!')
    
    running_version = libdbupgrade.get_running_version(dbio, logger)
    latest_version  = libdbupgrade.get_latest_version(dbio, logger)
    running_label = '.'.join([str(v) for v in running_version])
    latest_label  = '.'.join([str(v) for v in latest_version])
    
    logging.info("Running version {} to be upgraded to dbio version {}.".format(running_label, latest_label))
    error, msgs = libdbupgrade.upgrade(dbio, None)
    msg = '\n'.join(msgs)
    logging.info(msg)
    

if __name__ == "__main__":
    # 2. populate a local db with pyWAD modules and config files 
    # 3. add selectors for the given modules
    rootfolder = os.path.dirname(os.path.abspath(__file__)) # here this folder
    inifile = os.path.join(rootfolder,'wadconfig.ini')
    setupfile = os.path.join(rootfolder,'wadsetup.ini')
    
    parser = argparse.ArgumentParser(description='WAD-QC Database updater, version %s'%__version__)

    parser.add_argument('-i', '--inifile',
                        default=inifile,
                        type=str,
                        help='the inifile file of the WAD server if not using the default location [%s]'%inifile,
                        dest='inifile')

    parser.add_argument('-s', '--setupfile',
                        default=setupfile,
                        type=str,
                        help='the setupfile file of the WAD server if not using the default location [%s]'%setupfile,
                        dest='setupfile')

    args = parser.parse_args()

    if os.path.exists(LOGFILE):
        os.remove(LOGFILE)

    logger(None, parser.description)
    
    if not os.path.exists(args.inifile) or not os.path.exists(args.setupfile):
        logger(None, "ERROR! {} or {} does not exist!".format(args.inifile, args.setupfile))
        exit()

    msg  = update_db(args.inifile, args.setupfile)

    logging.info('')
    logging.info('All done...')
    exit()
