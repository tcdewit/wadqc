#!/usr/bin/env python
from __future__ import print_function
"""
Script to generate a list of packages needed for wad_qc.
import, from <package> import are supported, from . import is not, but that should be ok.

TODO:
  o For "from a import b" this script tries to find attribute "b" in package "a". This will fail is "b" is "c.d".
    Just importing "a" will not check if "b" actually is importable from "a"
  o Multi-line import lines will fail, while trying to import '\'

Changelog:
    20161221: initial version, based on CheckDependencies by DD
"""
__version__ = '20161221' 

import os
import sys
import re
from itertools import chain
import importlib

# change these three lines to use the checker for the modules
package_dirs = ['Tools', 'wad_admin', 'wad_core', 'wad_dashboard', 'wad_qc']
ignore_files = ['check_dependencies.py'] #, '__init__.py',
ignore_packages = ['app']# app reverse to the folder app in flask ['__future__']
#

importdict = {}
fimportdict = {}
root_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) # parent folder
localfiles = []

match_direct = '^(\s)*import(\s)+(\w)' # start at beginning of line, any white space 'import' at least 1 white space follow by a alphanum
match_from   = '^(\s)*from(\s)+(\w)+(\s)+import(\s)+(\w)'

def _directimport(line, filename, dirname):
    # line contains 'import' statement. 
    part = line.split('import ')[1].split(' as')[0].strip()
    for tmpmod in [elem.strip() for elem in part.split(',')]:
        if len(tmpmod) == 0:
            continue
        if not tmpmod in importdict.keys():
            importdict[tmpmod] = []
        importdict[tmpmod].append(os.path.join(os.path.relpath(dirname, root_dir), filename))

def _fromimport(line, filename, dirname):
    # line contains 'from...import' statement. 
    part = line.split('import ')[1].split(' as')[0].split('#')[0].strip()
    base = line.split('from ')[1].split('import ')[0].strip()
    if base in ignore_packages:
        return
    for tmpmod in [elem.strip() for elem in part.split(',')]:
        if len(tmpmod) == 0:
            continue
        if not base in fimportdict:
            fimportdict[base] = {}
        if not tmpmod in fimportdict[base].keys():
            fimportdict[base][tmpmod] = []
        fimportdict[base][tmpmod].append(os.path.join(os.path.relpath(dirname, root_dir), filename))

def test():
    lines = [
        'import first,second',
        '           import second',
        '\timport third as b',
        ' # import fourth as b',
        'from first import second',
        'from first import second as third',
        '    from first import second as third',
        '   # from first import second as third',
        '\tfrom first import second as third',
        '\tfrom . import second as third',
        ]
    print("from matching")
    for line in lines:
        match = not (re.match(match_direct, line) is None)
        print ('  ', match, line)
        if match:
            for tmpmod in [elem.split(' as')[0].strip() for elem in line.split('import ')[1].split(',')]:
                print('    ',tmpmod)
    print("from matching")
    for line in lines:
        match = not (re.match(match_from, line) is None)
        print ('  ', match, line)
        if match:
            base = line.split('from ')[1].split('import ')[0].strip()
            for tmpmod in [elem.split(' as')[0].strip() for elem in line.split('import ')[1].split(',')]:
                print('    %s.%s'%(base,tmpmod))
            
#__main__
if __name__ == "__main__":
    #test()
    #raise
    print('root',root_dir)
    # loop through al given folders and find python files; 
    # of those file take lines mentioning import
    for dirname, dirnames, filenames in chain.from_iterable(os.walk(os.path.join(root_dir, path)) for path in package_dirs):
        for filename in filenames:
            if os.path.splitext(filename)[-1] == '.py':
                if filename in ignore_files:
                    pass
                else:
                    tmpfile = os.path.join(dirname, filename)
                    localfiles.append(os.path.splitext(filename)[0])
                    text = open(tmpfile).readlines()
    
                    for line in text:
                        if len(line.split()) > 0: # line contains spaces
                            if re.match(match_direct, line): # line contains 'import'                       
                                _directimport(line, filename, dirname)
                            elif re.match(match_from, line): # line contains 'from....import' 
                                _fromimport(line, filename, dirname)
    
    for key in importdict.keys():
        if key not in localfiles:
            try:
                importlib.import_module(key)
            except:
                print('Warning could not import "%s". Sources:'%key, importdict[key])

    for base in fimportdict.keys():
        if base not in localfiles:
            for key in fimportdict[base].keys():
                try:
                    module = importlib.import_module(base)
                    getattr(module, key)
                    #importlib.import_module(key, package=base)
                except:
                    print('Warning could not import "%s" from "%s". Sources:'%(key,base), fimportdict[base][key])




