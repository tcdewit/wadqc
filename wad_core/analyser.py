#!/usr/bin/env python

from __future__ import print_function

import os
import shutil
import subprocess
import logging
import tempfile
import jsmin
import json
import traceback
import datetime
import time
import threading
    
from wad_qc.connection import bytes_as_string
from wad_qc.connection.pacsio import PACSIO

def prepareTask(tempdir, configblob):
    datadir = os.path.join(tempdir, 'DICOM')
    config_json = os.path.join(tempdir, 'config.json')
    result_json = os.path.join(tempdir, 'results.json')
    
    os.makedirs(datadir)
    
    with open(config_json, 'w') as f:
        f.write(bytes_as_string(configblob))

    return datadir, config_json, result_json


def submitResults(dbio, result, result_json):
    """
    Only add constraints to results if they are supplied by analysis; constraints from meta are to be used at display time only.
    """
    with open(result_json) as json_data:
        data = json.load(json_data)

    for res in data:
        if res['category'] == 'string':
            model = dbio.DBResultStrings
        elif res['category'] == 'bool':
            model = dbio.DBResultBools
        elif res['category'] == 'float':
            model = dbio.DBResultFloats
        elif res['category'] == 'datetime':
            model = dbio.DBResultDateTimes
            # convert string back to datetime
            res['val'] = datetime.datetime.strptime(res['val'], '%Y-%m-%d %H:%M:%S')
            if 'val_equal' in res:
                res['val_equal'] = datetime.datetime.strptime(res['val_equal'], '%Y-%m-%d %H:%M:%S')
                
        elif res['category'] == 'object':
            model = dbio.DBResultObjects
            
            with open(res['val'], 'rb') as f:
                value = bytes(f.read())
                
            res['filetype'] = res['val'].split('.')[-1].lower()
            res['val'] = bytes_as_string(value)

            
        else:
            logging.info("Unknown category: %s"%res['category'])
            continue

        model.create(result=result, **res)


def runProcess(cmdline, tempdir, timeout):
    try:        
        kill_check = threading.Event()
        def _kill_after_timeout(p):
            p.terminate()
            kill_check.set()

        proc = subprocess.Popen(cmdline, cwd=tempdir, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)

        timer = threading.Timer(timeout, _kill_after_timeout, args=(proc, ))
        timer.start()
        out, err = proc.communicate()
        timer.cancel()

        if kill_check.isSet():
            message = "Process killed due to timeout (%s seconds)"%timeout
            status = "module error"
        elif proc.returncode and err: # check for returncode, else python warnings are treated as errors!
            message = "Error during execution of process\n"
            if out:
                message += "Output of module before exception:\n%s\n"%out
            message += "Exception:\n%s"%err
            status = "module error"
        else:
            message = out
            if err:
                message += "\n{}".format(err)
            status = "finished"

    except Exception as e:
        message = "Error invoking process"
        message += " ".join(cmdline) + "\n"
        message += "Exception:\n%s"%traceback.format_exc()
        status = "module error"
    
    return status, message


def analyse(dbio, pacsio, tempdir, module_path, process, timeout):
    ### Set process status to busy
    if not process.process_status.name == 'queued': return
    busy_status = dbio.DBProcessStatus.get(dbio.DBProcessStatus.name == 'busy')
    process.process_status = busy_status
    process.save()
    
    ### Prepare working directory
    logging.info("Preparing task")
    datadir, config_json, result_json = prepareTask(tempdir, process.module_config.val)
    
    ### Retrieve data from PACS
    logging.info("Querying PACS db")
    pacsio.getData(process.data_id, process.module_config.data_type.name, datadir)

    ### Run the module
    logging.info("Processing module")
    cmdline = [module_path, '-d', datadir, '-c', config_json, '-r', result_json]
    status, message = runProcess(cmdline, tempdir, timeout)
    
    ### Handle the results
    process.process_log = message
    process_status = dbio.DBProcessStatus.get(dbio.DBProcessStatus.name == status)
    process.process_status = process_status

    if status is not "finished":
        # Error encountered when running the process
        info = "\nProcess ID: {}\nCommand: {}".format(process.id, " ".join(cmdline))
        process.process_log += info
        process.save()
        logging.error(process.process_log)
    else:
        # Process ran successfully
        logging.info("Submitting results")                  
        result = dbio.DBResults.finishedProcess(process)
        submitResults(dbio, result, result_json)
        process.delete_instance()        


def run(dbio, processid):
    process = None
    tempdir = None
    try:
        ### Initialize variables
        process = dbio.DBProcesses.get(dbio.DBProcesses.id == processid)
        timeout = float(dbio.DBVariables.get(dbio.DBVariables.name == 'processor_timeout').val)
        pacsio = PACSIO(process.data_source.as_dict())
        module = process.module_config.module
        wadqcroot = dbio.DBVariables.get(dbio.DBVariables.name == 'wadqcroot').val
        module_dir = dbio.DBVariables.get(dbio.DBVariables.name == 'modules_dir').val
        module_path = os.path.join(module_dir, module.foldername, module.filename)
        temp_root = dbio.DBVariables.get(dbio.DBVariables.name == 'temp_dir').val
        if not os.path.exists(temp_root):
            os.makedirs(temp_root)
        tempdir = tempfile.mkdtemp(dir=temp_root)

        ### Get data from PACS and run the module
        analyse(dbio, pacsio, tempdir, module_path, process, timeout)

    except Exception as e:
        ### Encountered unexpected error, not while running the module
        message = traceback.format_exc()
        logging.critical('Analyser failed')
        logging.critical(message)

        if process is not None:
            process.process_log = message
            process_status = dbio.DBProcessStatus.get(dbio.DBProcessStatus.name == 'analyser failed')
            process.process_status = process_status
            process.save()

    if tempdir is not None:
        try:
            logging.info("Cleaning up")
            shutil.rmtree(tempdir)
        except Exception as e:
            logging.critical('Cleanup failed')
            logging.critical(traceback.format_exc())
