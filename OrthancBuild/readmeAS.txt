The scripts in this folder provide a means to build a local, static Orthanc server with a sqlite database for testing purposes.
It provides only the building of Orthanc itself and the web-viewer plugin.

The scripts have been tested on Ubuntu 14.04.3 and on MacOSX 10.10.5 with homebrew only.

Make sure you have a valid build system before running the scripts; for Ubuntu 14.04.3 you need to:
  apt-get -y install python wget nano build-essential unzip cmake mercurial uuid-dev
On MacOSX 10.10.5 with homebrew you need to:
  brew install hg cmake (and a valid compiler)


To build Orthanc with sqlite as backend, execute:
  python orthanc_setup.py -m build -d sqlite

By default this will create a folder WADDEV/orthanc in your home directory, where all folders and executables for Orthanc will be placed. If you want this folder to be located somewhere else, adjust the variable definition of DEVROOT at the start of the orthanc_build.sh script.

To run an Orthanc server, go to the folder WADDEV/orthanc/bin in your home directory after successfully building the Orthanc server.
Then execute
  ./Orthanc ../config/orthanc.json

This will start a local Orthanc server, with default login orthanc and password waddemo
If you want to specify a different password, adjust the variable definition of DB_PASS at the start of the orthanc_build.sh script before building Orthanc.

To completely remove the Orthanc server from your system, just delete the WADDEV/orthanc folder in your home directory.

==========
Postgresql
----------
For compilation of the postgresql backend on Ubuntu 14.04.3 you need to:
  apt-get -y install postgresql-server-dev-all
On OSX:
  brew install postgresql
  
To install the postgresql backend, execute:
  python orthanc_setup.py -m build -d postgresql
  
================
Updating scripts
----------------
To only update the scripts and paths, execute:
  python orthanc_setup.py -m update -d sqlite (or postgresql)

