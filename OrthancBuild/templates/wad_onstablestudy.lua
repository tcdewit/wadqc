function OnStableStudy(studyId, tags, metadata)
   if (metadata['ModifiedFrom'] == nil and
       metadata['AnonymizedFrom'] == nil) then

      print('This study is now stable: ' .. studyId)
      
      -- Call WAD_Collector
      os.execute('__DEVROOT__/orthanc/lua/wadselector.py --source TKW --studyid ' .. studyId .. ' --inifile __DEVROOT__/WAD_QC/wadconfig.ini')
   end
end
