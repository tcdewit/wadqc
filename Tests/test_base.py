import os
import logging

def reset_logging(log_name):
    for handler in logging.root.handlers[:]:
        logging.root.removeHandler(handler)
    for filt in logging.root.filters[:]:
        logging.root.removeFilter(filt)

    logging.basicConfig(format='[%(levelname)s:%(module)s:%(funcName)s]: %(message)s',level=getattr(logging, log_name))

def multidict(inputdict, ncopies, ignore = []):
    # return a list of distinguishable copies of the same input dict
    output = []
    for i in range(ncopies):
        newdict = inputdict.copy()
        for key, value in newdict.items():
            if not key in ignore and isinstance(value, str):
                newdict[key] += '_'+str(i).zfill(2)
        output.append(newdict)
    return output
    
def dbfreshstart(dbio, inifile, setupfile):
    # truncates all tables from the database, ready for a fresh start
    try:
        dbio.db_connect(inifile)
        dbio.db_truncate(setupfile)
    except Exception as e:
        logging.info(' '.join(["Could not empty database, assuming it does not exist, so create it now",str(e)]))
        dbio.db_create_only(inifile, setupfile)
        dbio.db_connect(inifile)

#---------------
# dictionaries for creation of db entries
rootfolder = os.path.dirname(os.path.abspath(__file__)) # here this folder

moddict = {
    'name':'testmodule',
    'uploadfilepath':os.path.abspath(os.path.join(rootfolder,'Modules','TestModule','testmodule.py')),
    'filename':'testmodule.py',
    'description':'test module',
    'origin': 'factory',
}

confdict = {'modulename':moddict['name'],
            'name':'config_test','description':'test config',
            'origin': 'factory','datatypename':'dcm_series',
            'val':'<config/>'}
with open(os.path.join('Configs','dx_philips_wkz1_normi13.json'),'r') as f:
    confdict['val'] = f.read()

seldict = {'configname':confdict['name'], 'configversion': 1,
           'name':'selector_test', 'description':'test selector','isactive':True}

ruledict = {'logicname':'equals',
            'dicomtag':'0x0000,0x1234', 'values':['test rule','another value']}

json_results = [{"category": "string",
                 "name": "identifier1",
                 "description": "SeriesDescription", 
                 "displaylevel": 1, 
                 "val": "Kidneys static DMSA", 
                 "units": "", 
                 "displayposition": 1, 
                 "quantity": ""}, 
                {"category": "string", 
                 "name": "finding1",
                 "description": "SomeResult", 
                 "displaylevel": 1, 
                 "val": "Well, something happened", 
                 "units": "", 
                 "displayposition": 2, 
                 "quantity": ""},
                {"category": "object", 
                 "name": "image1",
                 "description": "SomeImage",
                 "filetype" : ".jpg",
                 "displaylevel": 1, 
                 "val": os.path.join(rootfolder,"test.img"), 
                 "units": "", 
                 "displayposition": 3, 
                 "quantity": ""}]

