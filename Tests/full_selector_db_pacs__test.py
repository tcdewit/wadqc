#!/usr/bin/env python
from __future__ import print_function
"""
stand alone test for combination of selector with db and running pacs

Changelog:
  20160622: splitin config and meta
  20160610: updated to match changes in dbio
  20160530: new dbio
  20160513: python3 compatible
  20160426: implement multi-value for rule
  20160425: prefer dbio and pacsio init by dict
"""

__version__ = '20160610'

import logging
logging.basicConfig(format='[%(levelname)s:%(module)s:%(funcName)s]: %(message)s',level=logging.INFO)
#logging.basicConfig(format='%(asctime)s [%(levelname)s:%(module)s:%(funcName)s]: %(message)s',level=logging.DEBUG)

import os
import sys

try: 
    # this will fail unless wad_qc is already installed
    from wad_qc.connection.pacsio import PACSIO
    DEVMODE=False
    logging.info("** using installed package wad_qc **")
except ImportError: 
    # add parent folder to search path for modules
    sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    from wad_qc.connection.pacsio import PACSIO
    DEVMODE=True
    logging.info("** development mode **")

#DEVMODE = True
from wad_qc.connection import dbio

import stat
import tempfile
import shutil
import subprocess
import argparse

LOGFILE = os.path.join(os.path.dirname(os.path.abspath(__file__)),'full_selector_db_pacs__test.log')


def logger(f,hdr):
    logging.info(hdr)
    if f is None:
        with open(LOGFILE, 'a') as f:
            f.write(hdr+'\n')
            f.flush()
        return

    f.write(hdr+'\n')
    f.flush()

def upload_dcm(pacsio, rootfolder):
    demostudies = []
    folders = [
        os.path.join('Modules','CR','Pehamed_Wellhofer','TestSet'),
        os.path.join('Modules','CT','Philips_QuickIQ','TestSet'), 
        os.path.join('Modules','DX','Normi13','TestSet'),
        os.path.join('Modules','MR','PIQT','TestSet'),
        os.path.join('Modules','MG','CDMam','TestSet'),
        os.path.join('Modules','MG','Flatfield','TestSet'),
        os.path.join('Modules','US','AirReverberations','TestSet'),
        os.path.join('Modules','RF','Pehamed','TestSet'),
    ]
    for folder in folders:
        logging.info(' '.join(['[upload]',folder]))
        logs = pacsio.uploadDicomFolder(os.path.join(rootfolder,folder))
        for log in logs:
            if 'invalid' in log['Status'].lower():
                logging.info('...ignoring invalid file %s'%(log['Filename']))
                continue

            studyid = pacsio.getStudyId(instanceid=log['ID'])
            if 'alreadystored' in log['Status'].lower():
                logging.info('...ignoring instance %s for study %s: instance already in pacs'%(log['ID'],studyid))
    
            if not studyid in demostudies: demostudies.append(studyid)

    logging.info('(Re)added %d demo studies to PACS'%len(demostudies))
    return demostudies

def ruleforselectorname(selectorname, tagname, logic, value):
    rule = {
        'selectorname':selectorname,
        'logicname': logic,
        'values':[value]
        }

    if tagname  == 'Modality':
        rule['dicomtag'] = '0008,0060' # Modality
    elif tagname  == 'StationName':
        rule['dicomtag'] = '0008,1010' # StationName
    elif tagname  == 'SeriesDescription':
        rule['dicomtag'] = '0008,103E' # SeriesDescription
    else:
        rule['dicomtag'] = tagname
    return rule

def selectorforconfig(name, configname, description, isactive=True, datatypename='dcm_series'):
    selector = {
        'configname':configname,
        'name':name,
        'description':description,
        'isactive':isactive,
        'datatypename':datatypename,
    }
    return selector
    
def moduledef(modality, test, exe, cfgs):
    #(modulename,moduledescription,modulepath,[(configname,configpath,datatype_name)]
    folder = os.path.join('Modules',modality,test)
    configs = []
    for name, jsonfile, datatype_name in cfgs:
        configs.append( (name, os.path.join(folder,'Config',jsonfile), datatype_name, os.path.join(folder,'Config','meta',jsonfile)) )
    result = ('%s_%s'%(modality,test), '%s for %s %s'%(exe.split('_')[0],modality,test), os.path.join(folder,exe), configs)
    return result

def populate_db():
    # add all Modules in zip format
    os.chdir(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    zipper = os.path.join(os.getcwd(),'Tools','zipmodule.py')

    modules = [] #(modulename,moduledescription,modulepath,[(configname,configpath)])
    modules.append(
        moduledef('CR','Pehamed_Wellhofer','QCXRay_wadwrapper.py',
                  [('pehamed',  'cr_philips_didi_pehamed.json',  'dcm_series'),
                   ('wellhofer','cr_siemens_fcr1_wellhofer.json','dcm_series')])
    )
    modules.append(
        moduledef('CT','Philips_QuickIQ','QCCT_wadwrapper.py',
                  [('mx8000idt_body',   'ct_philips_mx8000idt_body.json',   'dcm_series'),
                   ('brilliance64_head','ct_philips_brilliance64_head.json','dcm_series')])
    )
    modules.append( #Modules///.py
        moduledef('DX','Normi13','QCNormi13_wadwrapper.py',
                  [('wkz1_normi13',   'dx_philips_wkz1_normi13.json',   'dcm_series'),
                   ('wkz1_uniformity','dx_philips_wkz1_uniformity.json','dcm_series')])
    )
    modules.append( #Modules///.py
        moduledef('MG','CDMam','CDMam_wadwrapper.py',
                  [('cmdam32_L50', 'mg_cdmam32_L50.json', 'dcm_series')])
    )
    modules.append( #Modules///.py
        moduledef('MG','Flatfield','QCMammo_wadwrapper.py',
                  [('selenia_RH',   'mg_hologic_selenia_RH.json',   'dcm_series'),
                   ('dimensions_AG','mg_hologic_dimensions_AG.json','dcm_series')])
    )
    modules.append( #Modules///.py
        moduledef('MR','PIQT','QCMR_wadwrapper.py',
                  [('achieva15_sHB_QA1', 'mr_philips_achieva15_sHB_QA1.json', 'dcm_series'),
                   ('achieva30_sHB_QA2', 'mr_philips_achieva30_sHB_QA2.json', 'dcm_series')])
    )
    modules.append( #Modules///.py
        moduledef('US','AirReverberations','QCUS_wadwrapper.py',
                  [('cx50', 'us_philips_cx50_instance.json', 'dcm_instance'),
                   ('iu22', 'us_philips_iu22_instance.json', 'dcm_instance'),
                   ('epiq', 'us_philips_epiq_instance.json', 'dcm_instance')])
    )

    modules.append( #Modules///.py
        moduledef('RF','Pehamed','QCDDL_wadwrapper.py',
                  [('omni','rf_philips_omni.json','dcm_series'),])
    )
    selectors = [ #name, configname, description
        selectorforconfig('pehamed', 'pehamed','pehamed phantom in a bucky'),
        selectorforconfig('wellhofer','wellhofer','wellhofer phantom in a bucky'),
        selectorforconfig('mx8000idt_body','mx8000idt_body','mx8000idt QuickIQ'),
        selectorforconfig('brilliance64_head','brilliance64_head','brilliance64 QuickIQ'),
        selectorforconfig('wkz1_normi13_tafel',    'wkz1_normi13','wkz1 Normi13 tafel'),
        selectorforconfig('wkz1_uniformity_tafel', 'wkz1_uniformity','wkz1 uniformity tafel'),
        selectorforconfig('wkz1_normi13_wand',     'wkz1_normi13','wkz1 Normi13 wand'),
        selectorforconfig('wkz1_uniformity_wand',  'wkz1_uniformity','wkz1 uniformity wand'),
        selectorforconfig('cmdam32_L50', 'cmdam32_L50','cmdam32 on L50'),
        selectorforconfig('selenia_RH', 'selenia_RH','selenia flatfield'),
        selectorforconfig('dimensions_AG', 'dimensions_AG','dimensions flatfield'),
        selectorforconfig('achieva15_sHB_QA1', 'achieva15_sHB_QA1','achieva15 sHB PIQT'),
        selectorforconfig('achieva30_sHB_QA2', 'achieva30_sHB_QA2','achieva30 sHB PIQT'),
        selectorforconfig('epiq', 'epiq','epiq AirReverberations'),
        selectorforconfig('omni', 'omni','pehamed phantom with ddl omni diagnost'),
    ]

    rules = []
    rules.append(ruleforselectorname('brilliance64_head','Modality','equals','CT'))
    rules.append(ruleforselectorname('brilliance64_head','StationName','equals','AZUCT1'))
    rules.append(ruleforselectorname('mx8000idt_body','Modality','equals','CT'))
    rules.append(ruleforselectorname('mx8000idt_body','StationName','equals','AZUCT2iDT'))
    rules.append(ruleforselectorname('wellhofer','Modality','equals','CR'))
    rules.append(ruleforselectorname('wellhofer','StationName','equals','WKZBUCKY1'))
    rules.append(ruleforselectorname('pehamed','Modality','equals','CR'))
    rules.append(ruleforselectorname('pehamed','StationName','equals','AZUdidiF10'))
    rules.append(ruleforselectorname('wkz1_normi13_tafel','Modality','equals','DX'))
    rules.append(ruleforselectorname('wkz1_normi13_tafel','StationName','equals','WKZDIDI1'))
    rules.append(ruleforselectorname('wkz1_normi13_tafel','SeriesDescription','equals','QC tafel'))
    rules.append(ruleforselectorname('wkz1_normi13_wand','Modality','equals','DX'))
    rules.append(ruleforselectorname('wkz1_normi13_wand','StationName','equals','WKZDIDI1'))
    rules.append(ruleforselectorname('wkz1_normi13_wand','SeriesDescription','equals','QC wand'))
    rules.append(ruleforselectorname('wkz1_uniformity_tafel','Modality','equals','DX'))
    rules.append(ruleforselectorname('wkz1_uniformity_tafel','StationName','equals','WKZDIDI1'))
    rules.append(ruleforselectorname('wkz1_uniformity_tafel','SeriesDescription','equals','QC tafel homogeniteit'))
    rules.append(ruleforselectorname('wkz1_uniformity_wand','Modality','equals','DX'))
    rules.append(ruleforselectorname('wkz1_uniformity_wand','StationName','equals','WKZDIDI1'))
    rules.append(ruleforselectorname('wkz1_uniformity_wand','SeriesDescription','equals','QC wand homogeniteit'))
    rules.append(ruleforselectorname('cmdam32_L50','Modality','equals','MG'))
    rules.append(ruleforselectorname('cmdam32_L50','StationName','equals','MicroDose'))
    rules.append(ruleforselectorname('selenia_RH','Modality','equals','MG'))
    rules.append(ruleforselectorname('selenia_RH','StationName','equals','AZUMAMM'))
    rules.append(ruleforselectorname('dimensions_AG','Modality','equals','MG'))
    rules.append(ruleforselectorname('dimensions_AG','StationName','equals','Dimensions 3D'))
    rules.append(ruleforselectorname('achieva15_sHB_QA1','Modality','equals','MR'))
    rules.append(ruleforselectorname('achieva15_sHB_QA1','StationName','equals','AZUMR3'))
    rules.append(ruleforselectorname('achieva15_sHB_QA1','SeriesDescription','equals','PIQT       QA1S:MS,SE'))
    rules.append(ruleforselectorname('achieva30_sHB_QA2','Modality','equals','MR'))
    rules.append(ruleforselectorname('achieva30_sHB_QA2','StationName','equals','MR7'))
    rules.append(ruleforselectorname('achieva30_sHB_QA2','SeriesDescription','equals','PIQT       QA2S:MS,FE'))
    rules.append(ruleforselectorname('epiq','Modality','equals','US'))
    rules.append(ruleforselectorname('epiq','0018,1000','equals',388187124))
    rules.append(ruleforselectorname('omni','Modality','equals','RF'))
    rules.append(ruleforselectorname('omni','StationName','equals','AZUDDL'))
    
    with open(LOGFILE, 'a') as f: # write output of individual modules to log file
        hdr = '** Populating table modules **'
        logger(f,hdr)

        for modname, moddesc, fname, cfgs in modules:
            wrappername, ext = os.path.splitext(os.path.basename(fname))
            zipname = '%s.py.zip'%wrappername
            hdr = '  creating zip file %s'%zipname
            logger(f, hdr)
            try:
                subprocess.check_call([zipper, '-m', 'zip', '-e', fname], stdout=f, stderr=f)

                hdr = '  adding module %s'%zipname
                logger(f,hdr)
                moddict={
                    'name':modname,
                    'filename':os.path.basename(fname),
                    'uploadfilepath':os.path.join(os.path.abspath(os.getcwd()), zipname),
                    'description':moddesc,
                    'origin': 'factory'
                }
                mod_id = dbio.DBModules.create(**moddict)
                os.remove(zipname)
                for cfgname,cfg,datatype,meta in cfgs:
                    hdr = '  adding config %s'%cfg
                    logger(f,hdr)
                    with open(cfg,'r') as fcfg: cfgdata = fcfg.read()
                    with open(meta,'r') as fcfg: metadata = fcfg.read()
                    confdict = {
                        'module':mod_id,
                        'name':cfgname,
                        'description':'%s for %s'%(cfgname, modname),
                        'datatypename':datatype,
                        'val':cfgdata,
                        'meta':metadata,
                        'origin': 'factory'
                    }
                    cfg_id = dbio.DBModuleConfigs.create(**confdict)
                    # fo not set the module_config id in the selctors, keep reference by name!
                    
                result = '    Result: Success'
            except subprocess.CalledProcessError:
                result = '    Result: FAILED'

            #logging.info('')
            logger(f,result)

        # add the selectors
        for seldict in selectors:
            hdr = '  adding selector %s'%seldict['name']
            logger(f,hdr)
            dbio.DBSelectors.create(**seldict)
                    
        # add the rules
        for ruledict in rules:
            sel_id = dbio.DBSelectors.get(dbio.DBSelectors.name == ruledict['selectorname'])
            hdr = '  adding rule to selector %s'%ruledict['selectorname']
            logger(f,hdr)
            del ruledict['selectorname']
            sel_id.addRule(**ruledict)

def cleanstart(inifile, setupfile):
    # delete all existing stuff before start
    import shutil
    db_config    = dbio.get_dict_from_inifile(inifile)['iqc-db']
    setup_config = dbio.get_dict_from_inifile(setupfile)
    dbtype = db_config['TYPE']

    wadqcroot = os.path.abspath(os.path.expanduser(setup_config['iqc-storage']['WADQCROOT']))

    # delete database if sqlite
    if dbtype == 'sqlite':
        dbase = db_config['DBASE']
        logging.info("[cleanstart] deleting %s"%dbase)
        if os.path.exists(dbase): os.remove(dbase)
    elif dbtype == 'postgresql':
        try:
            dbio.db_connect(inifile)
            dbio.db.drop_tables(dbio.DBTables, safe=True)
        except Exception as e:
            logging.info(' '.join(["Could not drop database, assuming it does not exist",str(e)]))
           
    else:
        raise ValueError('[cleanstart] unknown database type %s'%dbtype)

    # delete WADQCROOT: all uploaded modules and other stuff
    logging.info("[cleanstart] removing WAD QC root folder")
    shutil.rmtree(wadqcroot,ignore_errors=True)
    os.makedirs(wadqcroot)
    
    # inifiles
    logging.info("[cleanstart] copying ini files")
    src = inifile
    dst = os.path.join(wadqcroot,'wadconfig.ini')
    shutil.copy(src, dst)
    src = setupfile
    dst = os.path.join(wadqcroot,'wadsetup.ini')
    shutil.copy(src, dst)

    # make a new database
    dbio.db_create_only(inifile, setupfile)
    dbio.db_connect(inifile)

    # clean PACS
    pacsconfig = dbio.DBDataSources.select()[0].as_dict()
    logging.info("[cleanstart] deleting all studies in IQC-PACS")
    
    # 1. make sure a local pacs is running and that it has the required demo data
    try:
        pacsio = PACSIO(pacsconfig)
        logging.info('OK. Orthanc server running at %s...'%pacsio.URL)
        for studyid in pacsio.getStudyIds():
            logging.info("[cleanstart]  deleting study %s from IQC-PACS"%studyid)
            pacsio.deleteData(studyid, 'dcm_study')
    except:
        raise ValueError('[cleanstart] PACS not running or not accessible')
    

    # copy taskmanager to proper location
    if DEVMODE:
        logging.info("[cleanstart] copying core files")
        dstroot = os.path.join(wadqcroot,'wad_core')
        os.makedirs(dstroot)
        srcroot = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),'wad_core')
        corefiles = ['client.py', 'processor.py', 'selector.py']
        for fn in corefiles:
            src = os.path.join(srcroot,fn)
            dst = os.path.join(dstroot,fn)
            shutil.copy(src, dst)
            os.chmod(dst, os.stat(dst).st_mode | stat.S_IEXEC) # make f executable

        # copy wad_qc stuff to proper location
        logging.info("[cleanstart] copying wad_qc")
        src = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),'wad_qc')
        dst = os.path.join(wadqcroot,'wad_qc')
        shutil.rmtree(dst,ignore_errors=True)
        shutil.copytree(src, dst, ignore=shutil.ignore_patterns('*.pyc'))
   

def findandkill(taskname):
    import psutil
    for pid in psutil.pids():
        p = psutil.Process(pid)
        if p.name() == 'python':
            cmdline = p.cmdline()
            if len(cmdline)>1 and cmdline[1] == taskname:
                logging.info(' '.join(['killing process %d'%pid,str(cmdline)]))
                p.kill()
        
def start_wadprocessor(inifile, wadqcroot):
    import time
    # find working directory

    with open(LOGFILE, 'a') as f: # write output of individual modules to log file
        # start a taskmanager daemon:
        hdr = '** Starting wadprocessor **'
        logger(f,hdr)
        pid = None

        try:
            if DEVMODE:
                pid = subprocess.Popen([os.path.join(wadqcroot, 'wad_core', 'processor.py'), '-i', inifile],
                                       cwd=wadqcroot, close_fds=True)
            else:
                pid = subprocess.Popen(['wadprocessor', '-i', inifile], cwd=wadqcroot, close_fds=True)
            t = 10
            logger(f, '   waiting %d seconds for nameserver to assign address'%t)
            time.sleep(t)
            result = '    Result: Success'
        except subprocess.CalledProcessError:
            result = '    Result: FAILED'
        logger(f,result)

        # start processing: in another shell:
        hdr = '** Starting TaskManagerClient **'
        logger(f,hdr)
        try:
            if DEVMODE:
                subprocess.check_call([os.path.join(wadqcroot,'wad_core','client.py'),'start'],stdout=f,stderr=f,cwd=wadqcroot)
            else:
                subprocess.check_call(['wadcontrol','start'],stdout=f,stderr=f,cwd=wadqcroot)
            result = '  Result: Success'
        except subprocess.CalledProcessError:
            result = '  Result: FAILED'
        logger(f,result)

if __name__ == "__main__":
    # Full testing:
    # 1. make sure a local pacs is running
    # 2. populate a local db with pyWAD modules and config files 
    # 3. add selectors for the given modules
    # 4. send pyWAD demo data to local pacs
    # 5. if the taskmanager runs, pyWAD folder is copied, this should all work
    rootfolder = os.path.dirname(os.path.abspath(__file__)) # here this folder
    inifile = os.path.join(rootfolder,'wadconfig_postgresql.ini')
    setupfile = os.path.join(rootfolder,'wadsetup.ini') # for testing, use the test config
    
    parser = argparse.ArgumentParser(description='Full Test, version %s'%__version__)
    delete = False
    parser.add_argument('--delete',
                        default=delete,action='store_true',
                        help='Delete database and uploaded modules and PACS studies before starting',dest='delete')

    args = parser.parse_args()

    if os.path.exists(LOGFILE):
        os.remove(LOGFILE)

    logger(None, parser.description)
    
    # kill other instances of wadprocessor
    findandkill('./wadprocessor.py')

    if args.delete:
        cleanstart(inifile, setupfile)
    else:
        cleanstart(inifile, setupfile)
        #dbio.db_connect(inifile)
    
    rootfolder = os.path.dirname(rootfolder) # here the parent folder

    # 1. make sure a local pacs is running 

    # clean PACS
    pacsconfig = dbio.DBDataSources.select()[0].as_dict()
    pacsio = PACSIO(pacsconfig)
    logging.info('OK. Orthanc server running at %s...'%pacsio.URL)
    logging.info('')

    # 2.1 make sure there is a basic iqc db available
    # 2.2 populate a local db with pyWAD modules and config files
    populate_db()
    logging.info('')
    sys.stdout.flush()
    sys.stderr.flush()
    #sys.exit()
    
    # 3. start the wadprocessor daemon
    wadqcroot = dbio.DBVariables.get(dbio.DBVariables.name == 'wadqcroot').val
    inifile = os.path.join(wadqcroot,'wadconfig.ini')
    start_wadprocessor(inifile, wadqcroot)
    logging.info('')
    sys.stdout.flush()
    sys.stderr.flush()

    # 4. send pyWAD demo data to local pacs
    demostudies = upload_dcm(pacsio, rootfolder)
    sys.stdout.flush()
    sys.stderr.flush()
    
    logging.info('All systems running...')
    

