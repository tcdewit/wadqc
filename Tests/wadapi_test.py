#!/usr/bin/env python
import requests
import json, jsmin
import time

BASEURL = "http://localhost:3000/api"

class MyException(Exception):
    def __init__(self,*args,**kwargs):
        Exception.__init__(self,*args,**kwargs)

def broken_url(method):
    url = "{}/broken_broken_broken".format(BASEURL)
    payload = {'foo': 1, 'bar': 'bar'}
    response = getattr(requests, method)(url, json=payload)
    try:
        return json.loads(response.text)
    except Exception as e:
        return {'success': False, 'msg':str(e)}
    
def test_broken_url():
    print("== Testing non-existing url...")
    t0 = time.time()
    methods = ['post', 'get', 'delete', 'put']
    badmethods = []
    try:
        for method in methods:
            result = broken_url(method)
            print("... {}:{}".format(method, result['msg']))
            if result["success"]:
                raise MyException("Reached non-existing url with {}".format(method))
            badmethods.append(method)
    except MyException as e:
        print("ERROR! {}".format(str(e)))
        
    elapsed = time.time()-t0
    print("== Successfully tested methods [{}] for non-existing url in {} s".format(', '.join(badmethods), elapsed))
    

#@flask_app.route('/api/authenticate', methods=['POST'])
def get_access_token(user, pswd):
    """
    curl -H "Content-Type: application/json" -X POST -d '{"username":"test","password":"test"}' http://localhost:12002/api/authenticate
    """
    url = "{}/authenticate".format(BASEURL)
    payload = {'username': user, 'password': pswd}
    response = requests.post(url, json=payload)
    return json.loads(response.text)

def auth_header(token):
    return {'Authorization': 'JWT {}'.format(token)}

## protected routes
#@flask_app.route('/api/verifytoken', methods=['POST'])
def verifytoken(token):
    """
    """
    url = "{}/verifytoken".format(BASEURL)
    payload = auth_header(token)
    response = requests.post(url, headers=payload)
    return json.loads(response.text)

#@flask_app.route('/api/selectors', methods=['GET'])
def selectors(token):
    """
    "Authorization: Bearer $ACCESS"
    """
    url = "{}/selectors".format(BASEURL)
    payload = auth_header(token)
    response = requests.get(url, headers=payload)
    return json.loads(response.text)

#@flask_app.route('/api/selectors/<int:id_selector>', methods=['GET'])
def selectors_id(token, id_selector):
    """
    "Authorization: Bearer $ACCESS"
    """
    url = "{}/selectors/{}".format(BASEURL, id_selector)
    payload = auth_header(token)
    response = requests.get(url, headers=payload)
    return json.loads(response.text)

#@flask_app.route('/api/selectors/<int:id_selector>/results', methods=['GET'])
def selectors_id_results(token, id_selector):
    """
    "Authorization: Bearer $ACCESS"
    """
    url = "{}/selectors/{}/results".format(BASEURL, id_selector)
    payload = auth_header(token)
    response = requests.get(url, headers=payload)
    return json.loads(response.text)

#@flask_app.route('/api/selectors/<int:id_selector>/results/<int:id_result>', methods=['GET'])
def selectors_id_results_id(token, id_selector, id_result):
    """
    "Authorization: JWT $ACCESS"
    """
    url = "{}/selectors/{}/results/{}".format(BASEURL, id_selector, id_result)
    payload = auth_header(token)
    response = requests.get(url, headers=payload)
    return json.loads(response.text)

#@flask_app.route('/api/selectors/<int:id_selector>/results/<int:id_result>/dicom',methods=['GET'])
def selectors_id_results_id_dicom(token, id_selector, id_result):
    """
    "Authorization: JWT $ACCESS"
    """
    url = "{}/selectors/{}/results/{}/dicom".format(BASEURL, id_selector, id_result)
    payload = auth_header(token)
    response = requests.get(url, headers=payload)
    return json.loads(response.text)

#@flask_app.route('/api/selectors/<int:id_selector>/results/last', methods=['GET'])
def selectors_id_results_last(token, id_selector):
    """
    "Authorization: JWT $ACCESS"
    """
    url = "{}/selectors/{}/results/last".format(BASEURL, id_selector)
    payload = auth_header(token)
    response = requests.get(url, headers=payload)
    return json.loads(response.text)

#@flask_app.route('/api/selectors/<int:id_selector>/results/<int:id_result>/tests/<int:id_test>/<path:type>', methods=['GET'])
def selectors_id_results_id_tests_id_path(token, id_selector, id_result, id_test, rtype):
    """
    "Authorization: JWT $ACCESS"
    """
    url = "{}/selectors/{}/results/{}/tests/{}/{}".format(BASEURL, id_selector, id_result, id_test, rtype)
    payload = auth_header(token)
    response = requests.get(url, headers=payload)
    return json.loads(response.text)

## unprotected routes
#@flask_app.route('/api/wadselector', methods=['GET'])
def wadselector(source, studyid):
    """
    """
    url = "{}/wadselector".format(BASEURL)
    payload = {'studyid':studyid, 'source':source}

    response = requests.get(url, params=payload)
    return json.loads(response.text)

## test access routes
#@flask_app.route('/api/testaccess_minor', methods=['GET'])
def testaccess_minor(token):
    """
    test if rest_minor access granted
    """
    url = "{}/testaccess_minor".format(BASEURL)
    payload = auth_header(token)
    response = requests.get(url, headers=payload)
    return json.loads(response.text)

#@flask_app.route('/api/testaccess_major', methods=['GET'])
def testaccess_major(token):
    """
    test if rest_major access granted
    """
    url = "{}/testaccess_major".format(BASEURL)
    payload = auth_header(token)
    response = requests.get(url, headers=payload)
    return json.loads(response.text)

#@flask_app.route('/api/testaccess_full', methods=['GET'])
def testaccess_full(token):
    """
    test if rest_full access granted
    """
    url = "{}/testaccess_full".format(BASEURL)
    payload = auth_header(token)
    response = requests.get(url, headers=payload)
    return json.loads(response.text)


def create_demo_user(status, role):
    """
    just create a user with the given role and status.
    return user, password
    """
    import uuid

    from werkzeug.security import generate_password_hash
    try:
        from app.mod_wadconfig.models import WAUsers, role_names
    except ImportError:
        from wad_admin.app.mod_wadconfig.models import WAUsers, role_names

    print("... creating new user")
    username = str(uuid.uuid4())[:20]
    exists = True
    while exists:
        try:
            user = WAUsers.get(WAUsers.username==username)
            exists = True
        except WAUsers.DoesNotExist:
            exists = False

    #role_names = {100: 'admin', 500: 'rest_full', 600: 'rest_minor'}
    passwd = str(uuid.uuid4())[:20]
    testuser = WAUsers.create(username=username, 
                              password=generate_password_hash(passwd), 
                              email="account only for testing", role=role, status=status, refresh=100)
    
    return testuser, passwd

def test_access():
    """
    Test access to protected routes.
    """
    
    print("== Testing access...")
    testcount = 0
    badcount = 0

    # create new rest_user
    testuser, passwd = create_demo_user(status=0, role=100) # start with inactive admin user
    username = testuser.username
    
    t0 = time.time()
    # create token
    try:
        print("... creating token for bad user")
        for status, role in [(0,100), (1,100), (0,500), (0,600), (0,700)]:
            testuser.status = status
            testuser.role = role
            testuser.save()
                
            auth = get_access_token(username, passwd)
            if auth["success"]:
                raise MyException("Got token for status {} role {}!".format(status, role))
            else:
                testcount += 1
    
        print("... creating token for good user")
        testuser.status = 1
        testuser.role = 500
        testuser.save()
    
        auth = get_access_token(username, passwd)
        if not auth["success"]:
            raise MyException("Could not create token!")
        else:
            testcount += 1
    
        token = auth["token"]
    
        for tok in [None, token]:
            label = 'without token' if tok is None else 'with stolen full access token'
            print("... checking access for bad user {}".format(label))
            for status, role in [(0,100), (1,100), (0,500), (0,600), (0,700)]:
                testuser.status = status
                testuser.role = role
                testuser.save()
                    
                result = testaccess_minor(tok)
                if result.get("success", None):
                    raise MyException("Got minor access {} for status {} role {}!".format(label, status, role))
                else:
                    testcount += 1
        
                result = testaccess_major(tok)
                if result.get("success", None):
                    raise MyException("Got major access {} for status {} role {}!".format(label, status, role))
                else:
                    testcount += 1
        
                result = testaccess_full(tok)
                if result.get("success", None):
                    raise MyException("Got full access {} for status {} role {}!".format(label, status, role))
                else:
                    testcount += 1
    
        print("... checking minor access for rest users with full access token")
        for status, role in [(1,500), (1,600), (1,700)]:
            testuser.status = status
            testuser.role = role
            testuser.save()
                
            result = testaccess_minor(token)
            if not result.get("success", None):
                raise MyException("Did not get minor access for status {} role {}!".format(status, role))
            else:
                testcount += 1
            
        print("... checking major access for rest users with full access token")
        for status, role in [(1,500), (1,600), (1,700)]:
            testuser.status = status
            testuser.role = role
            testuser.save()
                
            result = testaccess_major(token)
            if role in [500, 600]: # should get full access
                if not result.get("success", None):
                    raise MyException("Did NOT get major access for status {} role {}!".format(status, role))
                else:
                    testcount += 1

            if role == 700: # should not get full access
                if result.get("success", None):
                    raise MyException("Did get major access for status {} role {}!".format(status, role))
                else:
                    testcount += 1

        print("... checking full access for rest users with full access token")
        for status, role in [(1,500), (1,600), (1,700)]:
            testuser.status = status
            testuser.role = role
            testuser.save()
                
            result = testaccess_full(token)
            if role == 500: # should get full access
                if not result.get("success", None):
                    raise MyException("Did NOT get full access for status {} role {}!".format(status, role))
                else:
                    testcount += 1

            if role in [600, 700]: # should not get full access
                if result.get("success", None):
                    raise MyException("Did get full access for status {} role {}!".format(status, role))
                else:
                    testcount += 1

    except MyException as e:
        print("ERROR! {}".format(str(e)))
        badcount = 1

    elapsed = time.time()-t0
    # clean up
    print("... deleting test user")
    testuser.delete_instance()
    print("== Successfully tested access {}/{} times in {} s".format(testcount, testcount+badcount, elapsed))

def test_full_protected():
    """
    test all full protected endpoints
    """
    print("== Testing full protected end-points...")
    testcount = 0

    # create new rest_user
    testuser, passwd = create_demo_user(status=1, role=600) # start with active rest_minor user

    t0 = time.time()
    try:
        auth = get_access_token(testuser.username, passwd)
        if not auth["success"]:
            raise MyException("Could not create token!")
    
        token = auth["token"]
        
        pass
    
    except MyException as e:
        print("ERROR! {}".format(str(e)))

    elapsed = time.time()-t0
    # clean up
    print("... deleting test user")
    testuser.delete_instance()

    print("== Successfully tested {} full protected end-points in {} s".format(testcount, elapsed))

def test_major_protected():
    """
    test all major protected endpoints
    """
    print("== Testing major protected end-points...")
    testcount = 0

    # create new rest_user
    testuser, passwd = create_demo_user(status=1, role=600) # start with active rest_minor user

    t0 = time.time()
    try:
        auth = get_access_token(testuser.username, passwd)
        if not auth["success"]:
            raise MyException("Could not create token!")
    
        token = auth["token"]
    
        # token
        result = verifytoken(token)
        print("... verifytoken", result)
        if not result.get("success", None):
            raise MyException("Could not verify token!")
        testcount += 1
    
        # selectors
        result = selectors(token)
        print("... selectors", result)
        if not result.get("success", None):
            raise MyException("Could not access selectors!")
        testcount += 1
    
        daselectors = result['selectors']
        for sel in daselectors:
            result = selectors_id(token, sel['id'])
            print("..... selectors/{}".format(sel['id']), result)
            if not result.get("success", None):
                raise MyException("Could not access selectors/id!")
            testcount += 1
            
            # results
            result = selectors_id_results(token, sel['id'])
            print("....... selectors/{}/results".format(sel['id']), result)
            if not result.get("success", None):
                raise MyException("Could not access selectors/id/results!")
            testcount += 1
    
            data = result['results']
            for dat in data:
                result =  selectors_id_results_id(token, sel['id'], dat['id'])
                print("......... selectors/{}/results/{}".format(sel['id'], dat['id']), result)
                if not result.get("success", None):
                    raise MyException("Could not access selectors/id/results/id!")
                testcount += 1
                tests = result['tests']
            
                result = selectors_id_results_id_dicom(token, sel['id'], dat['id'])
                print("......... selectors/{}/results/{}/dicom".format(sel['id'], dat['id']), result)
                if not result.get("success", None):
                    raise MyException("Could not access selectors/id/results/id/dicom!")
                testcount += 1
                
                for test in tests:
                    result = selectors_id_results_id_tests_id_path(token, sel['id'], dat['id'], test['id'], test['type'])
                    print("......... selectors/{}/results/{}/tests/{}/{}".format(sel['id'], dat['id'], test['id'], test['type']), result)
                    if not result.get("success", None):
                        raise MyException("Could not access selectors/id/results/id/tests/id/type!")
                    testcount += 1
    
            result = selectors_id_results_last(token, sel['id'])
            print("..... selectors/{}/results/last".format(sel['id']), result)
            if not result.get("success", None):
                raise MyException("Could not access selectors/id/results/last!")
            testcount += 1
    except MyException as e:
        print("ERROR! {}".format(str(e)))

    elapsed = time.time()-t0
    # clean up
    print("... deleting test user")
    testuser.delete_instance()

    print("== Successfully tested {} major protected end-points in {} s".format(testcount, elapsed))


def test_minor_protected():
    """
    test all minor protected endpoints
    """
    print("== Testing minor protected end-points...")
    testcount = 0

    # create new rest_user
    testuser, passwd = create_demo_user(status=1, role=600) # start with active rest_minor user

    t0 = time.time()
    try:
        auth = get_access_token(testuser.username, passwd)
        if not auth["success"]:
            raise MyException("Could not create token!")
    
        token = auth["token"]
        
        pass
    
    except MyException as e:
        print("ERROR! {}".format(str(e)))

    elapsed = time.time()-t0
    # clean up
    print("... deleting test user")
    testuser.delete_instance()

    print("== Successfully tested {} minor protected end-points in {} s".format(testcount, elapsed))

def test_unprotected():
    """
    test all unprotected endpoints
    """
    testcount = 0

    t0 = time.time()
    # token
    try:
        result = wadselector(source="WADQC", studyid="bert")
        print("... wadselector", result)
    
        if not result.get("success", None):
            raise MyException("Could not access wadselector!")
        testcount += 1

    except MyException as e:
        print("ERROR! {}".format(str(e)))
        
    elapsed = time.time()-t0
    print("== Successfully tested {} unprotected end-points in {} s".format(testcount, elapsed))

if __name__ == "__main__":
    # test access
    test_access()
    
    # first test for none exsisting endpoints
    test_broken_url()

    # test protected end points
    test_full_protected()
    
    test_major_protected()

    test_minor_protected()
    
    # test unprotected end points
    test_unprotected()
    
