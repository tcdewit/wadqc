#!/usr/bin/env python
from __future__ import print_function

import os
import subprocess

__version__ = '20160610'

"""
This should be a one stop for all testing.

Testing:
 * pacsio for access to local Orthanc PACS
 * dbio for access to local iqc database
 * selector for matching of incoming dicom studies to rules
 * taskmanager and processor for creation and execution of analyser jobs
 * selector linked to pacsio and dbio to create jobs in local iqc database

Changelog:
  20160610: removed broken selector__test
  20160513: python3 compatible
  20160408: redirecting full output to file; short output to screen
  20160315: start from scratch

  TODO: 
"""
if __name__ == "__main__":
    testmodules = {
        'dbio': ['python','dbio__test.py'],
        'pacsio': ['python','pacsio__test.py'], 
        'selector_db_pacs': ['python','selector_db_pacs__test.py'], 
    }
    tests = ['pacsio','dbio','selector_db_pacs']
    rootfolder = os.path.dirname(os.path.abspath(__file__)) # here this folder

    with open('UnitTesting.log', 'w') as f: # write output of individual modules to log file
        for test in tests:
            if test in testmodules:
                hdr = '** RUNNING %s TEST **'%test.upper()
                print(hdr)
                f.write(hdr+'\n')
                f.flush()
                try:
                    subprocess.check_call(testmodules[test],stdout=f,stderr=f,cwd=rootfolder)
                    result = '  Result: Success'
                except subprocess.CalledProcessError:
                    result = '  Result: FAILED'
                #print ''
                print(result)
                f.write(result+'\n'+'\n')
                f.flush()
