#!/usr/bin/env python
import os
try: 
    # this will fail unless wad_qc is already installed
    from wad_qc.module import pyWADinput
except ImportError: 
    import sys
    # add root folder of WAD_QC to search path for modules
    _modpath = os.path.dirname(os.path.abspath(__file__))
    while(not os.path.basename(_modpath) == 'Modules'):
        _new_modpath = os.path.dirname(_modpath)
        if _new_modpath == _modpath:
            raise
        _modpath = _new_modpath
    sys.path.append(os.path.dirname(_modpath))
    from wad_qc.module import pyWADinput

# now defaults to json files as input
# ./testmodule.py -d ../../Some_dicom_data/Some_study -c ../../Configs/testmodule_some_config.json -r results.json

def testFunction(data,results,action):
    try:
        level = action['default_level']
    except KeyError:
        level = None

    for instance in data.getAllInstances():
        results.addChar('SeriesDescription', instance.SeriesDescription,level=level)
        print action['filters']
        
if __name__ == "__main__":
    data, results, config = pyWADinput()

    # read runtime parameters for module
    for name,action in config['actions'].iteritems():
        # here use action['filters'] to select the proper dataset if any
        if name == 'myFirstTest':
            testFunction(data, results, action)

    import time
    import random
    time.sleep(2.*random.random())

    results.addChar('SomeResult', 'Well, something " happened')

    results.write()
