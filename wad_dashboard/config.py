# Statement for enabling the development environment
DEBUG = True

# Define the application directory
import os
BASE_DIR = os.path.abspath(os.path.dirname(__file__))  

# Define the database - we are working with
# SQLite for this example
DATABASE_CONNECT_OPTIONS = {}

# Application threads. A common general assumption is
# using 2 per available processor cores - to handle
# incoming requests using one and performing background
# operations using the other.
THREADS_PER_PAGE = 2

# Enable protection agains *Cross-site Request Forgery (CSRF)*
CSRF_ENABLED     = True

# Generate a random secret key for signing cookies
from werkzeug import generate_password_hash
import uuid
SECRET_KEY = generate_password_hash(str(uuid.uuid4()))

UPLOAD_FOLDER = '/tmp'

SESSION_COOKIE_NAME = 'waddashboard.session'
