""""
Routines for deleting and downloading of datasets.

This should be a collection of functional stuff for interfacing with WAD-QC Results through the web interface.

Changelog:
    20171021: Initial version
"""
from __future__ import print_function

class DummyLogger:
    def _output(self, prefix, msg):
        print("{}: {}".format(prefix, msg))

    def warning(self, msg):
        self._output("[warning] ", msg)

    def info(self, msg):
        self._output("[info] ", msg)

    def error(self, msg):
        self._output("[error] ", msg)

    def debug(self, msg):
        self._output("[debug] ", msg)
