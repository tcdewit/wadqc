import os
from wad_qc.connection import dbio
from werkzeug import secure_filename
import tempfile

INIFILE = os.path.join(os.environ['WADROOT'], 'WAD_QC', 'wadconfig.ini')

import codecs
def string_as_bytes(x):
    return codecs.latin_1_encode(x)[0]
def bytes_as_string(x):
    return codecs.latin_1_decode(x)[0]

isconnected = False
def dbio_connect():
    global isconnected
    if not isconnected:
        dbio.db_connect(INIFILE)
        isconnected = True
    return dbio

def getIpAddress(dummy):
    """
    dummy param is just to ensure this function is not available directly from the website
    """
    import socket
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        s.connect(('1.1.1.1', 1))
    except socket.error as e:
        # No network, but we are called from a website, so the host address should be localhost
        return 'localhost'
    
    return s.getsockname()[0]
