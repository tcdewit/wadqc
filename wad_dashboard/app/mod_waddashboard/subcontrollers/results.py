# Import flask dependencies
from flask import Blueprint, request, render_template, \
     flash, session, redirect, url_for, Markup, send_file

try:
    from app.mod_auth.controllers import login_required
    from app.libs import html_elements
    from app.libs import libdisplay, libgroups
    from app.libs.shared import dbio_connect, INIFILE, getIpAddress, bytes_as_string, string_as_bytes
    from app.mod_waddashboard.models import WDSubGroups, role_names
    from app.mod_waddashboard.controllers import get_visible_groups, is_maingroup_visible, is_subgroup_visible, is_selector_visible
except ImportError:
    from wad_dashboard.app.mod_auth.controllers import login_required
    from wad_dashboard.app.libs import html_elements
    from wad_dashboard.app.libs import libdisplay, libgroups
    from wad_dashboard.app.libs.shared import dbio_connect, INIFILE, getIpAddress, bytes_as_string, string_as_bytes
    from wad_dashboard.app.mod_waddashboard.models import WDSubGroups, role_names
    from wad_dashboard.app.mod_waddashboard.controllers import get_visible_groups, is_maingroup_visible, is_subgroup_visible, is_selector_visible

from datetime import datetime as ddtt

# logging
from werkzeug.local import LocalProxy
from flask import current_app
logger = LocalProxy(lambda: current_app.logger)

dbio = dbio_connect()
import os
import io
import json
import jsmin
import numpy as np

import plotly.offline as offline
import plotly.graph_objs as go

# Define the blueprint: 'auth', set its url prefix: app.url/auth
mod_blueprint = Blueprint('results', __name__, url_prefix='/waddashboard')

MIME_IMAGE = ['jpg', 'png'] # object result file types to serve as images


@mod_blueprint.route('/results/log/')
@login_required
def showlog():
    """
    show log of process if available
    """
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    # invalid table request are to be ignored
    if _gid is None:
        return(redirect(url_for('waddashboard.home')))

    if not is_selector_visible(dbio.DBResults.get_by_id(_gid).selector):
        return(redirect(url_for('waddashboard.home')))
        
    try:
        msg = bytes_as_string(dbio.DBResults.get_by_id(_gid).process_log)
    except: # maybe the log does not exist, or this process just finished
        msg = ''

    # go back to overview page
    return render_template("waddashboard/generic.html", title='Results log', msg=msg)

# helper
def make_role_picker(classid, resultid, dataid, datatype, currentrole, subid):
    """
    Return a jQuery dropdown to add display of a result to a certain user role
    """
    pars = {'did':dataid, 'model':datatype}
    if not resultid is None: # for subgroup
        pars['rid'] = resultid
    if not subid is None: # for subgroup
        pars['subid'] = subid
    
    baseurl = url_for('.addtodisplay', **pars)#, role=0)

    # make a dropdown selector
    idname = [ [roleid, role_names[roleid]] for roleid in sorted(role_names.keys()) ]
    picker = html_elements.Picker(name=classid, idname=idname, sid=currentrole,
                                  fun=Markup('location.href = "{}&role="+jQuery(this).val();'.format(baseurl)))

    return Markup(picker)

# Set the route and accepted methods
@mod_blueprint.route('/results/groupshow', methods=['GET', 'POST'])
@login_required
def showgroupresults():
    # show a table with all details of a result
    _rid = int(request.args['rid']) if 'rid' in request.args else None
    _subid = int(request.args['subid']) if 'subid' in request.args else None
    if _subid is None:
        return(redirect(url_for('waddashboard.home')))
    
    # get dispay level of current user
    role = 2
    if session.get('logged_in'):
        role = session.get('role')

    subgroup = WDSubGroups.get_by_id(_subid)
    if not is_subgroup_visible(subgroup):
        return(redirect(url_for('waddashboard.home')))

    selectors = [ sub.selector for sub in subgroup.subitems ]
    subpagetitles = [ html_elements.Heading(label="{}:{}".format(sel.name, sel.description), type="h2") for sel in sorted(selectors, key=lambda x:x.name)]

    # make a list of (acqdate, period, result, acqdate.name) per selector 
    data_per_day = libgroups.getCombinedAcqResults(selectors, logger=logger)
    if data_per_day is None or len(data_per_day)<=_rid:
        return(redirect(url_for('waddashboard.home')))

    display_level = ['admin', 'key-user', 'user']

    page = Markup()
    # make the table
    subpages = []
    header_row=[
        'name', 'units', 'value',
        'equals', 'period', 'ref', 'min', 'low', 'high', 'max'
        ]
    if role<2:
        header_row.append('level')
    
    acqdate, ddata = data_per_day[_rid]
    for itable, (key, acqresult) in enumerate(sorted(list(ddata.items()), key=lambda x:x[0])):
        subpage = Markup()
        table_rows = []
        tableObj_rows = []
        # find selector, display, static_constraints
        selector = dbio.DBSelectors.get_by_name(key)
        display =  { k['name']: k for k in libdisplay.get_display(dbio, selector.id) } # needed to check if constraint is active
        static_constraints = libdisplay.get_all_limits_from_json(dbio, selector)        # needed for status check
        static_periods = static_constraints.get('constraint_period', {}) # needed for period check

        header_rowObj = [
            'name', 'thumbnail / link'
            ]
        if role<2:
            header_rowObj.append('level')

        static_equals = static_constraints.get('constraint_equals', {})
        static_periods = static_constraints.get('constraint_period', {})
        static_minlowhighmaxs = static_constraints.get('constraint_minlowhighmax', {})
        static_refminlowhighmaxs = static_constraints.get('constraint_refminlowhighmax', {})

        stuff = []
        if not acqresult is None:
            result = acqresult[1]
            for i, data in enumerate(result.getResults()):
                xtra = {'display_name': data.name, 'display_position':999999, 
                        'units': '', 'constraint_is_active': False, 'add_button': False, 
                        'display_level': 0}
                if data.name in display.keys():
                    if role > display[data.name]['display_level']:
                        continue
                    else:
                        dis = display[data.name]
                        if 'display_name' in dis and not dis['display_name'] is None and not dis['display_name'] == '' : xtra['display_name'] = dis['display_name']
                        if 'display_position' in dis and not dis['display_position'] is None: xtra['display_position'] = dis['display_position']
                        if 'units' in dis: xtra['units'] = dis['units']
                        if 'decimals' in dis: xtra['decimals'] = dis['decimals']
                        if 'constraint_is_active' in dis: xtra['constraint_is_active'] = dis['constraint_is_active']
                        if 'display_level' in dis: xtra['display_level'] = dis['display_level']
                        if role<2:
                            xtra['add_button'] = True
                elif role >1:
                    continue
                else: # not in meta, but we are admin/key-user, so option to add it to the list
                    xtra['add_button'] = True
                stuff.append((data, xtra))
    
            constraint_labels = ['val_equal','val_period', 'val_ref','val_min', 'val_low','val_high', 'val_max']
            # sort on name, sort on display position, sort on display_level
            stuff = sorted(stuff, key=lambda x: x[1]['display_name'])
            stuff = sorted(stuff, key=lambda x: x[1]['display_position'])
            stuff = sorted(stuff, key=lambda x: x[1]['display_level'], reverse=True)
            for data,xtra in stuff:
                if isinstance(data, dbio.DBResultObjects):
                    datatype='object'
                    if data.filetype.lower() in MIME_IMAGE:
                        lnkobj = html_elements.Link(label=html_elements.Image(label=data.name, src=url_for('.getobject',did=data.id), width=120), 
                                                href=url_for('.showobject',did=data.id), target="_blank") 
                    else:
                        lnkobj = html_elements.Link(label="download",
                                                    href=url_for('.getobject',did=data.id))
                    row = [html_elements.Link(label=xtra['display_name'], href=url_for('.showdata', datatype=datatype, rid=data.id)),
                           lnkobj
                           ]
                    if role<2:
                        row.append(display_level[xtra['display_level']])

                    if xtra['add_button']:
                        row.append(make_role_picker(classid="addObj_{}".format(len(tableObj_rows)), resultid=_rid, dataid=data.id, datatype=datatype, currentrole=xtra['display_level'], subid=_subid))
                    tableObj_rows.append(row)
                    continue
                elif isinstance(data, dbio.DBResultStrings):
                    datatype='string'
                elif isinstance(data, dbio.DBResultBools):
                    datatype='bool'
                elif isinstance(data, dbio.DBResultDateTimes):
                    datatype='datetime'
                elif isinstance(data, dbio.DBResultFloats):
                    datatype='float'
        
                static_single = {}
                if not static_equals.get(data.name, None) is None:
                    static_single['val_equal'] = static_equals[data.name]
                if not static_periods.get(data.name, None) is None:
                    static_single['val_period'] = static_periods[data.name]
                if not static_minlowhighmaxs.get(data.name, None) is None:
                    static_single['val_min'],static_single['val_low'],static_single['val_high'],static_single['val_max'] = static_minlowhighmaxs[data.name]
                if not static_refminlowhighmaxs.get(data.name, None) is None:
                    static_single['val_ref'],static_single['val_min'],static_single['val_low'],static_single['val_high'],static_single['val_max'] = static_refminlowhighmaxs[data.name]
        
                # make sure all displayed elements are (empty) strings
                constraints = {f: getattr(data, 'val_equal', None) for f in  constraint_labels }
                for k,v in constraints.items():
                    if v is None:
                        constraints[k] = ''
                    else:
                        constraints[k] = str(v)
        
                if static_refminlowhighmaxs.get(data.name, None) is None:
                    val_ref = None
                    val_min = static_minlowhighmaxs[data.name][0] if data.name in static_minlowhighmaxs else constraints['val_min']
                    val_low = static_minlowhighmaxs[data.name][1] if data.name in static_minlowhighmaxs else constraints['val_low']
                    val_high = static_minlowhighmaxs[data.name][2] if data.name in static_minlowhighmaxs else constraints['val_high']
                    val_max = static_minlowhighmaxs[data.name][3] if data.name in static_minlowhighmaxs else constraints['val_max']
                else:
                    val_ref = static_refminlowhighmaxs[data.name][0] if data.name in static_refminlowhighmaxs else constraints['val_ref']
                    val_min = static_refminlowhighmaxs[data.name][1] if data.name in static_refminlowhighmaxs else constraints['val_min']
                    val_low = static_refminlowhighmaxs[data.name][2] if data.name in static_refminlowhighmaxs else constraints['val_low']
                    val_high = static_refminlowhighmaxs[data.name][3] if data.name in static_refminlowhighmaxs else constraints['val_high']
                    val_max = static_refminlowhighmaxs[data.name][4] if data.name in static_refminlowhighmaxs else constraints['val_max']
        
                    if not val_ref is None:
                        val_min  = None if val_min is None else val_ref*(1.+val_min/100.)
                        val_low  = None if val_low is None else val_ref*(1.+val_low/100.)
                        val_high = None if val_high is None else val_ref*(1.+val_high/100.)
                        val_max  = None if val_max is None else val_ref*(1.+val_max/100.)


                row = [html_elements.Link(label=xtra['display_name'], href=url_for('.showdata', datatype=datatype, rid=data.id)),
                       xtra['units'],
                       Markup(getSingleStatus(data, static_single, xtra)) if xtra['constraint_is_active'] else formatResults(data.val, xtra),
                       str(static_equals.get(data.name, constraints['val_equal'])),
                       str(static_periods.get(data.name, constraints['val_period'])),
                       "" if val_ref is None else formatResults(val_ref, xtra),
                       formatResults(val_min, xtra),
                       formatResults(val_low, xtra),
                       formatResults(val_high, xtra),
                       formatResults(val_max, xtra)
                       ]
                    
                if role<2:
                    row.append(display_level[xtra['display_level']])
                
                if xtra['add_button']:
                    row.append(make_role_picker(classid="addRes_{}".format(len(table_rows)), resultid=_rid, dataid=data.id, datatype=datatype, currentrole=xtra['display_level'], subid=_subid))
                table_rows.append(row)
    
        table = html_elements.Table(headers=header_row, rows=table_rows,
                                    _class='tablesorter-wadred', _id='sortTable{}'.format(itable))
        subpage += table

        # add objects
        if len(tableObj_rows)>0:
            tableObj = html_elements.Table(headers=header_rowObj, rows=tableObj_rows,
                                           _class='tablesorter-wadred')
            subpage += tableObj
        
        if not acqresult is None:
            result = acqresult[1]

            if role == 0: # add button to pacs
                pacs_url = {}
                url_part =  {'dcm_study':'study', 'dcm_series':'series', 'dcm_instance':'instance'}
                
                if not result.data_source.name in pacs_url:
                    pacs_url[result.data_source.name] = None
                    if result.data_source.source_type.name == 'orthanc': # we know how to construct that url
                        # determine the outside address of this server
                        ip = result.data_source.host
                        if ip == 'localhost':
                            ip = getIpAddress(1) # dummy param
                        pacs_url[result.data_source.name] = '%s://%s:%s/app/explorer.html'%(result.data_source.protocol,
                                                                                              ip, 
                                                                                              result.data_source.port,
                                                                                              )
            
                btnPACS = ''
                if not pacs_url[result.data_source.name] is None:
                    url = '%s#%s?uuid=%s'%(pacs_url[result.data_source.name], url_part[result.module_config.data_type.name], result.data_id)
                    btnPACS = html_elements.Button(label='open in PACS', href=url)

                btnEditMeta = html_elements.Button(label="Edit Full Meta", href=url_for('editor.default', mid=result.module_config.meta.id), _class='btn btn-danger')
                subpage = btnPACS+btnEditMeta+subpage
                
            if role<2: # add log file button
                subpage = html_elements.Button(label='show log', href=url_for('.showlog', gid=result.id))+html_elements.Button(label='delete', href=url_for('.delete', gid=result.id))+subpage
            
        subpage = subpagetitles[itable]+subpage
        page = page +subpage
            
    subtitle = '%s: %s'%(subgroup.name, subgroup.description) # name + description of selector
    msg = ""

    # make a dropdown selector
    idname = [ [rid, dd[0]] for rid, dd in enumerate(data_per_day) ]
    picker = html_elements.Picker(name="acqdate", idname=idname, sid=_rid,
                                  fun=Markup('location.href = "?subid={}&rid="+jQuery(this).val();'.format(_subid)))
    
    page = picker + page
    
    return render_template('waddashboard/generic.html', title='WAD-QC Results', subtitle=subtitle, msg=msg, 
                           html=Markup(page), numtables=len(selectors))

@mod_blueprint.route('/results/delete/')
@login_required
def delete():
    """
    delete given id of given table from iqc db
    """
    # get dispay level of current user
    role = 2
    if session.get('logged_in'):
        role = session.get('role')

    if role >0: # only for admin
        return(redirect(url_for('waddashboard.home')))

    _gid = int(request.args['gid']) if 'gid' in request.args else None

    # invalid table request are to be ignored
    if not _gid is None:
        dbio.DBResults.get_by_id(_gid).delete_instance(recursive=True)

    # go back to overview page
    return redirect(url_for('waddashboard.home'))

# Set the route and accepted methods
@mod_blueprint.route('/results/show', methods=['GET', 'POST'])
@login_required
def showresults():
    # show a table with all details of a result
    _rid = int(request.args['rid']) if 'rid' in request.args else None
    if _rid is None:
        return(redirect(url_for('waddashboard.home')))
    
    result   = dbio.DBResults.get_by_id(_rid)
    selector = dbio.DBSelectors.get_by_id(result.selector.id)
    if not is_selector_visible(selector):
        return(redirect(url_for('waddashboard.home')))

    display  = libdisplay.get_display(dbio, result.selector.id)
    display = {k['name']: k for k in display}
    meta    = selector.module_config.meta.val

    # get a list of static constraints
    static_constraints = libdisplay.get_all_limits_from_json(dbio, selector)

    static_equals = static_constraints.get('constraint_equals', {})
    static_periods = static_constraints.get('constraint_period', {})
    static_minlowhighmaxs = static_constraints.get('constraint_minlowhighmax', {})
    static_refminlowhighmaxs = static_constraints.get('constraint_refminlowhighmax', {})

    # get dispay level of current user
    role = 2
    if session.get('logged_in'):
        role = session.get('role')

    msg =''
    subtitle = '%s: %s'%(selector.name, selector.description) # name + description of selector
    include_inactive=False
    stuff = []
    
    display_level = ['admin', 'key-user', 'user']

    for i, data in enumerate(result.getResults()):
        xtra = {'display_name': data.name, 'display_position':999999, 'units': '', 'constraint_is_active': False, 'add_button': False, 'display_level': 0}
        if data.name in display.keys():
            if role > display[data.name]['display_level']:
                continue
            else:
                dis = display[data.name]
                if 'display_name' in dis and not dis['display_name'] is None and not dis['display_name'] == '' : xtra['display_name'] = dis['display_name']
                if 'display_position' in dis and not dis['display_position'] is None: xtra['display_position'] = dis['display_position']
                if 'units' in dis: xtra['units'] = dis['units']
                if 'decimals' in dis: xtra['decimals'] = dis['decimals']
                if 'constraint_is_active' in dis: xtra['constraint_is_active'] = dis['constraint_is_active']
                if 'display_level' in dis: xtra['display_level'] = dis['display_level']
                if role<2:
                    xtra['add_button'] = True
        elif role >1:
            continue
        else: # not in meta, but we are admin/key-user, so option to add it to the list
            xtra['add_button'] = True
        stuff.append((data, xtra))
        
    header_row=[
        'name', 'units', 'value',
        'equals', 'period', 'ref', 'min', 'low', 'high', 'max'
        ]        
    if role<2:
        header_row.append('level')

    header_rowObj = [
        'name', 'thumbnail / link'
        ]
    if role<2:
        header_rowObj.append('level')

    table_rows = []
    tableObj_rows = []

    constraint_labels = ['val_equal','val_period', 'val_ref', 'val_min', 'val_low','val_high', 'val_max']
    # sort on name, sort on display position, sort on display_level
    stuff = sorted(stuff, key=lambda x: x[1]['display_name'])
    stuff = sorted(stuff, key=lambda x: x[1]['display_position'])
    stuff = sorted(stuff, key=lambda x: x[1]['display_level'], reverse=True)
    for data,xtra in stuff:
        if isinstance(data, dbio.DBResultObjects):
            datatype='object'
            if data.filetype.lower() in MIME_IMAGE:
                lnkobj = html_elements.Link(label=html_elements.Image(label=data.name, src=url_for('.getobject',did=data.id), width=120), 
                                            href=url_for('.showobject',did=data.id), target="_blank") 
            else:
                lnkobj = html_elements.Link(label="download",
                                            href=url_for('.getobject',did=data.id))
            row = [html_elements.Link(label=xtra['display_name'], href=url_for('.showdata', datatype=datatype, rid=data.id)),
                   lnkobj
                   ]
            if role<2:
                row.append(display_level[xtra['display_level']])

            if xtra['add_button']:
                row.append(make_role_picker(classid="addObj_{}".format(len(tableObj_rows)), resultid=None, dataid=data.id, datatype=datatype, currentrole=xtra['display_level'], subid=None))

            tableObj_rows.append(row)
            continue
        elif isinstance(data, dbio.DBResultStrings):
            datatype='string'
        elif isinstance(data, dbio.DBResultBools):
            datatype='bool'
        elif isinstance(data, dbio.DBResultDateTimes):
            datatype='datetime'
        elif isinstance(data, dbio.DBResultFloats):
            datatype='float'

        static_single = {}
        if not static_equals.get(data.name, None) is None:
            static_single['val_equal'] = static_equals[data.name]
        if not static_periods.get(data.name, None) is None:
            static_single['val_period'] = static_periods[data.name]
        if not static_minlowhighmaxs.get(data.name, None) is None:
            static_single['val_min'],static_single['val_low'],static_single['val_high'],static_single['val_max'] = static_minlowhighmaxs[data.name]
        if not static_refminlowhighmaxs.get(data.name, None) is None:
            static_single['val_ref'],static_single['val_min'],static_single['val_low'],static_single['val_high'],static_single['val_max'] = static_refminlowhighmaxs[data.name]

        # make sure all displayed elements are (empty) strings
        constraints = {f: getattr(data, 'val_equal', None) for f in  constraint_labels }
        for k,v in constraints.items():
            if v is None:
                constraints[k] = ''
            else:
                constraints[k] = str(v)

        if static_refminlowhighmaxs.get(data.name, None) is None:
            val_ref = None
            val_min = static_minlowhighmaxs[data.name][0] if data.name in static_minlowhighmaxs else constraints['val_min']
            val_low = static_minlowhighmaxs[data.name][1] if data.name in static_minlowhighmaxs else constraints['val_low']
            val_high = static_minlowhighmaxs[data.name][2] if data.name in static_minlowhighmaxs else constraints['val_high']
            val_max = static_minlowhighmaxs[data.name][3] if data.name in static_minlowhighmaxs else constraints['val_max']
        else:
            val_ref = static_refminlowhighmaxs[data.name][0] if data.name in static_refminlowhighmaxs else constraints['val_ref']
            val_min = static_refminlowhighmaxs[data.name][1] if data.name in static_refminlowhighmaxs else constraints['val_min']
            val_low = static_refminlowhighmaxs[data.name][2] if data.name in static_refminlowhighmaxs else constraints['val_low']
            val_high = static_refminlowhighmaxs[data.name][3] if data.name in static_refminlowhighmaxs else constraints['val_high']
            val_max = static_refminlowhighmaxs[data.name][4] if data.name in static_refminlowhighmaxs else constraints['val_max']

            if not val_ref is None:
                val_min  = None if val_min is None else val_ref*(1.+val_min/100.)
                val_low  = None if val_low is None else val_ref*(1.+val_low/100.)
                val_high = None if val_high is None else val_ref*(1.+val_high/100.)
                val_max  = None if val_max is None else val_ref*(1.+val_max/100.)

        row = [html_elements.Link(label=xtra['display_name'], href=url_for('.showdata', datatype=datatype, rid=data.id)),
               xtra['units'],
               Markup(getSingleStatus(data, static_single, xtra)) if xtra['constraint_is_active'] else str(data.val),
               str(static_equals.get(data.name, constraints['val_equal'])),
               str(static_periods.get(data.name, constraints['val_period'])),
               "" if val_ref is None else str(val_ref),
               str(val_min),
               str(val_low),
               str(val_high),
               str(val_max)
               ]

        if role<2:
            row.append(display_level[xtra['display_level']])
        
        if xtra['add_button']:
            row.append(make_role_picker(classid="addRes_{}".format(len(table_rows)), resultid=None, dataid=data.id, datatype=datatype, currentrole=xtra['display_level'], subid=None))
        table_rows.append(row)
        
    # make a dropdown acqdate vs DBResults id
    acq_ids = []
    for r in selector.results:
        if len(r.datetimes)>0:
            acq_ids.append((r.datetimes[0].val, r.id))
        else: # fall back to created_time
            acq_ids.append((r.created_time, r.id))
            
    #acq_ids = [(r.val, r.result.id) for r in result.datetimes]
    # make a dropdown selector
    idname = [ [rid, acq] for acq,rid in sorted(acq_ids) ]
    picker = html_elements.Picker(name="acqdate", idname=idname, sid=_rid,
                                  fun=Markup('location.href = "?rid="+jQuery(this).val();'))
    
    if role<2: # add log file button
        picker += html_elements.Button(label='show log', href=url_for('.showlog', gid=result.id))+html_elements.Button(label='delete', href=url_for('.delete', gid=result.id))

    if role == 0: # add button to pacs
        pacs_url = {}
        url_part =  {'dcm_study':'study', 'dcm_series':'series', 'dcm_instance':'instance'}
        if not result.data_source.name in pacs_url:
            pacs_url[result.data_source.name] = None
            if result.data_source.source_type.name == 'orthanc': # we know how to construct that url
                # determine the outside address of this server
                ip = result.data_source.host
                if ip == 'localhost':
                    ip = getIpAddress(1) # dummy param
                pacs_url[result.data_source.name] = '%s://%s:%s/app/explorer.html'%(result.data_source.protocol,
                                                                                  ip, 
                                                                                  result.data_source.port,
                                                                                  )

        if not pacs_url[result.data_source.name] is None:
            url = '%s#%s?uuid=%s'%(pacs_url[result.data_source.name], url_part[result.module_config.data_type.name], result.data_id)
            picker += html_elements.Button(label='open in PACS', href=url)


        btnEditMeta = html_elements.Button(label="Edit Full Meta", href=url_for('editor.default', mid=result.module_config.meta.id), _class='btn btn-danger')
        picker += btnEditMeta

        
    table = html_elements.Table(headers=header_row, rows=table_rows,
                                _class='tablesorter-wadred', _id='sortTable')   
    page = picker+table

    if len(tableObj_rows)>0:
        tableObj = html_elements.Table(headers=header_rowObj, rows=tableObj_rows,
                                       _class='tablesorter-wadred')
        page += tableObj

    if role == 0: # only for admin user
        page += html_elements.Button(label='add all new items to admin display', href=url_for('.addalltodisplay',rid=_rid,role=0))
    if role < 2: # not for normal user
        page += html_elements.Button(label='add all new items to key-user display', href=url_for('.addalltodisplay',rid=_rid,role=1))
        page += html_elements.Button(label='add all new items to user display', href=url_for('.addalltodisplay',rid=_rid,role=2))
    
    return render_template('waddashboard/generic.html', title='WAD-QC Results', subtitle=subtitle, msg=msg, html=Markup(page))

@mod_blueprint.route('/results/addall', methods=['GET', 'POST'])
@login_required
def addalltodisplay():
    # not for normal users
    role = 2
    if session.get('logged_in'):
        role = session.get('role')
    if role >1: 
        return(redirect(url_for('admin.default')))

    # read results abd add all not-in-display items to specified role
    _rid = int(request.args['rid']) if 'rid' in request.args else None
    if _rid is None:
        return(redirect(url_for('waddashboard.home')))

    _role = int(request.args['role']) if 'role' in request.args else None
    if _role is None or _role not in [0,1,2] or _role<role:
        return(redirect(url_for('waddashboard.home')))
    
    result   = dbio.DBResults.get_by_id(_rid)
    if not is_selector_visible(result.selector):
        return(redirect(url_for('waddashboard.home')))

    display  = libdisplay.get_display(dbio, result.selector.id)
    display = {k['name']: k for k in display}

    for res in result.getResults():
        if not res.name in display.keys():
            # build a new display entry; default is admin only, first position (so easily findable), no active constraints
            # do not worry about overriding constraints, cause there are none, else it could not be added now
            field_dict = {
                'description': '', 
                'display_level': _role, 
                'display_name': '', 
                'display_position': 0, 
                'constraint_is_active': False, 
                'units': ''
            }
            # use the meta coupled to the selector through config, not the one coupled to result
            meta = res.result.selector.module_config.meta
            blob = json.loads(jsmin.jsmin(bytes_as_string(meta.val)))
            blob['results'][res.name] = field_dict
            meta.val = json.dumps(blob)
            meta.save()

    return redirect(url_for('.showresults', rid=res.result.id))
        
@mod_blueprint.route('/results/add', methods=['GET', 'POST'])
@login_required
def addtodisplay():
    """
    add to display or change role if already added
    """
    # not for normal users
    role = 2
    if session.get('logged_in'):
        role = session.get('role')
    if role >1: 
        return(redirect(url_for('admin.default')))

    # read the meta blob, add given result to meta
    _did = int(request.args['did']) if 'did' in request.args else None
    _subid = int(request.args['subid']) if 'subid' in request.args else None
    _rid = int(request.args['rid']) if 'rid' in request.args else None
    _model = str(request.args['model']) if 'model' in request.args else None

    _role = int(request.args['role']) if 'role' in request.args else None
    if _role is None or _role not in [0,1,2] or _role<role:
        return(redirect(url_for('waddashboard.home')))

    models = {
        'object': dbio.DBResultObjects,
        'string': dbio.DBResultStrings,
        'bool': dbio.DBResultBools,
        'datetime': dbio.DBResultDateTimes,
        'float': dbio.DBResultFloats,
        
    }

    if _did is None or _model is None or not _model in models.keys():
        return redirect(url_for('.showresults'))
 
    model = models[_model]
    res = model.get_by_id(_did)
    if not is_selector_visible(res.result.selector):
        return(redirect(url_for('waddashboard.home')))

    # build a new display entry; default is admin only, first position (so easily findable), no active constraints
    # do not worry about overriding constraints, cause there are none, else it could not be added now
    field_dict = {
        'description': '', 
        'display_level': _role,
        'display_name': '', 
        'display_position': 0, 
        'constraint_is_active': False, 
        'units': ''
    }
    # use the meta coupled to the selector through config, not the one coupled to result
    meta = res.result.selector.module_config.meta
    blob = json.loads(jsmin.jsmin(bytes_as_string(meta.val)))
    if res.name in blob['results'].keys():
        blob['results'][res.name]['display_level'] = _role
    else:
        blob['results'][res.name] = field_dict
    meta.val = json.dumps(blob)
    meta.save()
    if not _subid is None:
        return redirect(url_for('.showgroupresults', rid=_rid, subid=_subid))
    else:
        return redirect(url_for('.showresults', rid=res.result.id))

@mod_blueprint.route('/results/object')
@login_required
def showobject():
    _did = int(request.args['did']) if 'did' in request.args else None
    if _did is None:
        return redirect(url_for('.showresults'))

    # get dispay level of current user
    role = 2
    if session.get('logged_in'):
        role = session.get('role')

    msg =''
    # check if we should display this data for this user
    res = dbio.DBResultObjects.get_by_id(_did)
    selector = dbio.DBSelectors.get_by_id(res.result.selector.id)
    if not is_selector_visible(selector):
        return(redirect(url_for('waddashboard.home')))

    display  = libdisplay.get_display(dbio, selector.id)
    display = {k['name']: k for k in display} # turn into dictionary
    if res.name in display.keys():
        dis = display[res.name]
        if 'description' in dis: 
            msg = dis['description']
        if role > display[res.name]['display_level']:
            return(redirect(url_for('waddashboard.home')))
    elif role >1: # only show unknown results to admin
        return(redirect(url_for('waddashboard.home')))

    img = html_elements.Image(label=res.name, src=url_for('.getobject',did=_did))
    
    subtitle = '%s: %s'%(selector.name, selector.description) # name + description of selector
    dt = ''
    if len(res.result.datetimes)>0:
        dt = str(res.result.datetimes[0].val)

    page = img
    
    return render_template('waddashboard/generic.html', title='%s: %s'%(res.name,dt), subtitle=subtitle, msg=msg, html=Markup(page))

@mod_blueprint.route('/results/data')
@login_required
def showdata():
    # show a table or a graph of all historical data of this name of this selector
    _rid = int(request.args['rid']) if 'rid' in request.args else None
    _datatype = str(request.args['datatype']) if 'datatype' in request.args else None

    if _rid is None or _datatype is None:
        return(redirect(url_for('waddashboard.qc', rid=_rid, datatype=_datatype)))
    
    picked, acq_vals = getDataList(_rid, _datatype)
    if not is_selector_visible(picked.result.selector):
        return(redirect(url_for('waddashboard.home')))

    display  = libdisplay.get_display(dbio, picked.result.selector.id)
    display = {k['name']: k for k in display} # turn into dictionary

    # get a list of static constraints
    static_constraints = libdisplay.get_all_limits_from_json(dbio, picked.result.selector.id)
    static_equals = static_constraints.get('constraint_equals', {}).get(picked.name, None)
    static_periods = static_constraints.get('constraint_period', {}).get(picked.name, None)
    static_minlowhighmaxs = static_constraints.get('constraint_minlowhighmax', {}).get(picked.name, [None, None, None, None])
    if picked.name in static_constraints.get('constraint_refminlowhighmax', {}).keys():
        val_ref, val_min, val_low, val_high, val_max = static_constraints.get('constraint_refminlowhighmax', {}).get(picked.name, [None, None, None, None, None])

        if not val_ref is None:
            val_min  = None if val_min is None else val_ref*(1.+val_min/100.)
            val_low  = None if val_low is None else val_ref*(1.+val_low/100.)
            val_high = None if val_high is None else val_ref*(1.+val_high/100.)
            val_max  = None if val_max is None else val_ref*(1.+val_max/100.)

        static_minlowhighmaxs = [val_min, val_low, val_high, val_max] 


    # get dispay level of current user
    role = 2
    if session.get('logged_in'):
        role = session.get('role')

    xtra = {'units': '', 'constraint_is_active': False}
    # check if we should display this data for this user
    dis = {}
    if picked.name in display.keys():
        dis = display[picked.name]
        if 'units' in dis: xtra['units'] = dis['units']
        if 'constraint_is_active' in dis: xtra['constraint_is_active'] = dis['constraint_is_active']
        if 'description' in dis: xtra['description'] = dis['description']
        if 'decimals' in dis: xtra['decimals'] = dis['decimals']
        if role > display[picked.name]['display_level']:
            return(redirect(url_for('waddashboard.home')))
    elif role >1: # only show unknown results to admin
        return(redirect(url_for('waddashboard.home')))

    msg = ''
    if 'description' in xtra:
        msg = xtra['description']

    subtitle = '%s: %s: '%(picked.result.selector.name, picked.result.selector.description) # name + description of selector

    if 'display_name' in dis and not dis['display_name'] is None and not dis['display_name'] == '' : 
        subtitle += dis['display_name']
    else:
        subtitle += picked.name

    if _datatype == 'float':
        page = plot(acq_vals, xtra, static_equals, static_minlowhighmaxs )

    elif _datatype == 'object':
        table_rows = []
        for dt, data in sorted(acq_vals, key=lambda x: x[0], reverse=True):
            if data.filetype.lower() in MIME_IMAGE:
                lnkobj = html_elements.Link(label=html_elements.Image(label=data.name, src=url_for('.getobject',did=data.id), width=120), 
                                            href=url_for('.showobject',did=data.id), target="_blank") 
            else:
                lnkobj = html_elements.Link(label="download",
                                            href=url_for('.getobject',did=data.id))

            table_rows.append([dt, lnkobj])
            
        table = html_elements.Table(headers=['datetime', 'value'],
                                    rows=table_rows,
                                    _class='tablesorter-wadred', _id='sortTable')
        page = table

    else:
        table_rows = []
        static_single = {}
        if not static_equals is None:
            static_single['val_equal'] = static_equals
        if not static_periods is None:
            static_single['val_period'] = static_periods
        if not static_minlowhighmaxs is None:
            static_single['val_min'],static_single['val_low'],static_single['val_high'],static_single['val_max'] = static_minlowhighmaxs

        for dt, data in sorted(acq_vals, key=lambda x: x[0], reverse=True):
            table_rows.append([dt, 
                               xtra['units'], 
                               getSingleStatus(data, static_single, xtra) if xtra['constraint_is_active'] else data.val,
                               static_single.get('val_equal', getattr(data, 'val_equal', None)),
                               static_single.get('val_period', getattr(data, 'val_period', None)),
                               ])
        
        table = html_elements.Table(headers=['datetime', 'units', 'value', 'equals', 'period'],
                                    rows=table_rows,
                                    _class='tablesorter-wadred', _id='sortTable')
        page = table

    if not _datatype == 'object':
        btnCopy = html_elements.Button(label='export to csv', href=url_for('.gettable', rid=_rid, datatype=_datatype))
        if role == 0: # only show to admin
            btnConstraints = html_elements.Button(label='modify constraints', href=url_for('display.modify', sid=picked.result.selector.id, did=picked.name))
            page = btnCopy+btnConstraints+page
        else:
            page = btnCopy+page

    if msg is None or len(msg) < 1:
        return render_template('waddashboard/generic.html', title='WAD-QC Data', subtitle=subtitle, msg=msg, html=Markup(page))
    else:
        return render_template('waddashboard/generic.html', title='WAD-QC Data', subtitle=subtitle, msg='', html=Markup(page),
                               inpanel={'type': "panel-info", 'title': "info", 'content':msg})


def formatResults(resval, xtra):
    """
    implements formatting of results on the reporting page
    fallback is always simple conversion to string by str()
    for float type with number of decimals set in xtra, an attempt to format the number is made
    """
    formatted = str(resval)
    deci = xtra.get('decimals', None)
    if type(resval) == float and not deci is None and not deci == "":
        try:
            formatted = '{:.{prec}f}'.format(resval, prec=deci)
        except:
            pass
    return formatted


def getSingleStatus(result, static_constraints, xtra):
    # determine if all results are within limits
    status = None
    """
    if status is None:
        if not getattr(result, 'val_period', None) is None:
            status = 'ok'
    """        
    # check equals
    if status is None:
        lim =  static_constraints.get('val_equal', getattr(result, 'val_equal', None))   
        if not lim is None:
            status = 'ok'
            if result.val is None or not str(result.val) == str(lim):
                status = 'critical'

    # check floats min_low_high_max
    if status is None and type(result.val) in [int, float, type(None)]:
        # check if we have a relative constraint
        val_ref =  static_constraints.get('val_ref', getattr(result, 'val_ref', None))

        # check if there is a constraint
        for lim in ['val_min', 'val_low', 'val_high', 'val_max']:
            if not static_constraints.get(lim, getattr(result, lim, None)) is None:
                status = 'ok'
                break;

        lim =  static_constraints.get('val_low', getattr(result, 'val_low', None))   
        if not lim is None:
            if result.val is None:
                status = 'critical'
            elif not val_ref is None: # a relative value
                if result.val < val_ref*(1.+lim/100.):
                    status = 'warning'
            elif result.val < lim:
                status = 'warning'
                
        if not status == 'crtitical':
            lim =  static_constraints.get('val_high', getattr(result, 'val_high', None))   
            if not lim is None:
                if result.val is None:
                    status = 'critical'
                elif not val_ref is None: # a relative value
                    if result.val > val_ref*(1.+lim/100.):
                        status = 'warning'
                elif result.val > lim:
                    status = 'warning'

        if not status == 'crtitical':
            lim =  static_constraints.get('val_min', getattr(result, 'val_min', None))   
            if not lim is None:
                if result.val is None:
                    status = 'critical'
                elif not val_ref is None: # a relative value
                    if result.val < val_ref*(1.+lim/100.):
                        status = 'critical'
                elif result.val < lim:
                    status = 'critical'

        if not status == 'crtitical':
            lim =  static_constraints.get('val_max', getattr(result, 'val_max', None))   
            if not lim is None:
                if result.val is None:
                    status = 'critical'
                elif not val_ref is None: # a relative value
                    if result.val > val_ref*(1.+lim/100.):
                        status = 'critical'
                elif result.val > lim:
                    status = 'critical'

    # fix number of decimals
    resval = formatResults(result.val, xtra)

    if status == 'ok':
        status_color = libdisplay.COLOR_OK #'yellowgreen'
    elif status == 'warning':
        status_color = libdisplay.COLOR_WARNING #'yellow'
    elif status == 'critical':
        status_color = libdisplay.COLOR_CRITICAL #'red'
    else:
        return resval
    
    # make sure the background is visible
    if resval in [None, "", " ", u"", u" "]:
        resval = "value missing"
    return html_elements.Div(label=resval, style="background-color:{}".format(status_color))


def plot(acq_vals, xtra, static_equals, static_minlowhighmax):
    """
    use plotly to make intermediate plots of results
    """
    config = {
        'showLink': False,
        'modeBarButtonsToRemove': ['sendDataToCloud','select2d', 'lasso2d'],
        'scrollZoom': True
    }
    layout_base = dict(
        xaxis=dict(
            rangeselector=dict(
                buttons=list([
                    dict(count=1,
                         label='1m',
                         step='month',
                         stepmode='backward'),
                    dict(count=6,
                         label='6m',
                         step='month',
                         stepmode='backward'),
                    dict(count=1,
                         label='1y',
                         step='year',
                         stepmode='backward'),
                    dict(step='all')
                ])
            ),
            #rangeslider=dict(visible = True), # setting to true disables y-scale zoom and pan
            mirror=True,
            type='date',
            linecolor= "#000000", 
            showline= True, 
            titlefont= {"color": "#000000"}, 
            zeroline= True,
            title='date',
            tickformat= '%Y-%m-%d'
        ),
        yaxis= {
            "anchor": "x", 
            "autorange": True, 
            "linecolor": "#000000", 
            "mirror": True, 
            "showline": True, 
            "side": "left", 
            "tickfont": {"color": "#000000"}, 
            "tickmode": "auto",
            "ticks": "", 
            "title": xtra['units'], 
            "titlefont": {"color": "#000000"}, 
            "type": "linear", 
            "zeroline": True,
        },
        hovermode = 'closest', # by default choose this mode instead of compare
        # reclaim space on top for we do not want a title
        margin={
            't': 50,
        },
        height=500
    )

    # get data in plotly format
    results = getFloatData(acq_vals, xtra, static_equals, static_minlowhighmax) # dict
    toplot = [
        # colornames: https://www.w3schools.com/colors/colors_groups.asp
        # name, legend, {color, dash}, addsymbol
        ('val_equal', 'criterion',      {'color': '#9ACD32', 'dash': 'dot'}), # 'YellowGreen'
        ('val_low', 'acceptable low',   {'color': '#FF8C00', 'dash': 'dot'}), # 'DarkOrange'
        ('val_min', 'critical low',     {'color': '#FF0000', 'dash': 'dot'}), # 'Red'
        ('val_high', 'acceptable high', {'color': '#FF8C00', 'dash': 'dot'}), # 'DarkOrange'
        ('val_max', 'critical high',    {'color': '#FF0000', 'dash': 'dot'}), # 'Red'
        ('val', 'measurement',          {'color': '#6495ED'}), # 'CornflowerBlue'  
    ]

    # plot results
    traces_results = []
    for name, label, line in toplot:
        if name in results.keys():
            plotmode = 'lines+markers' if len(acq_vals)==1 else 'lines'
            if name == 'val': plotmode = 'lines+markers'
            line['width'] = 4
            traces_results.append(
                # Scattergl is faster, but not always supported
                go.Scatter(
                    name = label,
                    x = results['datetime'],
                    y = results[name],
                    mode = plotmode,
                    yaxis="y",
                    line = line,
                    marker = {'size': 8}
                )
            )


    # output to html div
    fig = go.Figure(data=traces_results, layout=layout_base)
    divs = offline.plot(fig, auto_open=False, output_type='div', config=config)
    return Markup(divs)


@mod_blueprint.route('/results/gettable')
@login_required
def gettable():
    """Serves the table."""
    _rid = int(request.args['rid']) if 'rid' in request.args else None
    _datatype = str(request.args['datatype']) if 'datatype' in request.args else None

    if _rid is None or _datatype is None:
        return(redirect(url_for('waddashboard.qc', rid=_rid, datatype=_datatype)))
    
    picked, acq_vals = getDataList(_rid, _datatype)
    
    selector = dbio.DBSelectors.get_by_id(picked.result.selector.id)
    if not is_selector_visible(selector):
        return(redirect(url_for('waddashboard.home')))

    display  = libdisplay.get_display(dbio, picked.result.selector.id)
    display = {k['name']: k for k in display} # turn into dictionary

    # get dispay level of current user
    role = 2
    if session.get('logged_in'):
        role = session.get('role')

    xtra = {'units': '', 'description': ''}
    # check if we should display this data for this user
    if picked.name in display.keys():
        dis = display[picked.name]
        if not dis.get('units', None) is None: xtra['units'] = dis['units']
        if not dis.get('description', None) is None: xtra['description'] = dis['description']
        if role > display[picked.name]['display_level']:
            return(redirect(url_for('waddashboard.home')))
    elif role >1: # only show unknown results to admin
        return(redirect(url_for('waddashboard.home')))

    # add some information about the data
    res_name = display.get('display_name', picked.name)
    if len(res_name.strip()) == 0:
        res_name = picked['name']
    results = [ 
        ('selector name', picked.result.selector.name ) ,
        ('result name', res_name),
        ('description', xtra['description']),
        ('units', xtra['units']),
    ]
    
    headers = ['equals']
    constraints = ['val_equal']
    if _datatype == 'float':
        constraints.extend( [
            'val_min',
            'val_low',
            'val_high',
            'val_max',
        ])
        headers.extend( [
            'min',
            'low',
            'high',
            'max',
        ])
    elif _datatype == 'datetime':
        constraints.append( 'val_period' )
        headers.append( 'period' )
    elif _datatype == 'object':
        constraints = []
        headers = []
    
    header_line = ['datetime', 'value']
    header_line.extend(headers)
    results.append([''])
    results.append(header_line)

    # static constraints
    # get a list of static constraints
    static_constraints = libdisplay.get_all_limits_from_json(dbio, selector)

    static_equals = static_constraints.get('constraint_equals', {})
    static_periods = static_constraints.get('constraint_period', {})
    static_minlowhighmaxs = static_constraints.get('constraint_minlowhighmax', {})

    static_single = {}
    data = acq_vals[0][1]
    if not static_equals.get(data.name, None) is None:
        static_single['val_equal'] = static_equals[data.name]
    if not static_periods.get(data.name, None) is None:
        static_single['val_period'] = static_periods[data.name]
    if not static_minlowhighmaxs.get(data.name, None) is None:
        static_single['val_min'],static_single['val_low'],static_single['val_high'],static_single['val_max'] = static_minlowhighmaxs[data.name]
    
    for time, data in sorted(acq_vals, key=lambda x: x[0], reverse=False):
        res = [str(time), str(data.val)]
        res.extend( [str(static_single.get( atr, getattr(data, atr, None))) for atr in constraints] )
        results.append(res)

    from io import BytesIO
    bIO = BytesIO()
    for result in results:
        res = [ '"{}"'.format(r) if (' ' in r or ',' in r) else r for r in result]
        bIO.write(string_as_bytes((',').join(res)+'\n'))
    bIO.seek(0)
    
    return send_file(bIO, as_attachment=True,
                     attachment_filename='%s.%s'%('wadqc_results', 'csv'),
                     mimetype='image/csv')

@mod_blueprint.route('/results/getobject')
@login_required
def getobject():
    """Serves the image or other object."""
    _did = int(request.args['did']) if 'did' in request.args else None
    if _did is None:
        return redirect(url_for('.showresults'))

    res = dbio.DBResultObjects.get_by_id(_did)
    if not is_selector_visible(res.result.selector):
        return(redirect(url_for('waddashboard.home')))

    if res.filetype.lower() in MIME_IMAGE:
        return send_file(io.BytesIO(res.val),
                         attachment_filename='%s.%s'%(res.name, res.filetype),
                         mimetype='image/%s'%res.filetype)
    else:
        return send_file(io.BytesIO(res.val),
                         attachment_filename='%s.%s'%(res.name, res.filetype),
                         as_attachment=True,
                         mimetype=None)

def getFloatData(acq_vals, xtra, static_equals, static_minlowhighmaxs):
    # here define what fields we want to extract from the results
    fields = ['val', 'val_equal', 'val_min', 'val_low', 'val_high', 'val_max']
    results = { f:[] for f in fields }
    results['datetime'] = []
    results['strdatetime'] = []
    results['units'] = []

    static_constraint = {
        'val_equal': static_equals,
        'val_min': static_minlowhighmaxs[0],
        'val_low': static_minlowhighmaxs[1],
        'val_high': static_minlowhighmaxs[2],
        'val_max': static_minlowhighmaxs[3]
    }

    # extract the given fields from the results
    # initial sorting on datetime (descending)
    for dt, data in sorted(acq_vals, key=lambda x:x[0], reverse=True):
        results['datetime'].append(dt)
        results['strdatetime'].append(dt.strftime('%Y-%m-%d'))
        for f in fields:
            results[f].append(static_constraint.get(f, getattr(data, f, None)))
 
        results['units'].append( xtra['units'] )
        
    # remove empty columns
    for f in ['val_equal', 'val_min', 'val_low', 'val_high', 'val_max', 'units']:
        if sum([a is None for a in results[f]]) == len(results[f]):
            del results[f]

    return results
    
def getDataList(resultid, datatype):
    """
    Returns the picked resultsdata object and a list of (time, item) for that object
    """
    if datatype == 'string':
        model = dbio.DBResultStrings
    elif datatype == 'bool':
        model = dbio.DBResultBools
    elif datatype == 'float':
        model = dbio.DBResultFloats
    elif datatype == 'datetime':
        model = dbio.DBResultDateTimes
    elif datatype == 'object':
        model = dbio.DBResultObjects
        
    picked = model.get_by_id(resultid)

    acq_vals = []
    # find all entries of the model, which are linked to the same the picked selector
    items = model.select().join(dbio.DBResults).join(dbio.DBSelectors).where( (dbio.DBSelectors.id == picked.result.selector.id) & (model.name == picked.name) )
    for item in items:
        dts = item.result.datetimes
        if len(dts)>0:
            dt = max([p.val for p in dts])
        else:
            dt = item.result.created_time
        acq_vals.append((dt, item))

    # picked = wanted result e.g. a DBResultFloats
    return picked, acq_vals

