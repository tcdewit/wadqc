# Import Form
from flask_wtf import FlaskForm

# Import Form elements such as TextField and BooleanField (optional)
from wtforms import IntegerField, StringField, PasswordField, HiddenField, SelectField, BooleanField, validators
try:
    from app.mod_waddashboard.models import role_names
    from app import AUTO_REFRESH
except ImportError:
    from wad_dashboard.app.mod_waddashboard.models import role_names
    from wad_dashboard.app import AUTO_REFRESH

role_choices = [ (k, v) for k,v in sorted(role_names.items()) if k>1]

class ModifyForm(FlaskForm):
    username = StringField('Username', [validators.Length(min=4, max=25, 
                                                          message="Username must be between %(min)d and %(max)d characters long")])
    email    = StringField('Email Address', [validators.Length(min=6, max=35, 
                                                               message="Email Address must be between %(min)d and %(max)d characters long")])
    role     = SelectField("Role", choices = role_choices, coerce=int, default=2)
    refresh  = IntegerField('refresh (s)', [validators.NumberRange(min=1, message="Refresh must be at least %(min)s")], default=AUTO_REFRESH)
    isenabled = BooleanField('Enabled')

    nopassword = BooleanField('No password')

    password = PasswordField('New Password', [
        #validators.DataRequired('Password is required'),
        validators.EqualTo('confirm', message='Passwords must match')
    ])
    gid = HiddenField('gid', [])
    
    confirm = PasswordField('Repeat Password')

    def __init__(self, *args, **kwargs):
        super(FlaskForm, self).__init__(*args, **kwargs)
        role_choices = [ (k, v) for k,v in sorted(role_names.items()) if k >= kwargs['cur_role'] ]
        self.role.choices = role_choices
        self.role.default = 2